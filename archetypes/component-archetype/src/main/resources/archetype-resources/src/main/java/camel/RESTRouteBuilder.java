package ${package}.camel;

import org.apache.camel.builder.RouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class describes the mapping route from client to WS and back to client.
 */
public class RESTRouteBuilder extends RouteBuilder {

	private final Logger log = LoggerFactory.getLogger(RESTRouteBuilder.class);

	public RESTRouteBuilder() {
	}

	@Override
	public void configure() {

		restConfiguration().component("restlet");

		rest().post("Logger/{parameter}").consumes("application/json").produces("application/json").to("bean:loggerProcessor");
	}
}
