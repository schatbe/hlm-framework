package ${package}.processors;

import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoggerProcessor {

	private final Logger log = LoggerFactory.getLogger(LoggerProcessor.class);

	@Handler
	public void process(Exchange exchange) {

		log.info("Start " + this.getClass().getSimpleName());

		String parameter = (String) exchange.getIn().getHeader("parameter");
		log.info("parameter: " + parameter);

		log.info("inputbody: "+ exchange.getIn().getBody(String.class));

		String outBody = "{\"ontvangen\":true}";
		exchange.getOut().setBody(outBody);

		log.info("einde " + this.getClass().getSimpleName());
	}

}
