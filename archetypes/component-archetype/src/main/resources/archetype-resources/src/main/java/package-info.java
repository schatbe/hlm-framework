@HLMPackage(
		type=Type.COMPONENT,
		name="HLM Component ${artifactId}",
		organisation="Organisation"
		)
@HLMConfiguration(
		root="${artifactId}"
		)
package ${package};

import org.hlm.annotation.HLMConfiguration;
import org.hlm.annotation.HLMPackage.Type;
import org.hlm.annotation.HLMPackage;
