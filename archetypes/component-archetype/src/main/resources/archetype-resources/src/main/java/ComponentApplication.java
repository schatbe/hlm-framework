package ${package};

import org.hlm.framework.component.HLMComponentApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import io.datatree.Tree;

/*
 * The org.hlm.framework.component.HLMComponentApplication class takes care of 
 * HLM framework tasks during initialization, specifically (un)deployment of the application.
 * It also provides hooks to be used at various stages of deployment. 
 */
@Component
@DependsOn("ComponentConfiguration")
public class ComponentApplication extends HLMComponentApplication {

	private final Logger log = LoggerFactory.getLogger(ComponentApplication.class);

	/*
	 * preDeploy can be overridden to execute custom code <em>before</em> the
	 * application is <em>deployed</em> to the Camunda Engine.
	 */
	@Override
	public void preDeploy() {
	};

	/*
	 * postDeploy can be overridden to execute custom code <em>after</em> the
	 * application is <em>deployed</em> to the Camunda Engine.
	 *
	 * @param deployment the deployment Tree holding the result from the Camunda
	 * create deployment REST call
	 */
	@Override
	public void postDeploy(Tree deployment) {
	};

	/*
	 * preUnDeploy can be overridden to execute custom code <em>before</em> the
	 * application is <em>undeployed</em> from the Camunda Engine.
	 *
	 * @param deployment the deployment Tree holding the result from the original
	 * Camunda create deployment REST call
	 */
	@Override
	public void preUnDeploy(Tree deployment) {
	};

	/*
	 * postUnDeploy can be overridden to execute custom code <em>after</em> the
	 * application has been <em>undeployed</em> from the Camunda Engine.
	 */
	@Override
	public void postUnDeploy() {
	};
}
