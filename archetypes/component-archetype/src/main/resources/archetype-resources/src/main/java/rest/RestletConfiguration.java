package ${package}.rest;

import org.apache.camel.component.restlet.RestletComponent;
import org.restlet.Component;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RestletConfiguration {

	@Bean(name = "RestletComponent")
	public Component getComponent() {
		Component component = new Component();
		return component;

	}

	@Bean(name = "RestletComponentService")
	public RestletComponent getRestletComponent() {
		RestletComponent component = new RestletComponent(getComponent());
		return component;
	}
}
