package ${package};

import org.hlm.framework.component.HLMComponentConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@Component("ComponentConfiguration")
public class ComponentConfiguration extends HLMComponentConfiguration {
	/*
	 * Place any methods/beans for configuration of the application here
	 */
}
