package org.hlm.common.utils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;

public class PackageInfo {
	String name;
	Class<?> infoClass;
	URI baseURI;

	public PackageInfo(Class<?> infoClass) {
		this.infoClass = infoClass;
		String canonicalName = infoClass.getName();
		this.name = canonicalName.substring(0, canonicalName.lastIndexOf("."));
		this.baseURI = extractBaseResourcePath(name, infoClass.getResource(""));
	}

	public PackageInfo(String packageName, ClassLoader... classLoaders) {
		this(findInfoClass(packageName, classLoaders));
	}

	public URL getResource(String fileName) {
		try {
			String s = baseURI.toString() + fileName;
			return new URL(s);
		} catch (MalformedURLException e) {
			return null;
		}
	}

	public InputStream getResourceAsStream(String fileName) {
		InputStream in = null;
		try {
			URL rel = getResource(fileName);
//			System.out.println(rel);
			in = rel.openStream();
		} catch (IOException e) {
			return null;
		}
		return in;
	}

	private static URI extractBaseResourcePath(String pkgName, URL classpathResourceURL) {
		String pkgResource = classpathResourceURL.toString();
		String baseResource = pkgResource;

		String pkgDir = pkgName.replace('.', File.separatorChar);
		// remove trailing package path
		int end = pkgResource.lastIndexOf(pkgDir);
		if (end == -1)
			end = pkgResource.length() - 1;
		baseResource = pkgResource.substring(0, end);
		URI retval = null;
		try {
			retval = new URI(baseResource);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retval;
	}

	private static Class<?> findInfoClass(String packageName, ClassLoader... classLoaders) {
		Class<?> packageInfo = null;
		ArrayList<ClassLoader> classLoaderList = new ArrayList<ClassLoader>(Arrays.asList(classLoaders));
		if (classLoaderList.isEmpty())
			classLoaderList.add(Thread.currentThread().getContextClassLoader());

		for (ClassLoader classLoader : classLoaderList) {
			try {
				packageInfo = Class.forName(packageName + ".package-info", false, classLoader);
			} catch (ClassNotFoundException ex) {
				// do nothing so the other classloaders can be checked
			}
		}

		if (packageInfo == null) {
			// the package-info could not be found using the provided classloaders,
			// store a proxy for the package info that has no annotations
			class PackageInfoProxy {
			}
			packageInfo = PackageInfoProxy.class;
		}

		return packageInfo;
	}

	public String getName() {
		return name;
	}

	public Class<?> getInfoClass() {
		return infoClass;
	}

	public URI getBaseURI() {
		return baseURI;
	}
}
