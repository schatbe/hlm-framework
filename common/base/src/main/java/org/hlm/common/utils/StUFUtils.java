package org.hlm.common.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public abstract class StUFUtils {

	public static String getStUFDateTime() {
		Calendar cal = Calendar.getInstance();
		// cal.add(Calendar.DATE, 1);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmss");
		return format1.format(cal.getTime());
	}

	//TODO add argument versions for getStUFDate<Time>
	
	public static String getStUFDate() {
		Calendar cal = Calendar.getInstance();
		// cal.add(Calendar.DATE, 1);
		SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMdd");
		return format1.format(cal.getTime());
	}

	public static String getRandomUUID() {
		return java.util.UUID.randomUUID().toString();
	}

}
