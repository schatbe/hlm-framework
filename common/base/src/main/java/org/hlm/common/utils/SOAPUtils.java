package org.hlm.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang.text.StrSubstitutor;
import org.springframework.core.io.ClassPathResource;

public class SOAPUtils {

	public static JAXBElement<?> load(Class<?> clazz, String resource, Map<String, String> variables) {
		String xml = loadResourceasString(resource);
		//		System.out.println(xml);
		String resolvedXml = resolve(xml, variables);
		//		System.out.println(resolvedXml);
		byte[] resolvedXmlByteArray = resolvedXml.getBytes(StandardCharsets.UTF_8);
		// System.out.println(resolvedXmlByteArray);
		InputStream xml_is = new ByteArrayInputStream(resolvedXmlByteArray);
		JAXBElement<?> output = null;
		try {
			JAXBContext jc = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			output = (JAXBElement<?>) unmarshaller.unmarshal(xml_is);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return output;
	}

	public static String loadResourceasString(String resource) {
		ByteArrayOutputStream result = new ByteArrayOutputStream();
		try (InputStream in = new ClassPathResource(resource).getInputStream()) {
			byte[] buffer = new byte[1024];
			int length;
			while ((length = in.read(buffer)) != -1) {
				result.write(buffer, 0, length);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result.toString();
	}

	public static String resolve(String source, Map<String, String> variables) {
		// resolve any variables
		StrSubstitutor sub = new StrSubstitutor(variables);
		return sub.replace(source);
	}

}
