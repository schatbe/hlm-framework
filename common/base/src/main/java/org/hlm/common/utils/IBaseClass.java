package org.hlm.common.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;

public interface IBaseClass {
	static ObjectMapper objectMapper = new ObjectMapper();
	static Logger log = LoggerFactory.getLogger(IBaseClass.class);

	/*
	 * default Object get(String key) { Object value=null; try { Field field =
	 * this.getClass().getField(key); value = field.get(this); } catch(Exception e)
	 * { log.error("Unable to get field value from object: " + e); } return value; }
	 * 
	 * default <T> C set(String key, T value) { try { Field field =
	 * this.getClass().getField(key); field.set(this, value); } catch(Exception e) {
	 * log.error("Unable to set field value for object: " + e); }; return (C) this;
	 * }
	 */
	/*
	 * This object will be converted to a JSON String
	 */
	default String toJSON() {
		return toJSON(this);
	}

	/*
	 * The given object will be converted to a JSON String
	 */
	default String toJSON(Object object) {
		String jsonInString = "{}";

		try {
			jsonInString = objectMapper.writeValueAsString(object);
		} catch (Exception e) {
			log.error("Unable to parse filter into JSON: " + e);
		}
		return jsonInString;
	}

	/*
	 * The given JSON will be used to update the properties of the current object
	 */
	default void updateFromJSON(String jsonInString) {
		try {
			objectMapper.readerForUpdating(this).readValue(jsonInString);
		} catch (Exception e) {
			log.error("Unable to update from JSON: " + e);
		}
	}

	/*
	 * Objects will be transformed into JSON using the ObjectMapper and the
	 * resulting JSON will be used to update the properties of the current object
	 */
	default void updateFrom(Object object) {
		updateFromJSON(toJSON(object));
	}

}
