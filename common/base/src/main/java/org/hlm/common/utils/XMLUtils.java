package org.hlm.common.utils;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 * A collection of utility functions for XML
 */
public abstract class XMLUtils {

	/**
	 * prints a JAXB Class as XML
	 * 
	 * @param root
	 *            the instance to be printed
	 * @param clz
	 *            the class of the instance
	 */
	public static <T> void printXML(T root, Class<?> clz) {
		StringWriter stringWriter = new StringWriter();
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clz);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.marshal(root, stringWriter);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
		System.out.println(stringWriter.toString());
	}

	public static <T> void printXML(T root) {
		printXML(root, root.getClass());
	}

	public static <T> void printXML(JAXBElement<T> root) {
		printXML(root, root.getValue().getClass());
	}
}
