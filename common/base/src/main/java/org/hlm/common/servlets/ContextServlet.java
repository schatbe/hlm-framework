package org.hlm.common.servlets;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hlm.framework.HLM;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@WebServlet("/context")
public class ContextServlet extends HttpServlet {
	private static final long serialVersionUID = -3462096228274971485L;

	//	@Autowired
	//	ServletContext servletContext;

	/* Force creation of the Engine since Beans are created lazily (when they are referenced for the first time)
	 */
	@Override
	protected void doGet(HttpServletRequest reqest, HttpServletResponse response) throws ServletException, IOException {
		// test the spring framework
		ServletContext servletContext = this.getServletContext();
		WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);

		//response.getWriter().println("HLM-Engine Hello World!");
		response.getWriter()
				.println("################################################################################");
		response.getWriter().println("Invoking servlet in " + getClass().getName());
		response.getWriter().println("########");
		//		SpringProcessEngineConfiguration conf = (SpringProcessEngineConfiguration) processEngine
		//				.getProcessEngineConfiguration();
		//		response.getWriter().println(conf.toString());
		//		response.getWriter().println(conf.getExpressionManager().toString());
		response.getWriter().println("\n######## ApplicationContext");
		response.getWriter().println(Arrays.asList(wac.getBeanDefinitionNames()));

		response.getWriter().println("\n######## ParentApplicationContext");
		if (wac.getParent() != null) {
			response.getWriter().println(Arrays.asList(wac.getParent().getBeanDefinitionNames()));
		} else {
			response.getWriter().println("\n[]");
		}
		response.getWriter().println("\n######## Server");
		response.getWriter().println(HLM.Server.toString("yaml", true));
		response.getWriter().println("\n######## Packages");
		response.getWriter().println(HLM.Packages.toString("yaml", true));
		response.getWriter().println("\n######## Preferences");
		response.getWriter().println(HLM.Config.toString("yaml", true));
		response.getWriter().println("\n########");
	}
}