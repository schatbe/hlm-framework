package org.hlm.common.utils;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class PackageUtils {

	private static Map<String, PackageInfo> packageInfoMap = new HashMap<String, PackageInfo>();

	public static PackageInfo getPackageInfo(String pkgName) {
		PackageInfo info = packageInfoMap.get(pkgName);
		if (info == null) {
			info = new PackageInfo(pkgName);
			packageInfoMap.put(pkgName, info);
		}
		return info;
	}

//	public static <A extends Annotation> List<PackageInfo> findLoadedPackagesAnnotatedWith(Class<A> annotationClass) {
//		List<PackageInfo> list = new ArrayList<PackageInfo>();
//		for (Package p : Package.getPackages()) {
//			A c = p.getAnnotation(annotationClass);
//			if (c != null) {
//				list.add(new PackageInfo(p.getName()));
//			}
//		}
//		return list;
//	}

	/////////////////////////////////////////////////////////////////////

//	private static List<Package> findAllPackages() {
//		System.out.println("findAllPackages");
//		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();// getClass().getClassLoader();
//		URLClassLoader uc = null;
//		if (classLoader instanceof URLClassLoader) {
//			uc = (URLClassLoader) classLoader;
//		} else {
//			throw new RuntimeException("classLoader is not an instanceof URLClassLoader");
//		}
//		URL[] urLs = uc.getURLs();
//		// just have a look at what you get...
//		for (int i = 0; i < urLs.length; i++) {
//			System.out.println(i + ". " + urLs[i]);
//		}
//		return null;
//	}
//
//	private static void findAllComponents() {
//		System.out.println("findAllComponents");
//		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
//		scanner.addIncludeFilter(new AnnotationTypeFilter(HLMComponent.class));
//		for (BeanDefinition bd : scanner.findCandidateComponents(""))
//			System.out.println(bd.getBeanClassName());
//	}

	/**
	 *
	 * Attempts to list all the classes in the specified package as determined * by
	 * the Context class loader…
	 *
	 * @param pckgname the package name to search
	 * @return a list of classes that exist within that package
	 * @throws ClassNotFoundException if something went wrong
	 *
	 */
	public static List<PackageInfo> findPackagesAnnotatedWith(Class<? extends Annotation> annotationClass) {
//		System.out.println("findPackagesAnnotatedWith: "+annotationClass.getName());
		List<PackageInfo> result = new ArrayList<PackageInfo>();
		URLClassLoader cld = (URLClassLoader) Thread.currentThread().getContextClassLoader();

		for (URL url : cld.getURLs()){
//			System.out.println("findPackagesAnnotatedWith url: "+url.toString());
			if (url.toString().endsWith(".jar")) {
				String path = url.getPath();
				try {
					path = URLDecoder.decode(path, "utf-8");
					path = new File(path).getPath();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				//System.out.println("Adding jar: "+path);
				result.addAll(findJarPackagesAnnotatedWith(path, annotationClass));
			} else {
				String path = url.getPath();
				try {
					path = URLDecoder.decode(path, "utf-8");
					path = new File(path).getPath();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
				//System.out.println("Adding classpath package: "+path);
				result.addAll(findClasspathPackagesAnnotatedWith(path, annotationClass));
			}
		}

		return result;
	}

	private static PackageInfo anyAnnotationPresent(String packageName, Class<? extends Annotation> annotationClass) {
		PackageInfo info = getPackageInfo(packageName);
		Class<?> infoClass = info.getInfoClass();
		if (infoClass.isAnnotationPresent(annotationClass)) {
				//System.out.println("ANNOTATION FOUND: " + annotationClass + ", IN " + packageName);
			return info;
		}
		return null;
	}

	public static List<PackageInfo> findClasspathPackagesAnnotatedWith(String urlPath,
			Class<? extends Annotation> annotationClass) {
		//System.out.println("findClasspathPackagesAnnotatedWith: " + urlPath);

        String path = urlPath;

		try {
			path = URLDecoder.decode(path, "utf-8");
			path = new File(path).getPath();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		List<PackageInfo> result = new ArrayList<PackageInfo>();

		try {
			Files.walkFileTree(Paths.get(path), new SimpleFileVisitor<Path>() {

				// Compares the glob pattern against
				void find(Path file) {
					Path name = file.getFileName();
					//System.out.println("trying " + name + " :: " + urlPath);
					if (name != null && "package-info.class".equals(name.toString())) {
						String packageName = file.getParent().toString().replace(urlPath, "").replace("/", ".").replace("\\",".");
							//remove leading dot on Windows systems
							if(packageName.indexOf(".")==0){
								packageName = packageName.substring(1);
							}
						 	//System.out.println("trying " + packageName + " :: " + urlPath);
						PackageInfo info;
						if ((info = anyAnnotationPresent(packageName, annotationClass)) != null){
							//System.out.println("Adding: "+info.getName().toString()+" : "+annotationClass.getName().toString());
							result.add(info);
						}

					}
				}

				// Invoke the pattern matching
				// method on each file.
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
					find(file);
					return FileVisitResult.CONTINUE;
				}

				@Override
				public FileVisitResult visitFileFailed(Path file, IOException exc) {
					System.err.println(exc);
					return FileVisitResult.CONTINUE;
				}

			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}

	public static List<PackageInfo> findJarPackagesAnnotatedWith(String jarPath,
			Class<? extends Annotation> annotationClass) {
//		System.out.println("findJarPackagesAnnotatedWith: " + jarPath);
		List<PackageInfo> result = new ArrayList<PackageInfo>();
		try (JarFile jarFile = new JarFile(jarPath)) {
			Enumeration<JarEntry> en = jarFile.entries();
			while (en.hasMoreElements()) {
				JarEntry entry = en.nextElement();
				String entryName = entry.getName();
				if (entryName != null && entryName.endsWith("package-info.class")) {
					String packageName = entryName.substring(0, entryName.lastIndexOf("package-info.class") - 1)
							.replace("/", ".");
					PackageInfo info;
					if ((info = anyAnnotationPresent(packageName, annotationClass)) != null)
						result.add(info);
				}
			}
		} catch (Exception e) {
		}
		return result;
	}
}
