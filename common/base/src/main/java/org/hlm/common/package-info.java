@HLMPackage(type = Type.SERVICE, name = "HLM Common", organisation = "Gemeente Haarlem")
@HLMConfiguration(root = "common")
package org.hlm.common;

import org.hlm.annotation.HLMConfiguration;
import org.hlm.annotation.HLMPackage.Type;
import org.hlm.annotation.HLMPackage;
