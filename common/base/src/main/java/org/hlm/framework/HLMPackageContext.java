package org.hlm.framework;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.hlm.annotation.HLMConfiguration;
import org.hlm.annotation.HLMPackage;
import org.hlm.common.utils.PackageInfo;

import io.datatree.Tree;

public class HLMPackageContext extends Tree {
	private static final long serialVersionUID = 1L;

	private PackageInfo packageInfo = null;

	public HLMPackageContext(PackageInfo packageInfo) {
//		System.out.println("HLMPackageContext: " + packageName);
		this.packageInfo = packageInfo;
		Set<String> basePackageSet = new HashSet<String>();

		HLMPackage packageAnnotation = packageInfo.getInfoClass().getAnnotation(HLMPackage.class);

		this.put("name", packageAnnotation.name()).put("organisation", packageAnnotation.organisation())
				.put("package.name", packageInfo.getName()).put("type", packageAnnotation.type().name());

		// add HLMPackage basePackages
		for (String basePackage : packageAnnotation.type().basePackages) {
			basePackageSet.add(basePackage);
		}
		if (packageAnnotation.scan().length > 0) {
			// add HLMComponent basePackages
			for (String basePackage : packageAnnotation.scan()) {
				basePackageSet.add(basePackage);
			}
		} else {
			// add the package name as default scan Package
			basePackageSet.add(get("package.name", ""));
		}

		// add the scanlist to the tree
		Tree basePackages = this.putList("package.scan");
		basePackageSet.forEach((String basePackage) -> {
			basePackages.add(basePackage);
		});

		HLMConfiguration configurationAnnotation = packageInfo.getInfoClass().getAnnotation(HLMConfiguration.class);
		if (configurationAnnotation != null) {
			this.put("configuration.root", configurationAnnotation.root()).put("file",
					configurationAnnotation.file());

			System.out.println("Loading configuration: "+get("configuration.file"));
			try (InputStream confResource = getResourceAsStream(get("configuration.file", ""))) {
				if (confResource != null) {
					System.out.println("loading configuration for " + get("package.name", ""));
					String yaml = IOUtils.toString(confResource, StandardCharsets.UTF_8);

					Tree configuration = this.putMap("configuration.content");
					configuration.copyFrom(new Tree(yaml, "yaml"), true);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public HLMPackageContext(String packageName) {
		this(new PackageInfo(packageName));
	}

	public PackageInfo getPackageInfo() {
		return packageInfo;
	}

	public URL getResource(String fileName) {
		return packageInfo.getResource(fileName);
	}

	public InputStream getResourceAsStream(String fileName) {
		InputStream stream = packageInfo.getResourceAsStream(fileName);
		if(stream==null){ //Fix for resources not being loaded correctly on Windows
			stream = packageInfo.getInfoClass().getResourceAsStream("/"+fileName);
		}
		return stream;
	}

}
