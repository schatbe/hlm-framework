package org.hlm.framework;

import javax.servlet.ServletContext;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

/**
 * This class is responsible for starting the initialization of the HLM
 * Framework. It is automatically called when the HLM Application is created by
 * the web server, and ensures that the Spring framework is properly initialized
 * at startup and destroyed at shutdown. <br/>
 * <br/>
 * It creates a new root Spring ApplicationContext and then calls
 * {@link org.hlm.framework.HLM#init(ServletContext, AnnotationConfigWebApplicationContext)}
 * to handle the initialization of the HLM Framework itself.
 * 
 * @see org.springframework.web.WebApplicationInitializer#onStartup(javax.servlet.
 *      ServletContext)
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
public class HLMInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext container) {

		// Create the 'root' Spring application Context
		AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();

		System.out.println("********************************************************************************");
		System.out.println("HLM init");
        System.out.println("********************************************************************************");
		HLM.init(container, context);

		container.addListener(new ContextLoaderListener(context));
	}
}
