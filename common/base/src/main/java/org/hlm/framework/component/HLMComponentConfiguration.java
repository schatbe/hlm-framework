package org.hlm.framework.component;

import org.apache.camel.CamelContext;
import org.apache.camel.spring.CamelContextFactoryBean;
import org.hlm.framework.HLM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

/**
 * An abstract class intended to be extended in HLM components to provide
 * required beans for a component. <br/>
 * For example: CamelContext <br/>
 */
public abstract class HLMComponentConfiguration {

	/** The application context. */
	@Autowired
	ApplicationContext applicationContext;

	/**
	 * The camel context. <br/>
	 * It is autowired here to immediately instantiate it, thereby ensuring that the
	 * defined camel routes (microservices) are initialized <em>before</em> the
	 * application is deployed to Camunda. <br/>
	 */
	@Autowired
	CamelContext camelContext;

	/**
	 * The camel context bean definition.<br/>
	 * 
	 * @return the camel context
	 */
	@Bean("CamelContext")
	public CamelContext getCamelContext() {
		String packageName = this.getClass().getPackage().getName();
		String name = HLM.Packages.get(packageName + ".name").asString();

		CamelContextFactoryBean camelContextFactoryBean = new CamelContextFactoryBean();
		camelContextFactoryBean.setId(name + "CamelContext");
		camelContextFactoryBean.setApplicationContext(applicationContext);
		String[] packages = { packageName };
		camelContextFactoryBean.setPackages(packages);
		camelContextFactoryBean.start();
		return camelContextFactoryBean.getContext();
	}
}
