/*
 * 
 */
package org.hlm.framework;

import java.net.InetAddress;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;

import org.apache.catalina.Container;
import org.apache.catalina.connector.Connector;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.core.StandardEngine;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.hlm.annotation.HLMPackage;
import org.hlm.common.utils.PackageInfo;
import org.hlm.common.utils.PackageUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import io.datatree.Tree;

/**
 * An abstract class responsible for initializing and maintaining HLM framework
 * global configuration. It should be used in components to get HLM component
 * and configuration information.
 *
 * <p>
 * <b>Usage:</b><br/>
 * <table>
 * <tr>
 * <td>{@code HLM.Server}</td>
 * <td>the server Tree, it contains information on the framework deployment
 * (host URL, service endpoint, ...)</td>
 * </tr>
 * <tr>
 * <td>{@code HLM.Packages}</td>
 * <td>the package Tree, it contains information on each HLM package in the
 * deployment</td>
 * </tr>
 * <tr>
 * <td>{@code HLM.Config}</td>
 * <td>the configuration Tree, it contains a copy of each packages'
 * configuration file under the root as provided in the
 * {@link org.hlm.framework.annotation.HLMConfiguration @HLMConfiguration}
 * annotation</td>
 * </tr>
 * </table>
 * 
 * @see #init(ServletContext, AnnotationConfigWebApplicationContext)
 */
public abstract class HLM {
	/*
	 * this class MUST be abstract/static, it CANNOT be a Spring bean because it is
	 * needed at the static class level, where Spring beans cannot be autoWired
	 */
	public static ServletContext servletContext;
	public static AnnotationConfigWebApplicationContext applicationContext;
	public static Tree Server = new Tree();
	public static Tree Packages = new Tree();
	public static Tree Config = new Tree();

	private static Map<String, HLMPackageContext> packageContexts = new HashMap<String, HLMPackageContext>();

	/**
	 * Initializes the HLM framework by setting up the global variables to be used
	 * in subsequent code by loading and initializing all present HLM packages. Any
	 * relevant information is written to an in-memory {@link io.datatree.Tree Tree}
	 * structure (datatree.io) which can be easily manipulated from code. <br/>
	 * <br/>
	 * First it finds and initializes all packages that have a package-info class
	 * that is annotated with
	 * {@link org.hlm.framework.annotation.HLMPackage @HLMPackage} and loads the
	 * relevant info into the Packages Tree with the packageName as (sub)root. It
	 * then looks for the
	 * {@link org.hlm.framework.annotation.HLMConfiguration @HLMConfiguration}
	 * annotation and if present processes that by loading the relevant
	 * configuration file into the Configuration Tree with the given configuration
	 * root as tree root.
	 *
	 * @param container the servlet context of the webserver container
	 * @param context   the Spring ApplicationContext to be used as the root for the
	 *                  (local) HLM framework
	 */
	public static void init(ServletContext container, AnnotationConfigWebApplicationContext context) {
		servletContext = container;
		applicationContext = context;

		initServerTree();

		assert (applicationContext instanceof ApplicationContext);

		System.out.println("Getting all packages...");

		// find all HLMPackage annotated packages and register them
		for (PackageInfo p : PackageUtils.findPackagesAnnotatedWith(HLMPackage.class)) {
			System.out.println("Loading package: "+p.getName());

			HLMPackageContext packageContext = new HLMPackageContext(p);
			System.out.println("Adding package: "+p.getName());
			addPackage(packageContext);

			// do a Spring ComponentScan of the java packages required by the HLM package
			packageContext.get("package.scan").forEach((child) -> {
				applicationContext.scan(child.asString());
			});
		}
        System.out.println("***************************\nServer:");
		System.out.println(Server.toString("yaml"));
        System.out.println("***************************\nPackages:");
		System.out.println(Packages.toString("yaml"));
        System.out.println("***************************\nConfig:");
		System.out.println(Config.toString("yaml"));
	}

	private static void addPackage(HLMPackageContext packageContext) {
		String packageName = packageContext.get("package.name", "");

		packageContexts.put(packageName, packageContext);
		Packages.putMap(packageName).copyFrom(packageContext, true);

		if (packageContext.isExists("configuration.root")) {
			String configRoot = packageContext.get("configuration.root", "UNDEFINED");
			Config.putMap(configRoot).copyFrom(packageContext.get("configuration.content"), true);
		}
	}

	private static void removeComponent(String packageName) {
		HLMPackageContext packageContext = Packages.getObject(packageName, null);
		if (packageContext != null) {
			if (packageContext.isExists("configuration.root")) {
				String configRoot = packageContext.get("configuration.root", "UNDEFINED");
				Config.remove(configRoot);
			}
			packageContext.remove(packageName);
			packageContexts.remove(packageName);
		}
	}

	/**
	 * Initialize the Server Tree with information to get the application service
	 * endpoint
	 */
	private static void initServerTree() {
		try {
			// get the hostname
			Server.put("host", InetAddress.getLocalHost().getCanonicalHostName());

			// See
			// https://stackoverflow.com/questions/7481432/i-need-to-know-the-http-and-https-port-my-java-webapp-is-running-on-webapp-start
			// retrieve the Tomcat Catalina container context
			Object o = FieldUtils.readField(servletContext, "context", true);
			StandardContext sCtx = (StandardContext) FieldUtils.readField(o, "context", true);
			Container container = (Container) sCtx;

			// get the root Catalina container context (a.k.a. Engine)
			Container c = container.getParent();
			while (c != null && !(c instanceof StandardEngine)) {
				c = c.getParent();
			}

			if (c != null) {
				StandardEngine engine = (StandardEngine) c;
				// cycle through all connectors to get the HTTP/1.1 connector and get the port
				// number
				for (Connector connector : engine.getService().findConnectors()) {
					if (connector.getProtocol().equals("HTTP/1.1")) {
						Server.put("port", connector.getPort());
						break;
					}
				}
			}

			// get the web application path
			Server.put("path", servletContext.getContextPath());

			// build and set the endpoint
			URI endpoint = new URI("http://" + Server.get("host", "localhost") + ":" + Server.get("port", 8080)
					+ Server.get("path", "/") + "/rs");
			Server.put("endpoint", endpoint.normalize().toString());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}