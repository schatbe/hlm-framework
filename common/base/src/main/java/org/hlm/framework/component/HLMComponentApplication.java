package org.hlm.framework.component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.hlm.framework.HLM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.datatree.Tree;

/**
 * An abstract class intended to be extended in HLM components to provide a
 * generic system for (un)deploying the component to a Camunda Engine. <br/>
 * <ul>
 * <li>The component name as given in the HLMPackage annotation will be used in
 * the deployment.</li>
 * <li>The relevant resources to be deployed will be copied from
 * <code>resources/camunda/*</code>, these should include the BPMN process
 * definition(s) and relevant Forms.</li>
 * </ul>
 * This class provides the bare functions that are called at each stage of
 * deployment. These functions can be overridden to add custom code. </br>
 * </br>
 * <table>
 * <tr>
 * <td><b>preDeploy:</b></td>
 * <td>execute custom code <em>before</em> the application is <em>deployed</em>
 * to the Camunda Engine</td>
 * </tr>
 * <tr>
 * <td><b>postDeploy:</b></td>
 * <td>execute custom code <em>after</em> the application is <em>deployed</em>
 * to the Camunda Engine.</td>
 * </tr>
 * <tr>
 * <td><b>preUnDeploy:</b></td>
 * <td>execute custom code <em>before</em> the application is
 * <em>undeployed</em> from the Camunda Engine.</td>
 * </tr>
 * <tr>
 * <td><b>postUnDeploy:</b></td>
 * <td>execute custom code <em>after</em> the application has been
 * <em>undeployed</em> from the Camunda Engine.</td>
 * </tr>
 * </table>
 * </br>
 */
public abstract class HLMComponentApplication {

	private final Logger log = LoggerFactory.getLogger(HLMComponentApplication.class);

	private String DEPLOYMENT_NAME;
	private String CAMUNDA_REST_API;
	private String CAMUNDA_ENGINE;
	private String CAMUNDA_DEPLOYMENT;
	private static final String demarcationLine = new String(new char[80]).replace("\0", "#");

	private Tree deployment = null;

	/**
	 * preDeploy can be overridden to execute custom code <em>before</em> the
	 * application is <em>deployed</em> to the Camunda Engine.
	 */
	public void preDeploy() {
	};

	/**
	 * postDeploy can be overridden to execute custom code <em>after</em> the
	 * application is <em>deployed</em> to the Camunda Engine.
	 *
	 * @param deployment the deployment Tree holding the result from the Camunda
	 *                   create deployment REST call
	 */
	public void postDeploy(Tree deployment) {
	};

	/**
	 * preUnDeploy can be overridden to execute custom code <em>before</em> the
	 * application is <em>undeployed</em> from the Camunda Engine.
	 *
	 * @param deployment the deployment Tree holding the result from the original
	 *                   Camunda create deployment REST call
	 */
	public void preUnDeploy(Tree deployment) {
	};

	/**
	 * postUnDeploy can be overridden to execute custom code <em>after</em> the
	 * application has been <em>undeployed</em> from the Camunda Engine.
	 */
	public void postUnDeploy() {
	};

	@PostConstruct
	private void deployComponent() {
		String packageName = this.getClass().getPackage().getName();
		DEPLOYMENT_NAME = HLM.Packages.get(packageName + ".name").asString();

		String configRoot = HLM.Packages.get(packageName + ".configuration.root", "");
		if (!configRoot.isEmpty()) {
			CAMUNDA_REST_API = HLM.Config.get(configRoot + ".camunda.rest-api").asString();
			CAMUNDA_ENGINE = HLM.Config.get(configRoot + ".camunda.engine.name").asString();
			CAMUNDA_DEPLOYMENT = CAMUNDA_REST_API + "/engine/" + CAMUNDA_ENGINE + "/deployment";
		}

		preDeploy();
		log.info(demarcationLine + " DEPLOY APPLICATION\n" + "deploying '" + DEPLOYMENT_NAME + "' to "
				+ CAMUNDA_DEPLOYMENT);

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {

			HttpPost uploadFile = new HttpPost(CAMUNDA_DEPLOYMENT + "/create");

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.addTextBody("deployment-name", DEPLOYMENT_NAME, ContentType.TEXT_PLAIN);
			builder.addTextBody("enable-duplicate-filtering", "true", ContentType.TEXT_PLAIN);
			builder.addTextBody("deployment-source", "process application", ContentType.TEXT_PLAIN);
			builder.addTextBody("enable-duplicate-filtering", "true", ContentType.TEXT_PLAIN);

			List<String> addedResources = new ArrayList<String>();
			addedResources.add("adding Resources:");
			URI uri = getClass().getClassLoader().getResource("/camunda").toURI();
			Files.find(Paths.get(uri), Integer.MAX_VALUE, (filePath, fileAttr) -> fileAttr.isRegularFile())
					.forEach((path) -> {
						File f = path.toFile();
						String relPath = Paths.get(uri).relativize(path).toString();
						addedResources.add("	adding resource: '" + relPath + "'");
						try {
							builder.addBinaryBody(relPath, new FileInputStream(f), ContentType.APPLICATION_OCTET_STREAM,
									relPath);
						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
						}

					});
			log.info(addedResources.stream().collect(Collectors.joining("\n")));

			HttpEntity multipart = builder.build();
			uploadFile.setEntity(multipart);
			CloseableHttpResponse response = httpClient.execute(uploadFile);

			deployment = new Tree(IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8));
			log.info("deployment of '" + DEPLOYMENT_NAME + " complete");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			postDeploy(deployment);
		}
	}

	@PreDestroy
	private void undeployComponent() {
		preUnDeploy(deployment);
		log.info(demarcationLine + " UNDEPLOY APPLICATION\n" + "undeploying '" + DEPLOYMENT_NAME + "' from "
				+ CAMUNDA_DEPLOYMENT);

		try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
			HttpDelete remove = new HttpDelete(CAMUNDA_DEPLOYMENT + "/" + deployment.get("id", ""));
			CloseableHttpResponse response = httpClient.execute(remove);
			log.info("undeployment of '" + DEPLOYMENT_NAME + " complete");
			log.info(demarcationLine);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			postUnDeploy();
		}
	}
}
