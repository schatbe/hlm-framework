package org.hlm.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.stereotype.Component;

@Component
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PACKAGE)
public @interface HLMPackage {
	public enum Type {
		SERVICE   ("org.hlm.common"), 
		COMPONENT ("org.hlm.common", "org.hlm.component"),
		MODULE    ("org.hlm.common", "org.hlm.component");

		public String[] basePackages;

		Type(String... basePackages) {
			this.basePackages = basePackages;
		}
	}

	Type type();

	String name();

	String organisation();

	String[] scan() default {};
}
