package org.hlm.annotation;

import static java.lang.annotation.ElementType.PACKAGE;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(PACKAGE)
public @interface HLMConfiguration {

	String root();

	String file() default "configuration.yaml";
}
