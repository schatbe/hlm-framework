package org.hlm.component.processors;

import io.datatree.Tree;
import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.hlm.component.dao.Beantwoordvraag;
import org.hlm.component.dao.OntvangAsynchroonMutatie;
import org.hlm.component.dao.VrijeBerichten;
import org.hlm.component.dto.zkn.Besluit;
import org.hlm.framework.HLM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class VoegBesluitToeProcessor {

    private static DateTimeFormatter datumFormatter = DateTimeFormatter.BASIC_ISO_DATE;

    private final OntvangAsynchroonMutatie asynchroonClient;
    private final VrijeBerichten vrijBerichtClient;
    private final Beantwoordvraag beantwoordvraagClient;

    @Autowired
    public VoegBesluitToeProcessor(OntvangAsynchroonMutatie asynchroonClient, VrijeBerichten vrijBerichtClient, Beantwoordvraag beantwoordvraagClient) {
        this.asynchroonClient = asynchroonClient;
        this.vrijBerichtClient = vrijBerichtClient;
        this.beantwoordvraagClient = beantwoordvraagClient;
    }

    @Handler
    public void process(Exchange exchange) {

        try {
            Tree treeBody = new Tree(exchange.getIn().getBody(String.class));

            System.out.println("ExchangeBody TreeJson: " + treeBody.toString("json", true));

            // get variables from process
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/engine-rest/engine/" + HLM.Config.get("test22.hlm.engine.name").asString() + "/process-instance/" + treeBody.get("process.processInstanceId").asString() + "/variables";
            System.out.println("URL REQUEST: " + url);

            // Camunda API call to get variables
            if (restTemplate.getForEntity(url, String.class).getStatusCode() == HttpStatus.OK) {
                ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
                Tree result = new Tree(response.getBody());
                System.out.println("RESULT RestTemplate: " + result.toString("json", true));
            }

            // TODO: make generic
            if (treeBody.get("besluit.type") != null && treeBody.get("besluit.type").asString().equals("Ontheffing Straatoptredens")) {

                String startdate = treeBody.get("besluit.startdate").asString();
                String aanvraag = treeBody.get("besluit.dagOfKwartaal").asString();

                LocalDate now = LocalDate.now();
                LocalDate localDateStartdate = LocalDate.parse(startdate, datumFormatter);
                LocalDate localDateEnddate;
                LocalDate nextYear = LocalDate.of(now.plusYears(1).getYear(), 1, 1);

                // Calculate enddate for besluit
                String enddate = "";

                if (aanvraag.equals("Dagontheffing")) {
                    enddate = startdate;
                } else if (aanvraag.equals("Kwartaalontheffing")) {
                    localDateEnddate = localDateStartdate.plusMonths(3);

                    // Kwartaalontheffingen only till end of year
                    if (localDateEnddate.isAfter(nextYear)) {
                        localDateEnddate = LocalDate.of(now.getYear(), 12, 31);
                    }
                    enddate = localDateEnddate.format(datumFormatter);
                }

                Besluit besluit = vrijBerichtClient.genereerBesluitIdentificatie();
                // Uit ZTC?
                besluit.setOmschrijving("Verlenen ontheffing");
                besluit.setBesluitdatum(startdate);
//        besluit.setToelichting("Hier een toelichting. (BPM)");
                besluit.setIngangsdatum(startdate);
                besluit.setVervaldatum(enddate);
                besluit.setBeginGeldigheid(startdate);
                besluit.setEindGeldigheid(enddate);
                besluit.setZaakidentificatie(treeBody.get("zaak.zaakidentificatie").asString());

                asynchroonClient.voegBesluitToe(besluit);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
