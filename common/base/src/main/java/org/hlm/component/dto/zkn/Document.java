package org.hlm.component.dto.zkn;

import java.util.Calendar;

public class Document {

	private String identificatie;
	public String getIdentificatie() {
		return identificatie;
	}

	public void setIdentificatie(String identificatie) {
		this.identificatie = identificatie;
	}

	private String bestandsnaam;
	private String contentType;
	private String inhoud;
	private String vertrouwelijkAanduiding;
	private String auteur;
	private String taal;
	private String formaat;
	private String titel;
	private String omschrijving;
	private Calendar creatiedatum;
	private Calendar ontvangstdatum;
	
	public String getVersie() {
		return versie;
	}

	public void setVersie(String versie) {
		this.versie = versie;
	}

	private String versie;
	private Zaak zaak;
	private String status;

	public String getVertrouwelijkAanduiding() {
		return vertrouwelijkAanduiding;
	}

	public void setVertrouwelijkAanduiding(String vertrouwelijkAanduiding) {
		this.vertrouwelijkAanduiding = vertrouwelijkAanduiding;
	}

	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}

	public String getTaal() {
		return taal;
	}
	public void setTaal(String taal) {
		this.taal = taal;
	}

	public String getFormaat() {
		return formaat;
	}
	public void setFormaat(String formaat) {
		this.formaat = formaat;
	}

	public String getTitel() {
		return titel;
	}
	public void setTitel(String titel) {
		this.titel = titel;
	}

	public String getOmschrijving() {
		return omschrijving;
	}
	public void setOmschrijving(String omschrijving) {
		this.omschrijving = omschrijving;
	}

	public Calendar getCreatiedatum() {
		return creatiedatum;
	}
	public void setCreatiedatum(Calendar creatiedatum) {
		this.creatiedatum = creatiedatum;
	}

	public Calendar getOntvangstdatum() {
		return ontvangstdatum;
	}
	public void setOntvangstdatum(Calendar ontvangstdatum) {
		this.ontvangstdatum = ontvangstdatum;
	}

	public Zaak getZaak() {
		return zaak;
	}
	public void setZaak(Zaak zaak) {
		this.zaak = zaak;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getBestandsnaam() {
		return bestandsnaam;
	}
	public void setBestandsnaam(String bestandsnaam) {
		this.bestandsnaam = bestandsnaam;
	}

	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getInhoud() {
		return inhoud;
	}
	public void setInhoud(String inhoud) {
		this.inhoud = inhoud;
	}
}

