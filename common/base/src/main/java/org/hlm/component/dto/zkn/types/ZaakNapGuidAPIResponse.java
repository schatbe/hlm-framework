
package org.hlm.component.dto.zkn.types;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ZaakNapGuidAPIResponse {

    @SerializedName("guid")
    private String Guid;
    @SerializedName("status")
    private String Status;

    public String getGuid() {
        return Guid;
    }

    public void setGuid(String guid) {
        Guid = guid;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
