package org.hlm.component.dao;

import java.util.List;
import java.util.Objects;

import org.hlm.component.dto.ztc.json2pojo.Catalogus;
import org.hlm.component.dto.ztc.json2pojo.Resultaattype;
import org.hlm.component.dto.ztc.json2pojo.Results;
import org.hlm.component.dto.ztc.json2pojo.Statustype;
import org.hlm.component.dto.ztc.json2pojo.Zaaktype;
import org.hlm.framework.HLM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

@Service
public class Ztc {

	private String ZTC_OAUTH2_URI = HLM.Config.get("common.ztc.rest.api-uri","");
	
	@Autowired
	OAuth2RestTemplate ztcOauth2RestTemplate;

//	@Autowired
//    public Ztc(OAuth2RestTemplate ztcOauth2RestTemplate) {
//        this.ztcOauth2RestTemplate = ztcOauth2RestTemplate;
//    }

	public Catalogus getCatalogus(String domein) {
		Results<Catalogus> catalogusResponse = ztcOauth2RestTemplate
				.exchange(ZTC_OAUTH2_URI + "catalogussen/?zoek=" + domein, HttpMethod.GET, null,
						new ParameterizedTypeReference<Results<Catalogus>>() {
						})
				.getBody();
		List<Catalogus> results = Objects.requireNonNull(catalogusResponse).getResults();

		if (results.size() > 0)
			return results.get(0);
		return null;
	}

	public Zaaktype getZaaktype(Catalogus catalogus, String zaaktypeidentificatie) {
		Results<Zaaktype> zaaktypeResponse = ztcOauth2RestTemplate
				.exchange(catalogus.getUrl() + "zaaktypen/" + "?zoek=" + zaaktypeidentificatie, HttpMethod.GET, null,
						new ParameterizedTypeReference<Results<Zaaktype>>() {
						})
				.getBody();
		List<Zaaktype> results = Objects.requireNonNull(zaaktypeResponse).getResults();

		return results.get(0);
	}

	public Resultaattype getResultaattype(Zaaktype zaaktype, String resultaatomschrijving) {
		String melding = "De melding is afgehandeld.";
		if (resultaatomschrijving.equals(melding)) {
			resultaatomschrijving = "Afgehandeld";
		}

		Results<Resultaattype> resultaattypeResponse = ztcOauth2RestTemplate
				.exchange(zaaktype.getUrl() + "resultaattypen/" + "?zoek=" + resultaatomschrijving, HttpMethod.GET,
						null, new ParameterizedTypeReference<Results<Resultaattype>>() {
						})
				.getBody();
		List<Resultaattype> results = Objects.requireNonNull(resultaattypeResponse).getResults();

		return results.get(0);
	}

	public Statustype getFirstStatustype(Zaaktype zaaktype) {
		Results<Statustype> statustypeResponse = ztcOauth2RestTemplate.exchange(zaaktype.getUrl() + "statustypen/",
				HttpMethod.GET, null, new ParameterizedTypeReference<Results<Statustype>>() {
				}).getBody();
		List<Statustype> results = Objects.requireNonNull(statustypeResponse).getResults();

		return results.get(0);
	}

	public Statustype getStatustypeByVolgnummer(Zaaktype zaaktype, Long statustypeVolgnummer) {
		Results<Statustype> statustypeResponse = ztcOauth2RestTemplate
				.exchange(zaaktype.getUrl() + "statustypen/" + "?zoek=" + statustypeVolgnummer.toString(),
						HttpMethod.GET, null, new ParameterizedTypeReference<Results<Statustype>>() {
						})
				.getBody();
		List<Statustype> results = Objects.requireNonNull(statustypeResponse).getResults();

		return results.get(0);
	}
}
