package org.hlm.component.dao;

import org.hlm.component.dto.gba.Persoon;
import org.hlm.component.dto.zkn.Zaak;
import org.hlm.component.dto.zkn.types.Contactpersoon;
import org.hlm.component.dto.zkn.types.ZaakNapAPIResponse;
import org.hlm.component.dto.zkn.types.ZaakNapGuidAPIResponse;
import org.hlm.framework.HLM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ZaakNapAPI {

	private static String restNapAPIURI = HLM.Config.get("common.zkn.restNap.api-uri","");

	private final RestTemplate restTemplateZaakNapAPI;

	@Autowired
	public ZaakNapAPI(RestTemplate restTemplateZaakNapAPI) {
		this.restTemplateZaakNapAPI = restTemplateZaakNapAPI;
	}

	public <T> T get(String query, Class<T> responseType) {
		return restTemplateZaakNapAPI.getForObject(restNapAPIURI + query, responseType);
	}

	private <T> T post(String query, String inputJSON, Class<T> responseType) {
		restTemplateZaakNapAPI.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(inputJSON, headers);

		ResponseEntity<T> response = restTemplateZaakNapAPI.exchange(restNapAPIURI + query, HttpMethod.POST, entity,
				responseType);
		return response.getBody();
	}

	public Contactpersoon getContactpersoon(String napId) {
		ZaakNapAPIResponse response = get("contactpersonen/" + napId, ZaakNapAPIResponse.class);
		return response.getData();
	}

	public String postContactpersoonGetGUID(Zaak zaak) {
		Persoon persoon = zaak.getPersoon();
		String input = "{\"geslachtsnaam\":\"" + persoon.getGeslachtsnaam() + "\"," + "\"voorvoegsel\":\""
				+ persoon.getVoorvoegselGeslachtsnaam() + "\"," + "\"voorletters\":\"" + persoon.getVoorletters()
				+ "\"," + "\"telefoonnummer\":" + "null" + "," + "\"emailadres\":" + "null" + ","
				+ "\"zaakidentificatie\":\"" + zaak.getZaakidentificatie() + "\"," + "\"geboortedatum\":\""
				+ persoon.getGeboortedatum() + "\"" + "}";
		ZaakNapGuidAPIResponse response = post("contactpersonen/", input, ZaakNapGuidAPIResponse.class);
		return response.getGuid();
	}
}
