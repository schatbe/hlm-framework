package org.hlm.component.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import nl.egem.stuf.sector.zkn.*;
import org.hlm.common.utils.XMLUtils;
import org.hlm.component.dto.gba.Persoon;
import org.hlm.component.dto.zkn.Besluit;
import org.hlm.component.dto.zkn.Besluiten;
import org.hlm.component.dto.zkn.Zaak;
import org.hlm.component.dto.zkn.types.Contactpersoon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

@Service("beantwoordVraagImplClient")
public class BeantwoordvraagImpl implements Beantwoordvraag {

	private final ZaakNapAPI zaakNapAPI;
	private WebServiceTemplate bevWebServiceTemplate;

	@Autowired
    public BeantwoordvraagImpl(WebServiceTemplate bevWebServiceTemplate, ZaakNapAPI zaakNapAPI) {
		this.bevWebServiceTemplate = bevWebServiceTemplate;
        this.zaakNapAPI = zaakNapAPI;
    }

	@Override
	public Zaak geefZaakdetails(Zaak zaak) {
		ObjectFactory factory = new ObjectFactory();

		// stuurgegevens
		GeefZaakDetailsZAKLv01 zaakDetails = factory.createGeefZaakDetailsZAKLv01();
		ZAKStuurgegevensLv01 stuurgegevens = getStuurGegevens(factory);

		zaakDetails.setStuurgegevens(stuurgegevens);

		// parameters
		ZAKParametersVraagSynchroon params2 = factory.createZAKParametersVraagSynchroon();
		params2.setSortering(4);
		params2.setIndicatorVervolgvraag(false);
		BigInteger bigint = BigInteger.valueOf(15);
		params2.setMaximumAantal(bigint);
		params2.setIndicatorAfnemerIndicatie(false);
		params2.setIndicatorAantal(false);

		zaakDetails.setParameters(params2);

		// gelijk
		GeefZaakDetailsZAKVraagSelectie gelijk = factory.createGeefZaakDetailsZAKVraagSelectie();
		gelijk.setEntiteittype("ZAK");

		// zaaknummer uit zaak halen
		ZaakIdentificatieE zaaknummer = factory.createZaakIdentificatieE();
		zaaknummer.setValue(zaak.getZaakidentificatie());
		gelijk.setIdentificatie(zaaknummer);

		zaakDetails.setGelijk(gelijk);

		// scope
		ZAKVraagScope scope = factory.createZAKVraagScope();
		scope.setEntiteittype("ZAK");
		scope.setScope("alles");

		GeefZaakDetailsZAKLv01.Scope scope1 = factory.createGeefZaakDetailsZAKLv01Scope();
		scope1.setObject(scope);
		zaakDetails.setScope(scope1);

		// print GeefZaakDetailsZAKLv01 SOAP request
		QName qName = new QName("http://www.stufstandaarden.nl/koppelvlak/zds0120", "geefZaakdetails_ZakLv01");
		JAXBElement<GeefZaakDetailsZAKLv01> root = new JAXBElement<>(qName, GeefZaakDetailsZAKLv01.class, zaakDetails);
		XMLUtils.printXML(root);

		// make SOAP request and print GeefZaakDetailsZAKLa01 SOAP response
		JAXBElement JAXBEresponse = (JAXBElement) bevWebServiceTemplate.marshalSendAndReceive(root);
        XMLUtils.printXML(JAXBEresponse);

		// cast response to GeefZaakDetailsZAKLa01 object
		GeefZaakDetailsZAKLa01 response = (GeefZaakDetailsZAKLa01) JAXBEresponse.getValue();

		if (response != null && response.getAntwoord() != null) {

			GeefZaakDetailsZAKLa01.Antwoord antwoord = response.getAntwoord();
			GeefZaakDetailsZAKAntwoord antwoordObject = antwoord.getObject();

			String identificatie = antwoordObject.getIdentificatie().getValue();
			int zaaktypeidentificatie = Integer.parseInt(antwoordObject.getIsVan().get(0).getGerelateerde().getCode().getValue().getValue());
			String zaaktypeomschrijving = antwoordObject.getIsVan().get(0).getGerelateerde().getOmschrijving().getValue().getValue();

			String resultaatomschrijving = null;
			if (antwoordObject.getResultaat().getValue().getOmschrijving().getValue().getValue() != null && antwoordObject.getResultaat().getValue().getOmschrijving().getValue().getValue().equals("")) {
				resultaatomschrijving = antwoordObject.getResultaat().getValue().getOmschrijving().getValue().getValue();
			}

			String organisatie = response.getStuurgegevens().getZender().getOrganisatie();

			String einddatum = null;
			if (antwoordObject.getEinddatum().getValue().getValue() != null) {
				einddatum = antwoordObject.getEinddatum().getValue().getValue().toString();
			}

			String archiefactiedatum = null;
			if (antwoordObject.getDatumVernietigingDossier().getValue().getValue() != null) {
				archiefactiedatum = antwoordObject.getDatumVernietigingDossier().getValue().getValue().toString();
			}

			Persoon persoon = new Persoon();
			if (antwoordObject.getHeeftAlsInitiator().get(0).getGerelateerde().getNatuurlijkPersoon() != null) {
				persoon.setBsn(antwoordObject.getHeeftAlsInitiator().get(0).getGerelateerde().getNatuurlijkPersoon().getValue().getInpBsn().getValue().getValue());
				persoon.setVoorvoegselGeslachtsnaam(antwoordObject.getHeeftAlsInitiator().get(0).getGerelateerde().getNatuurlijkPersoon().getValue().getVoorvoegselGeslachtsnaam().getValue().getValue());
				persoon.setVoorletters(antwoordObject.getHeeftAlsInitiator().get(0).getGerelateerde().getNatuurlijkPersoon().getValue().getVoorletters().getValue().getValue());
				persoon.setGeslachtsnaam(antwoordObject.getHeeftAlsInitiator().get(0).getGerelateerde().getNatuurlijkPersoon().getValue().getGeslachtsnaam().getValue().getValue());
				persoon.setGeboortedatum(antwoordObject.getHeeftAlsInitiator().get(0).getGerelateerde().getNatuurlijkPersoon().getValue().getGeboortedatum().getValue().getValue().toString());

			} else if (antwoordObject.getAnderZaakObject().size() > 0) {
				if (antwoordObject.getAnderZaakObject().get(0).getAanduiding().getValue().equals("contactpersoon")) {
					String napId = antwoordObject.getAnderZaakObject().get(0).getRegistratie().getValue();

					// get personal info from zaakmagazijnnietauthentiek
					Contactpersoon contactpersoon = zaakNapAPI.getContactpersoon(napId);

					persoon.setVoorvoegselGeslachtsnaam(contactpersoon.getVoorvoegsel());
					persoon.setVoorletters(contactpersoon.getVoorletters());
					persoon.setGeslachtsnaam(contactpersoon.getGeslachtsnaam());
					persoon.setGeboortedatum(contactpersoon.getGeboortedatum());
				}
			}

			Zaak zaak1 = new Zaak();
			zaak1.setZaakidentificatie(identificatie);
			zaak1.setZaaktypeidentificatie(zaaktypeidentificatie);
			zaak1.setZaaktypeomschrijving(zaaktypeomschrijving);
			zaak1.setResultaatomschrijving(resultaatomschrijving);
			zaak1.setBronorganisatie(organisatie);
			zaak1.setEinddatum(einddatum);
			zaak1.setArchiefactiedatum(archiefactiedatum);
			zaak1.setPersoon(persoon);

			return zaak1;
		}
		return null;
	}

	public Besluiten geefLijstBesluiten(Zaak zaak) {
		ObjectFactory factory = new ObjectFactory();
		GeefLijstBesluitenZAKLv01 geefLijstBesluitenZAKLv01 = factory.createGeefLijstBesluitenZAKLv01();

		// stuurgegevens
		ZAKStuurgegevensLv01 stuurgegevens = getStuurGegevens(factory);
		geefLijstBesluitenZAKLv01.setStuurgegevens(stuurgegevens);

		// parameters
		ZAKParametersVraagSynchroon params2 = factory.createZAKParametersVraagSynchroon();
		params2.setSortering(4);
		params2.setIndicatorVervolgvraag(false);
		BigInteger bigint = BigInteger.valueOf(15);
		params2.setMaximumAantal(bigint);
		params2.setIndicatorAfnemerIndicatie(false);
		params2.setIndicatorAantal(false);
		geefLijstBesluitenZAKLv01.setParameters(params2);

		// gelijk
		GeefLijstBesluitenZAKVraagSelectie geefLijstBesluitenZAKVraagSelectie = factory.createGeefLijstBesluitenZAKVraagSelectie();
		geefLijstBesluitenZAKVraagSelectie.setEntiteittype("ZAK");

		// get zaakidentificatie from zaak
		ZaakIdentificatieE zaaknummer = factory.createZaakIdentificatieE();
		zaaknummer.setValue(zaak.getZaakidentificatie());
		geefLijstBesluitenZAKVraagSelectie.setIdentificatie(zaaknummer);
		geefLijstBesluitenZAKLv01.setGelijk(geefLijstBesluitenZAKVraagSelectie);

		// scope
		GeefLijstBesluitenZAKVraagScope scope = factory.createGeefLijstBesluitenZAKVraagScope();
		scope.setEntiteittype("ZAK");
		scope.setScope("alles");
		ZaakIdentificatieE zaakIdentificatieE = factory.createZaakIdentificatieE();
		zaakIdentificatieE.setValue("00000");
		scope.setIdentificatie(zaakIdentificatieE);

		GeefLijstBesluitenZAKBSLVraag geefLijstBesluitenZAKBSLVraag = factory.createGeefLijstBesluitenZAKBSLVraag();
		geefLijstBesluitenZAKBSLVraag.setEntiteittype("ZAKBSL");

		GeefLijstBesluitenBSLGerelateerdeVraag geefLijstBesluitenBSLGerelateerdeVraag = factory.createGeefLijstBesluitenBSLGerelateerdeVraag();
		geefLijstBesluitenBSLGerelateerdeVraag.setEntiteittype("BSL");
		DocumentIdentificatieE documentIdentificatieE = factory.createDocumentIdentificatieE();
		documentIdentificatieE.setValue("");
		geefLijstBesluitenBSLGerelateerdeVraag.setIdentificatie(documentIdentificatieE);
		DatumE datumE = factory.createDatumE();
		datumE.setValue(new BigDecimal(0));
		geefLijstBesluitenBSLGerelateerdeVraag.setDatumBeslissing(datumE);
		geefLijstBesluitenBSLGerelateerdeVraag.setIngangsdatumWerking(datumE);
		geefLijstBesluitenZAKBSLVraag.setGerelateerde(geefLijstBesluitenBSLGerelateerdeVraag);
		scope.setLeidtTot(geefLijstBesluitenZAKBSLVraag);

		GeefLijstBesluitenZAKLv01.Scope scope1 = factory.createGeefLijstBesluitenZAKLv01Scope();
		scope1.setObject(scope);
		geefLijstBesluitenZAKLv01.setScope(scope1);

		// print SOAP request
		QName qName = new QName("http://www.stufstandaarden.nl/koppelvlak/zds0120", "geefLijstBesluiten_ZakLv01");
		JAXBElement<GeefLijstBesluitenZAKLv01> root = new JAXBElement<>(qName, GeefLijstBesluitenZAKLv01.class, geefLijstBesluitenZAKLv01);
        XMLUtils.printXML(root);

		// make SOAP request and print SOAP response
		JAXBElement JAXBEresponse = (JAXBElement) bevWebServiceTemplate.marshalSendAndReceive(root);
        XMLUtils.printXML(JAXBEresponse);

		// cast response to GeefZaakDetailsZAKLa01 object
		GeefLijstBesluitenZAKLa01 response = (GeefLijstBesluitenZAKLa01) JAXBEresponse.getValue();

		if (response != null && response.getAntwoord() != null) {

			GeefLijstBesluitenZAKLa01.Antwoord antwoord = response.getAntwoord();
			List<GeefLijstBesluitenZAKAntwoord> antwoordObjectList = antwoord.getObject();
			GeefLijstBesluitenZAKAntwoord antwoordObject = antwoordObjectList.get(0);          // first object only

			if (antwoordObject.getLeidtTot() != null) {
				List<GeefLijstBesluitenZAKBSLAntwoord> leidtTotList = antwoordObject.getLeidtTot();
				GeefLijstBesluitenZAKBSLAntwoord leidtTot = leidtTotList.get(0);                   // first item only (temp)

				Besluit besluit = new Besluit();
				besluit.setBesluitIdentificatie(leidtTot.getGerelateerde().getIdentificatie().getValue());
				besluit.setBesluitdatum(leidtTot.getGerelateerde().getDatumBeslissing().getValue().toString());
				besluit.setOmschrijving(leidtTot.getGerelateerde().getBstOmschrijving().getValue().getValue());
				besluit.setToelichting(leidtTot.getGerelateerde().getToelichting().getValue().getValue());
				besluit.setIngangsdatum(leidtTot.getGerelateerde().getIngangsdatumWerking().getValue().toString());

				if (leidtTot.getGerelateerde().getEinddatumWerking().getValue().getValue() != null) {
					besluit.setVervaldatum(leidtTot.getGerelateerde().getEinddatumWerking().getValue().getValue().toString());
				}

				if (leidtTot.getGerelateerde().getVervalreden().getValue().getValue() != null) {
//                    Vervalreden vervalreden = leidtTot.getGerelateerde().getVervalreden().getValue().getValue();
					besluit.setVervalreden(leidtTot.getGerelateerde().getVervalreden().getValue().getValue().value());
				}

				Besluiten besluiten = new Besluiten();
				besluiten.getBesluiten().add(besluit);

				return besluiten;
			}
		}
		return new Besluiten();
	}

	private ZAKStuurgegevensLv01 getStuurGegevens(ObjectFactory factory) {

		ZAKStuurgegevensLv01 stuurgegevens = factory.createZAKStuurgegevensLv01();
		stuurgegevens.setBerichtcode(BerichtcodeLv01.LV_01);

		Systeem zender = factory.createSysteem();
		zender.setOrganisatie("0392");
		zender.setApplicatie("GAAPP");
		stuurgegevens.setZender(zender);

		Systeem ontvanger = factory.createSysteem();
		ontvanger.setOrganisatie("0392");
		ontvanger.setApplicatie("ZSH");
		stuurgegevens.setOntvanger(ontvanger);
		stuurgegevens.setEntiteittype(EntiteittypeZAK.ZAK);

		return stuurgegevens;
	}
}
