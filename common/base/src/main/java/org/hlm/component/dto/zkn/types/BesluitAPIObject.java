package org.hlm.component.dto.zkn.types;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class BesluitAPIObject {

    @SerializedName("besluitdatum")
    private String Besluitdatum;
    @SerializedName("besluittoelichting")
    private String Besluittoelichting;
    @SerializedName("besluittypeomschrijving")
    private String Besluittypeomschrijving;
    @SerializedName("ingangsdatum")
    private String Ingangsdatum;
    @SerializedName("omschrijving")
    private String Omschrijving;
    @SerializedName("toelichting")
    private String Zaaktoelichting;
    @SerializedName("vervaldatum")
    private String Vervaldatum;
    @SerializedName("vervalreden")
    private String Vervalreden;
    @SerializedName("zaakidentificatie")
    private String Zaakidentificatie;
    @SerializedName("zaaktypeidentificatie")
    private String Zaaktypeidentificatie;
    @SerializedName("zaaktypeomschrijving")
    private String Zaaktypeomschrijving;

    public String getBesluitdatum() {
        return Besluitdatum;
    }
    public void setBesluitdatum(String besluitdatum) {
        Besluitdatum = besluitdatum;
    }

    public String getBesluittoelichting() {
        return Besluittoelichting;
    }
    public void setBesluittoelichting(String besluittoelichting) {
        Besluittoelichting = besluittoelichting;
    }

    public String getBesluittypeomschrijving() {
        return Besluittypeomschrijving;
    }
    public void setBesluittypeomschrijving(String besluittypeomschrijving) {
        Besluittypeomschrijving = besluittypeomschrijving;
    }

    public String getIngangsdatum() {
        return Ingangsdatum;
    }
    public void setIngangsdatum(String ingangsdatum) {
        Ingangsdatum = ingangsdatum;
    }

    public String getOmschrijving() {
        return Omschrijving;
    }
    public void setOmschrijving(String omschrijving) {
        Omschrijving = omschrijving;
    }

    public String getToelichting() {
        return Zaaktoelichting;
    }
    public void setToelichting(String toelichting) {
        Zaaktoelichting = toelichting;
    }

    public String getVervaldatum() {
        return Vervaldatum;
    }
    public void setVervaldatum(String vervaldatum) {
        Vervaldatum = vervaldatum;
    }

    public Object getVervalreden() {
        return Vervalreden;
    }
    public void setVervalreden(String vervalreden) {
        Vervalreden = vervalreden;
    }

    public String getZaakidentificatie() {
        return Zaakidentificatie;
    }
    public void setZaakidentificatie(String zaakidentificatie) {
        Zaakidentificatie = zaakidentificatie;
    }

    public String getZaaktypeidentificatie() {
        return Zaaktypeidentificatie;
    }
    public void setZaaktypeidentificatie(String zaaktypeidentificatie) {
        Zaaktypeidentificatie = zaaktypeidentificatie;
    }

    public String getZaaktypeomschrijving() {
        return Zaaktypeomschrijving;
    }
    public void setZaaktypeomschrijving(String zaaktypeomschrijving) {
        Zaaktypeomschrijving = zaaktypeomschrijving;
    }
}
