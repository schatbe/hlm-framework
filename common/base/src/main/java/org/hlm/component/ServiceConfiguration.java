package org.hlm.component;

import java.io.InputStream;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLContext;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.hlm.framework.HLM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.transport.http.HttpComponentsMessageSender;

@Configuration
@EnableOAuth2Client
public class ServiceConfiguration {

	private final ApplicationContext applicationContext;

	private String gbaUrl = HLM.Config.get("common.sbc.gbaUrl","");
	private String zknUrl = HLM.Config.get("common.sbc.zknUrl","");
	private String vrbUrl = HLM.Config.get("common.sbc.vrbUrl","");
	private String bevUrl = HLM.Config.get("common.sbc.bevUrl","");
	private String molzUrl = HLM.Config.get("common.mijnOverheid.lopendeZaken.url","");

	@Autowired
	public ServiceConfiguration(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Bean
	@Scope("prototype")
	public Jaxb2Marshaller jaxb2Marshaller(String contextPath) {
		Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
		jaxb2Marshaller.setLazyInit(false);
		jaxb2Marshaller.setContextPath(contextPath);
		return jaxb2Marshaller;
	}

	@Bean
	public Jaxb2Marshaller gbaJaxb2Marshaller() {
		return (Jaxb2Marshaller) applicationContext.getBean("jaxb2Marshaller", "nl.egem.stuf.sector.bg");
	}

	@Bean
	public Jaxb2Marshaller zknJaxb2Marshaller() {
		return (Jaxb2Marshaller) applicationContext.getBean("jaxb2Marshaller", "nl.egem.stuf.sector.zkn");
	}

	@Bean
	@Scope("prototype")
	public WebServiceTemplate webServiceTemplate(String marshaller, String uri) {
		WebServiceTemplate webServiceTemplate = new WebServiceTemplate();
		Jaxb2Marshaller jaxb2Marshaller = (Jaxb2Marshaller) applicationContext.getBean(marshaller);
		webServiceTemplate.setMarshaller(jaxb2Marshaller);
		webServiceTemplate.setUnmarshaller(jaxb2Marshaller);
		webServiceTemplate.setDefaultUri(uri);
		return webServiceTemplate;
	}

	@Bean(name = "gbaWebServiceTemplate")
	public WebServiceTemplate gbaWebServiceTemplate() {
		return (WebServiceTemplate) applicationContext.getBean("webServiceTemplate", "gbaJaxb2Marshaller", gbaUrl);
	}

	@Bean(name = "zknWebServiceTemplate")
	public WebServiceTemplate zknWebServiceTemplate() throws Exception {
		WebServiceTemplate webServiceTemplate = (WebServiceTemplate) applicationContext.getBean("webServiceTemplate",
				"zknJaxb2Marshaller", zknUrl);
		webServiceTemplate.setMessageSender(httpComponentsMessageSender());
		return webServiceTemplate;
	}

	@Bean(name = "molzWebServiceTemplate")
	public WebServiceTemplate molzWebServiceTemplate() {
		return (WebServiceTemplate) applicationContext.getBean("webServiceTemplate", "zknJaxb2Marshaller", molzUrl);
	}

	@Bean(name = "vrbWebServiceTemplate")
	public WebServiceTemplate vrbWebServiceTemplate() throws Exception {
		WebServiceTemplate webServiceTemplate = (WebServiceTemplate) applicationContext.getBean("webServiceTemplate",
				"zknJaxb2Marshaller", vrbUrl);
		webServiceTemplate.setMessageSender(httpComponentsMessageSender());
		return webServiceTemplate;
	}

	@Bean(name = "bevWebServiceTemplate")
	public WebServiceTemplate bevWebServiceTemplate() throws Exception {
		WebServiceTemplate webServiceTemplate = (WebServiceTemplate) applicationContext.getBean("webServiceTemplate",
				"zknJaxb2Marshaller", bevUrl);
		webServiceTemplate.setMessageSender(httpComponentsMessageSender());
		return webServiceTemplate;
	}

	@Bean
	public HttpComponentsMessageSender httpComponentsMessageSender() throws Exception {
		HttpComponentsMessageSender httpComponentsMessageSender = new HttpComponentsMessageSender();
		httpComponentsMessageSender.setHttpClient(httpClient());
		return httpComponentsMessageSender;
	}

	private HttpClient httpClient() throws Exception {
		return HttpClientBuilder.create().setSSLSocketFactory(sslConnectionSocketFactory())
				.addInterceptorFirst(new HttpComponentsMessageSender.RemoveSoapHeadersInterceptor()).build();
	}

	private SSLConnectionSocketFactory sslConnectionSocketFactory() throws Exception {
		// NoopHostnameVerifier essentially turns hostname verification off as otherwise
		// following error
		// is thrown: java.security.cert.CertificateException: No name matching
		// localhost found
		return new SSLConnectionSocketFactory(sslContext(), NoopHostnameVerifier.INSTANCE);
	}

	private SSLContext sslContext() throws Exception {
		ClassLoader classLoader = getClass().getClassLoader();
		InputStream input = classLoader.getResourceAsStream("jks/flowable_trust.jks");
		KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		keyStore.load(input, "haarlem023".toCharArray());
		return SSLContextBuilder.create().loadTrustMaterial(keyStore, new TrustSelfSignedStrategy()).build();
	}

	@Bean(name = "restTemplateZaakNapAPI")
	public RestTemplate restTemplateZaakNapAPI() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getInterceptors()
				.add(new BasicAuthorizationInterceptor(
						HLM.Config.get("common.zkn.restNap.basicauth.username",""),
						HLM.Config.get("common.zkn.restNap.basicauth.password","")));
		return restTemplate;
	}

	@Bean(name = "restTemplateZaakAPI")
	public RestTemplate restTemplateZaakAPI() {
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.getInterceptors()
				.add(new BasicAuthorizationInterceptor(
						HLM.Config.get("common.zkn.rest.basicauth.username",""),
						HLM.Config.get("common.zkn.rest.basicauth.password","")));
		return restTemplate;
	}

	@Bean(name = "ztcOauth2RestTemplate")
	public OAuth2RestTemplate oauth2RestTemplate() {
		ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();
		resource.setAccessTokenUri(HLM.Config.get("common.ztc.rest.access.token-uri",""));
		resource.setClientId(HLM.Config.get("common.ztc.rest.access.client-id",""));
		resource.setClientSecret(HLM.Config.get("common.ztc.rest.access.client-secret",""));

		List<String> scopes = new ArrayList<>();
		scopes.add("read");
		resource.setScope(scopes);

		return new OAuth2RestTemplate(resource);
	}
}
