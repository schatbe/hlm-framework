package org.hlm.component.dto.zkn;

import java.util.ArrayList;
import java.util.List;

import org.hlm.component.dto.zkn.types.ZaakAPIObject;

import spinjar.com.fasterxml.jackson.annotation.JsonIgnore;
import spinjar.com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Zaken {

	private List<Zaak> zaken = new ArrayList<Zaak>();

	public List<Zaak> getZaken() {
		return this.zaken;
	}

	public void setZaken(List<Zaak> zaken) {
		this.zaken = zaken;
	}

	public Zaken() {
		super();
	}

	/*
	 * Additional Constructor for ZaakAPIResponse data, i.e. List<ZaakAPIObject>
	 */
	public Zaken(List<ZaakAPIObject> zaakAPIObjects) {
		zaakAPIObjects.forEach(zaakAPIObject -> zaken.add(new Zaak(zaakAPIObject)));
	}

	@JsonIgnore
	public Zaken getIdsOnly() {
		Zaken zakenIds = new Zaken();

		this.zaken.forEach(z -> {
			Zaak zaak = new Zaak();
			zaak.setZaakidentificatie(z.getZaakidentificatie());
			zakenIds.getZaken().add(zaak);
		});

		return zakenIds;
	}
}
