
package org.hlm.component.dto.ztc.json2pojo;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Resultaattype {

    @SerializedName("archiefactietermijn")
    private int Archiefactietermijn;
    @SerializedName("archiefnominatie")
    private String Archiefnominatie;
    @SerializedName("bepaaltAfwijkendArchiefRegimeVan")
    private List<Object> BepaaltAfwijkendArchiefRegimeVan;
    @SerializedName("brondatumProcedure")
    private String BrondatumProcedure;
    @SerializedName("einddatumObject")
    private Object EinddatumObject;
    @SerializedName("heeftVerplichtDocumentype")
    private List<Object> HeeftVerplichtDocumentype;
    @SerializedName("heeftVerplichteZaakobjecttype")
    private List<Object> HeeftVerplichteZaakobjecttype;
    @SerializedName("heeftVoorBrondatumArchiefprocedureRelevante")
    private Object HeeftVoorBrondatumArchiefprocedureRelevante;
    @SerializedName("ingangsdatumObject")
    private String IngangsdatumObject;
    @SerializedName("isRelevantVoor")
    private String IsRelevantVoor;
    @SerializedName("leidtTot")
    private List<Object> LeidtTot;
    @SerializedName("omschrijving")
    private String Omschrijving;
    @SerializedName("omschrijvingGeneriek")
    private String OmschrijvingGeneriek;
    @SerializedName("selectielijstklasse")
    private String Selectielijstklasse;
    @SerializedName("toelichting")
    private Object Toelichting;
    @SerializedName("url")
    private String Url;

    public int getArchiefactietermijn() {
        return Archiefactietermijn;
    }

    public void setArchiefactietermijn(int archiefactietermijn) {
        Archiefactietermijn = archiefactietermijn;
    }

    public String getArchiefnominatie() {
        return Archiefnominatie;
    }

    public void setArchiefnominatie(String archiefnominatie) {
        Archiefnominatie = archiefnominatie;
    }

    public List<Object> getBepaaltAfwijkendArchiefRegimeVan() {
        return BepaaltAfwijkendArchiefRegimeVan;
    }

    public void setBepaaltAfwijkendArchiefRegimeVan(List<Object> bepaaltAfwijkendArchiefRegimeVan) {
        BepaaltAfwijkendArchiefRegimeVan = bepaaltAfwijkendArchiefRegimeVan;
    }

    public String getBrondatumProcedure() {
        return BrondatumProcedure;
    }

    public void setBrondatumProcedure(String brondatumProcedure) {
        BrondatumProcedure = brondatumProcedure;
    }

    public Object getEinddatumObject() {
        return EinddatumObject;
    }

    public void setEinddatumObject(Object einddatumObject) {
        EinddatumObject = einddatumObject;
    }

    public List<Object> getHeeftVerplichtDocumentype() {
        return HeeftVerplichtDocumentype;
    }

    public void setHeeftVerplichtDocumentype(List<Object> heeftVerplichtDocumentype) {
        HeeftVerplichtDocumentype = heeftVerplichtDocumentype;
    }

    public List<Object> getHeeftVerplichteZaakobjecttype() {
        return HeeftVerplichteZaakobjecttype;
    }

    public void setHeeftVerplichteZaakobjecttype(List<Object> heeftVerplichteZaakobjecttype) {
        HeeftVerplichteZaakobjecttype = heeftVerplichteZaakobjecttype;
    }

    public Object getHeeftVoorBrondatumArchiefprocedureRelevante() {
        return HeeftVoorBrondatumArchiefprocedureRelevante;
    }

    public void setHeeftVoorBrondatumArchiefprocedureRelevante(Object heeftVoorBrondatumArchiefprocedureRelevante) {
        HeeftVoorBrondatumArchiefprocedureRelevante = heeftVoorBrondatumArchiefprocedureRelevante;
    }

    public String getIngangsdatumObject() {
        return IngangsdatumObject;
    }

    public void setIngangsdatumObject(String ingangsdatumObject) {
        IngangsdatumObject = ingangsdatumObject;
    }

    public String getIsRelevantVoor() {
        return IsRelevantVoor;
    }

    public void setIsRelevantVoor(String isRelevantVoor) {
        IsRelevantVoor = isRelevantVoor;
    }

    public List<Object> getLeidtTot() {
        return LeidtTot;
    }

    public void setLeidtTot(List<Object> leidtTot) {
        LeidtTot = leidtTot;
    }

    public String getOmschrijving() {
        return Omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        Omschrijving = omschrijving;
    }

    public String getOmschrijvingGeneriek() {
        return OmschrijvingGeneriek;
    }

    public void setOmschrijvingGeneriek(String omschrijvingGeneriek) {
        OmschrijvingGeneriek = omschrijvingGeneriek;
    }

    public String getSelectielijstklasse() {
        return Selectielijstklasse;
    }

    public void setSelectielijstklasse(String selectielijstklasse) {
        Selectielijstklasse = selectielijstklasse;
    }

    public Object getToelichting() {
        return Toelichting;
    }

    public void setToelichting(Object toelichting) {
        Toelichting = toelichting;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String printResultaattype() {
        return "Resultaattype{" +
                "Archiefactietermijn=" + Archiefactietermijn +
                ", Archiefnominatie='" + Archiefnominatie + '\'' +
                ", BepaaltAfwijkendArchiefRegimeVan=" + BepaaltAfwijkendArchiefRegimeVan +
                ", BrondatumProcedure='" + BrondatumProcedure + '\'' +
                ", EinddatumObject=" + EinddatumObject +
                ", HeeftVerplichtDocumentype=" + HeeftVerplichtDocumentype +
                ", HeeftVerplichteZaakobjecttype=" + HeeftVerplichteZaakobjecttype +
                ", HeeftVoorBrondatumArchiefprocedureRelevante=" + HeeftVoorBrondatumArchiefprocedureRelevante +
                ", IngangsdatumObject='" + IngangsdatumObject + '\'' +
                ", IsRelevantVoor='" + IsRelevantVoor + '\'' +
                ", LeidtTot=" + LeidtTot +
                ", Omschrijving='" + Omschrijving + '\'' +
                ", OmschrijvingGeneriek='" + OmschrijvingGeneriek + '\'' +
                ", Selectielijstklasse='" + Selectielijstklasse + '\'' +
                ", Toelichting=" + Toelichting +
                ", Url='" + Url + '\'' +
                '}';
    }
}
