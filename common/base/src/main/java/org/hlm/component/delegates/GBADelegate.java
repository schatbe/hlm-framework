package org.hlm.component.delegates;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.hlm.component.dao.GBAClientImpl;
import org.hlm.component.dto.gba.Persoon;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("gbaDelegate")
public class GBADelegate implements JavaDelegate {

	private final Logger log = LoggerFactory.getLogger(GBADelegate.class);

	@Autowired
	private GBAClientImpl gbaClient;

	@Override
	public void execute(DelegateExecution execution) {
		log.info("Start ophalen gegevens GBA");

		String bsn = (String) execution.getVariable("bsn");

		Persoon persoon = gbaClient.ophalenGegevens(bsn);
		log.info("Persoon: " + persoon.getGeslachtsnaam());

		execution.setVariable("persoon", persoon);

		log.info("Einde Ophalen Gegevens GBA");

	}

}
