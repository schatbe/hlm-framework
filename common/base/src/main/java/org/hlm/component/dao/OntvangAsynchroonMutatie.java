package org.hlm.component.dao;

import org.hlm.component.dto.zkn.Besluit;
import org.hlm.component.dto.zkn.Document;
import org.hlm.component.dto.zkn.Zaak;

public interface OntvangAsynchroonMutatie {

	Zaak creeerZaak(Zaak zaak);
	void actualiseerZaakstatus(Zaak zaak);
	void voegZaakDocumentToe(Document document);
	void updateZaak(Zaak zaak);
	void voegBesluitToe(Besluit besluit);
}