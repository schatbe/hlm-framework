
package org.hlm.component.dto.ztc.json2pojo;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Results<T> {

    @SerializedName("results")
    private List<T> Results;
    @Expose
    private org.hlm.component.dto.ztc.json2pojo._links _links;

    public List<T> getResults() {
        return (List<T>) Results;
    }

    public void setResults(List<T> results) {
        Results = results;
    }

    public org.hlm.component.dto.ztc.json2pojo._links get_links() {
        return _links;
    }

    public void set_links(org.hlm.component.dto.ztc.json2pojo._links _links) {
        this._links = _links;
    }

}