package org.hlm.component.delegates;

import java.time.LocalDate;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.hlm.component.dto.zkn.Zaak;
import org.hlm.component.dto.ztc.json2pojo.Resultaattype;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ArchiefactiedatumDelegate implements JavaDelegate {

	private final Logger log = LoggerFactory.getLogger(ArchiefactiedatumDelegate.class);

	public void execute(DelegateExecution execution) throws Exception {

		Zaak zaak = (Zaak) execution.getVariable("zaak");
		Resultaattype resultaattype = (Resultaattype) execution.getVariable("resultaattype");

		log.info("Start Archiefactiedatum berekenen");

		int archiefactietermijn = resultaattype.getArchiefactietermijn();
		LocalDate einddatum = zaak.getEinddatumLocalDate();

		// set archiefactiedatum: einddatum + archiefactietermijn (in months)
		LocalDate archiefactiedatum = einddatum.plusMonths(archiefactietermijn);
		zaak.setArchiefactiedatum(archiefactiedatum);

		execution.setVariable("zaak", zaak);

		System.out
				.println("******************************** ArchiefactiedatumDelegate ********************************");
		System.out.println("Einddatum: " + einddatum);
		System.out.println("Archiefactietermijn: " + archiefactietermijn);
		System.out.println("Archiefactiedatum: " + archiefactiedatum);
		System.out
				.println("*******************************************************************************************");

		log.info("Einde Archiefactiedatum berekenen");
	}
}
