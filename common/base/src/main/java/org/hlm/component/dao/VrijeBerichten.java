package org.hlm.component.dao;

import org.hlm.component.dto.zkn.Besluit;
import org.hlm.component.dto.zkn.Zaak;

public interface VrijeBerichten {

	Zaak ophalenZaaknummer();
	String ophalenDocumentnummer();
	Besluit genereerBesluitIdentificatie();
}
