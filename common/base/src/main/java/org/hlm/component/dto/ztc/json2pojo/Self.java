
package org.hlm.component.dto.ztc.json2pojo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Self {

    @SerializedName("href")
    private String Href;

    public String getHref() {
        return Href;
    }

    public void setHref(String href) {
        Href = href;
    }

}
