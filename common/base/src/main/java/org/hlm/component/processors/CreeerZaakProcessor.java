package org.hlm.component.processors;

import io.datatree.Tree;
import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.hlm.component.dao.OntvangAsynchroonMutatie;
import org.hlm.component.dao.VrijeBerichten;
import org.hlm.component.dto.gba.Persoon;
import org.hlm.component.dto.zkn.Zaak;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreeerZaakProcessor {

    private final OntvangAsynchroonMutatie asynchroonClient;
    private final VrijeBerichten vrijBerichtClient;
    private final Logger log = LoggerFactory.getLogger(CreeerZaakProcessor.class);

    @Autowired
    public CreeerZaakProcessor(OntvangAsynchroonMutatie asynchroonClient, VrijeBerichten vrijBerichtClient) {
        this.asynchroonClient = asynchroonClient;
        this.vrijBerichtClient = vrijBerichtClient;
    }

    @Handler
    public void process(Exchange exchange) {
        log.info("Start " + this.getClass().getSimpleName());

        int zaaktypeidentificatie = 0;
        String omschrijving = null, toelichting = null, uitvoerendeOEidentificatie = null, initiatorOEidentificatie = null, zaakIdFull = null;

        try {
            // get body from microservice call
            Tree treeBody = new Tree(exchange.getIn().getBody(String.class));
            System.out.println("ExchangeBody TreeJson: " + treeBody.toString("json", true));

            // set variables that are defined in Modeler (Connector - Input Parameters - payload (script) )
            if (treeBody.get("zaak.zaaktypeidentificatie").asInteger() != 0) zaaktypeidentificatie = treeBody.get("zaak.zaaktypeidentificatie").asInteger();
            if (treeBody.get("zaak.omschrijving") != null) omschrijving = treeBody.get("zaak.omschrijving").asString();
            if (treeBody.get("zaak.toelichting") != null) toelichting = treeBody.get("zaak.toelichting").asString();
            if (treeBody.get("zaak.uitvoerendeOEidentificatie") != null) uitvoerendeOEidentificatie = treeBody.get("zaak.uitvoerendeOEidentificatie").asString();
            if (treeBody.get("zaak.initiatorOEidentificatie") != null) initiatorOEidentificatie = treeBody.get("zaak.initiatorOEidentificatie").asString();

            Zaak zaak;

            // set optional variables from the StartProcess variables, that are set from Camunda API call or (start)form
            if (treeBody.get("zaak.zaakIdFull") != null) zaakIdFull = treeBody.get("zaak.zaakIdFull").asString();

            // only use zaakIdFull (created in startProcess.html) for first case if multiple cases are created
            // TODO: loopCounter check
            // && treeBody.get("process.processLoopCounter") != null && treeBody.get("process.processLoopCounter").asInteger() == 0
            if (zaakIdFull != null) {
                zaak = new Zaak();
                zaak.setZaakidentificatie(zaakIdFull);
            } else zaak = vrijBerichtClient.ophalenZaaknummer();
            // update treeBody with zaakId
            treeBody.put("zaak.zaakidentificatie", zaak.getZaakidentificatie());

            Persoon persoon;

            if (treeBody.get("persoon") != null) {
                persoon = new Persoon();
                if (treeBody.get("persoon.bsn") != null) persoon.setBsn(treeBody.get("persoon.bsn").asString());
                if (treeBody.get("persoon.geslachtsnaam") != null) persoon.setGeslachtsnaam(treeBody.get("persoon.geslachtsnaam").asString());
                if (treeBody.get("persoon.voorvoegselGeslachtsnaam") != null) persoon.setVoorletters(treeBody.get("persoon.voorvoegselGeslachtsnaam").asString());
                if (treeBody.get("persoon.voorletters") != null) persoon.setVoorletters(treeBody.get("persoon.voorletters").asString());
                if (treeBody.get("persoon.geboortedatum") != null) persoon.setGeboortedatum(treeBody.get("persoon.geboortedatum").asString());
                zaak.setPersoon(persoon);
            }

//            Persoon persoon1 = new Persoon();
//            persoon1.setVoorletters("M.M.");
//            persoon1.setVoorvoegselGeslachtsnaam("de");
//            persoon1.setGeslachtsnaam("Mora");
//            persoon1.setGeboortedatum("20001212");
//            zaak.setPersoon(persoon1);

            zaak.setZaaktypeidentificatie(zaaktypeidentificatie);
            zaak.setOmschrijving(omschrijving);
            zaak.setToelichting(toelichting);
            zaak.setUitvoerendeOEidentificatie(uitvoerendeOEidentificatie);
            zaak.setInitiatorOEidentificatie(initiatorOEidentificatie);

            Zaak zaakCreated = asynchroonClient.creeerZaak(zaak);
            // startdatum created in creeerZaak, updating treeBody
            treeBody.put("zaak.startdatum", zaakCreated.getStartdatum());
            System.out.println("ZaakObject created: " + zaakCreated.toJSON());

            // hardcoded! for ontheffing straat optredens
            zaakCreated.setStatustypeVolgnummer(601L);
            zaakCreated.setStatustypeomschrijving("In behandeling");

            treeBody.put("zaak.statustypevolgnummer", zaakCreated.getStatustypeVolgnummer());
            treeBody.put("zaak.statustypeomschrijving", zaakCreated.getStatustypeomschrijving());

            asynchroonClient.actualiseerZaakstatus(zaakCreated);

            exchange.getOut().setBody(treeBody.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        log.debug("einde " + this.getClass().getSimpleName());
    }
}
