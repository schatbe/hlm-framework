package org.hlm.component.delegates;

import java.util.Map;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.hlm.component.dao.Beantwoordvraag;
import org.hlm.component.dao.OntvangAsynchroonMutatie;
import org.hlm.component.dao.VrijeBerichten;
import org.hlm.component.dao.Ztc;
import org.hlm.component.dto.zkn.Zaak;
import org.hlm.component.dto.ztc.json2pojo.Statustype;
import org.hlm.component.dto.ztc.json2pojo.Zaaktype;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreeerZaakDelegate implements JavaDelegate {

	private final Logger log = LoggerFactory.getLogger(CreeerZaakDelegate.class);

	private final OntvangAsynchroonMutatie asynchroonClient;
	private final VrijeBerichten vrijBerichtClient;
	private final Beantwoordvraag beantwoordvraagClient;
	private final Ztc ztc;

	@Autowired
	public CreeerZaakDelegate(OntvangAsynchroonMutatie asynchroonClient, VrijeBerichten vrijBerichtClient, Beantwoordvraag beantwoordvraagClient, Ztc ztc) {
		this.asynchroonClient = asynchroonClient;
		this.vrijBerichtClient = vrijBerichtClient;
		this.beantwoordvraagClient = beantwoordvraagClient;
		this.ztc = ztc;
	}

	public void execute(DelegateExecution execution) {
		log.info("Start CreeerZaak");

		Map creeerZaakInput = (Map) execution.getVariable("creeerZaakInput");

		Zaak zaak = vrijBerichtClient.ophalenZaaknummer();

		if (creeerZaakInput.containsKey("zaaktypeidentificatie")) {
			zaak.setZaaktypeidentificatie(Integer.parseInt(creeerZaakInput.get("zaaktypeidentificatie").toString()));
		}

		if (creeerZaakInput.containsKey("initiatorOEidentificatie")) {
			zaak.setInitiatorOEidentificatie(creeerZaakInput.get("initiatorOEidentificatie").toString());
		}

		if (creeerZaakInput.containsKey("uitvoerendeOEidentificatie")) {
			zaak.setUitvoerendeOEidentificatie(creeerZaakInput.get("uitvoerendeOEidentificatie").toString());
		}

		Zaaktype zaaktype = ztc.getZaaktype(ztc.getCatalogus("HLM"), String.valueOf(zaak.getZaaktypeidentificatie()));

		// verantwoordelijke uit ZTC halen ?
		// einddatum_gepland uit ZTC berekenen (of null laten?)

		if (creeerZaakInput.containsKey("omschrijving")) {
			zaak.setOmschrijving(creeerZaakInput.get("omschrijving").toString());
		} else {
			zaak.setOmschrijving(zaaktype.getOmschrijving());
		}

		if (creeerZaakInput.containsKey("toelichting")) {
			zaak.setToelichting(creeerZaakInput.get("toelichting").toString());
		} else {
			zaak.setToelichting(zaaktype.getToelichting());
		}

		Zaak zaakC = asynchroonClient.creeerZaak(zaak);

		// get first statustype from ZTC and make call to 'actualiseerZaakstatus'
		Statustype statustype = ztc.getFirstStatustype(zaaktype);
		zaakC.setStatustypevolgnummer(statustype.getVolgnummer());
		zaakC.setStatustypeomschrijving(statustype.getOmschrijving());
		asynchroonClient.actualiseerZaakstatus(zaakC);

		System.out.println(zaakC.toJSON());
		execution.setVariable("zaak", zaakC);

		log.info("Einde CreeerZaak");
	}
}