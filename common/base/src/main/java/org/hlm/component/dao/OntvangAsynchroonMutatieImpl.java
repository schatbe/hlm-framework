package org.hlm.component.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import nl.egem.stuf.sector.zkn.*;
import org.hlm.common.utils.StUFUtils;
import org.hlm.common.utils.XMLUtils;
import org.hlm.component.dto.gba.Persoon;
import org.hlm.component.dto.zkn.Besluit;
import org.hlm.component.dto.zkn.Document;
import org.hlm.component.dto.zkn.Zaak;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

@Service
public class OntvangAsynchroonMutatieImpl implements OntvangAsynchroonMutatie {

    private final WebServiceTemplate zknWebServiceTemplate;
    private Zaak zaakC;
    private final ZaakNapAPI zaakNapAPI;

    @Autowired
    public OntvangAsynchroonMutatieImpl(WebServiceTemplate zknWebServiceTemplate, ZaakNapAPI zaakNapAPI) {
        this.zknWebServiceTemplate = zknWebServiceTemplate;
        this.zaakNapAPI = zaakNapAPI;
    }

    public Zaak creeerZaak(Zaak zaak) {
        zaakC = zaak;
        ObjectFactory factory = new ObjectFactory();

        CreeerZaakZAKLk01 zakLk01 = factory.createCreeerZaakZAKLk01();
        zakLk01.setStuurgegevens(getStuurgegevens(factory));
        zakLk01.setParameters(getParams(factory));
        zakLk01.setObject(getKennisgeving(factory, zaak));

        QName qName = new QName("http://www.stufstandaarden.nl/koppelvlak/zds0120", "creeerZaak_ZakLk01");
        JAXBElement<CreeerZaakZAKLk01> root = new JAXBElement<>(qName, CreeerZaakZAKLk01.class, zakLk01);

        XMLUtils.printXML(root);
        zknWebServiceTemplate.marshalSendAndReceive(root);
        return zaakC;
    }

    public void updateZaak(Zaak zaak) {

        ObjectFactory factory = new ObjectFactory();

        // Stuurgegevens
        UpdateZaakZAKLk01 updateZaak = factory.createUpdateZaakZAKLk01();
        updateZaak.setStuurgegevens(getStuurgegevens(factory));

        // Parameters
        ParametersLk01 params = factory.createParametersLk01();
        params.setMutatiesoort(Mutatiesoort.W);
        params.setIndicatorOvername(IndicatorOvername.V);
        updateZaak.setParameters(params);

        // Object 1
        UpdateZaakZAKKennisgeving object1 = factory.createUpdateZaakZAKKennisgeving();
        ZaakIdentificatieE zaaknummer = factory.createZaakIdentificatieE();
        zaaknummer.setValue(zaak.getZaakidentificatie());
        object1.setIdentificatie(zaaknummer);
        object1.setEntiteittype("ZAK");
        object1.setVerwerkingssoort(Verwerkingssoort.W);

        updateZaak.getObject().add(object1);

        // Object 2
        UpdateZaakZAKKennisgeving object2 = factory.createUpdateZaakZAKKennisgeving();
        object2.setIdentificatie(zaaknummer);
        object2.setEntiteittype("ZAK");
        object2.setVerwerkingssoort(Verwerkingssoort.W);

        DatumE archiefactiedatum = factory.createDatumE();
        archiefactiedatum.setValue(zaak.getArchiefactiedatumBigDecimal());
        JAXBElement<DatumE> archiefactiedatumEJAXBElement = factory
                .createUpdateZaakZAKKennisgevingDatumVernietigingDossier(archiefactiedatum);
        object2.setDatumVernietigingDossier(archiefactiedatumEJAXBElement);

        updateZaak.getObject().add(object2);

        // print UpdateZaakZAKLk01 SOAP request
        QName qName = new QName("http://www.stufstandaarden.nl/koppelvlak/zds0120", "updateZaak_ZakLk01");
        JAXBElement<UpdateZaakZAKLk01> root = new JAXBElement<>(qName, UpdateZaakZAKLk01.class, updateZaak);
        XMLUtils.printXML(root);

        zknWebServiceTemplate.marshalSendAndReceive(root);
    }

    public void actualiseerZaakstatus(Zaak zaak) {
        ObjectFactory factory = new ObjectFactory();

        ActualiseerZaakstatusZAKLk01 zaakstatus = factory.createActualiseerZaakstatusZAKLk01();

        ZAKStuurgegevensLk01 stuurgegevens = getStuurgegevens(factory);
        zaakstatus.setStuurgegevens(stuurgegevens);

        ParametersLk01 params = getParams(factory);
        zaakstatus.setParameters(params);

        List<ActualiseerZaakstatusZAKKennisgeving> actualiseerZaakstatusZAKKennisgevingList = zaakstatus.getObject();
        ActualiseerZaakstatusZAKSTTKennisgeving heeft = getHeeft(factory, zaak, actualiseerZaakstatusZAKKennisgevingList);

        ActualiseerZaakstatusZAKKennisgeving object = factory.createActualiseerZaakstatusZAKKennisgeving();
        object.setEntiteittype("ZAK");
        object.setVerwerkingssoort(Verwerkingssoort.W);

        ZaakIdentificatieE zaakIdentificatieE = factory.createZaakIdentificatieE();
        zaakIdentificatieE.setValue(zaak.getZaakidentificatie());
        object.setIdentificatie(zaakIdentificatieE);

        actualiseerZaakstatusZAKKennisgevingList.add(object);
        actualiseerZaakstatusZAKKennisgevingList.add(object);

        List<ActualiseerZaakstatusZAKSTTKennisgeving> actualiseerZaakstatusZAKSTTKennisgevingList = object.getHeeft();

        actualiseerZaakstatusZAKSTTKennisgevingList.add(heeft);

        QName qName = new QName("http://www.stufstandaarden.nl/koppelvlak/zds0120", "actualiseerZaakstatus_ZakLk01");
        JAXBElement<ActualiseerZaakstatusZAKLk01> root = new JAXBElement<ActualiseerZaakstatusZAKLk01>(qName,
                ActualiseerZaakstatusZAKLk01.class, zaakstatus);

        XMLUtils.printXML(root);
        zknWebServiceTemplate.marshalSendAndReceive(root);
    }

    public void voegZaakDocumentToe(Document document) {
        ObjectFactory factory = new ObjectFactory();

        VoegZaakdocumentToeEDCLk01 voegZaakdocumentToeEDCLk01 = factory.createVoegZaakdocumentToeEDCLk01();
        voegZaakdocumentToeEDCLk01.setStuurgegevens(getEDCStuurgegevensLk01(factory));

        ParametersLk01 params = getParams(factory);
        voegZaakdocumentToeEDCLk01.setParameters(params);
        VoegZaakdocumentToeEDCKennisgeving voegZaakdocumentToeEDCKennisgeving = factory
                .createVoegZaakdocumentToeEDCKennisgeving();

        voegZaakdocumentToeEDCLk01.setObject(voegZaakdocumentToeEDCKennisgeving);
        voegZaakdocumentToeEDCKennisgeving.setEntiteittype("EDC");
        voegZaakdocumentToeEDCKennisgeving.setVerwerkingssoort(Verwerkingssoort.T);

        DocumentIdentificatieE identificatie = factory.createDocumentIdentificatieE();
        identificatie.setValue(document.getIdentificatie());
        voegZaakdocumentToeEDCKennisgeving.setIdentificatie(identificatie);

        OmschrijvingE omschrijving = factory.createOmschrijvingE();
        omschrijving.setValue(document.getOmschrijving());
        voegZaakdocumentToeEDCKennisgeving.setDctOmschrijving(omschrijving);

        DatumE creatieDatum = factory.createDatumE();
        creatieDatum.setExact(true);
        creatieDatum.setIndOnvolledigeDatum(IndOnvolledigeDatum.V);
        creatieDatum.setNoValue(NoValue.GEEN_WAARDE);
        creatieDatum.setValue(new BigDecimal(document.getCreatiedatum().getTimeInMillis()));
        voegZaakdocumentToeEDCKennisgeving.setCreatiedatum(creatieDatum);

        DatumE ontvangstDatum = factory.createDatumE();
        ontvangstDatum.setExact(true);
        ontvangstDatum.setIndOnvolledigeDatum(IndOnvolledigeDatum.V);
        ontvangstDatum.setNoValue(NoValue.GEEN_WAARDE);
        ontvangstDatum.setValue(new BigDecimal(document.getCreatiedatum().getTimeInMillis()));
        JAXBElement<DatumE> jOntvangstDatum = factory.createEDCGdiOntvangstdatum(ontvangstDatum);
        voegZaakdocumentToeEDCKennisgeving.setOntvangstdatum(jOntvangstDatum);

        DocumentTitelE titel = factory.createDocumentTitelE();
        titel.setValue(document.getTitel());
        voegZaakdocumentToeEDCKennisgeving.setTitel(titel);

        BestandsformaatE formaat = factory.createBestandsformaatE();
        formaat.setValue(document.getFormaat());
        voegZaakdocumentToeEDCKennisgeving.setFormaat(formaat);

        DocumentTaalE taal = factory.createDocumentTaalE();
        taal.setValue(document.getTaal());
        voegZaakdocumentToeEDCKennisgeving.setTaal(taal);

        DocumentVersieE versie = factory.createDocumentVersieE();
        versie.setValue(document.getVersie());
        JAXBElement<DocumentVersieE> jVersie = factory.createEDCAntwoordGzdbVersie(versie);
        voegZaakdocumentToeEDCKennisgeving.setVersie(jVersie);

        DocumentStatusE status = factory.createDocumentStatusE();
        status.setValue(document.getStatus());
        JAXBElement<DocumentStatusE> jStatus = factory.createEDCAntwoordGzdbStatus(status);
        voegZaakdocumentToeEDCKennisgeving.setStatus(jStatus);

        VertrouwelijkAanduidingE vertrouwelijkAanduidingE = factory.createVertrouwelijkAanduidingE();
        vertrouwelijkAanduidingE.setValue(VertrouwelijkAanduiding.OPENBAAR);
        voegZaakdocumentToeEDCKennisgeving.setVertrouwelijkAanduiding(vertrouwelijkAanduidingE);

        DocumentAuteurE auteur = factory.createDocumentAuteurE();
        auteur.setValue(document.getAuteur());
        voegZaakdocumentToeEDCKennisgeving.setAuteur(auteur);

        BinaireInhoud inhoud = factory.createBinaireInhoud();
        inhoud.setValue(document.getInhoud().getBytes());
        inhoud.setBestandsnaam(document.getTitel());
        inhoud.setContentType(document.getContentType());
        voegZaakdocumentToeEDCKennisgeving.setInhoud(inhoud);

        List<VoegZaakdocumentToeEDCZAKKennisgeving> isRelevantVoorList = voegZaakdocumentToeEDCKennisgeving
                .getIsRelevantVoor();

        VoegZaakdocumentToeEDCZAKKennisgeving isRelevantVoor = factory.createVoegZaakdocumentToeEDCZAKKennisgeving();
        isRelevantVoor.setEntiteittype("EDCZAK");
        isRelevantVoor.setVerwerkingssoort(Verwerkingssoort.T);
        VoegZaakdocumentToeZAKKerngegevensKennisgeving gerelateerde = factory
                .createVoegZaakdocumentToeZAKKerngegevensKennisgeving();
        gerelateerde.setEntiteittype("ZAK");
        gerelateerde.setVerwerkingssoort(Verwerkingssoort.I);
        ZaakIdentificatieE zaakIdentificatieE = factory.createZaakIdentificatieE();
        zaakIdentificatieE.setValue("bla");
        gerelateerde.setIdentificatie(zaakIdentificatieE);
        isRelevantVoor.setGerelateerde(gerelateerde);

        isRelevantVoorList.add(isRelevantVoor);

        // voegZaakdocumentToeKennisgevingsList.add(voegZaakdocumentToeEDCKennisgeving);

        QName qName = new QName("http://www.stufstandaarden.nl/koppelvlak/zds0120", "voegZaakdocumentToe_EdcLk01");
        JAXBElement<VoegZaakdocumentToeEDCLk01> root = new JAXBElement<VoegZaakdocumentToeEDCLk01>(qName,
                VoegZaakdocumentToeEDCLk01.class, voegZaakdocumentToeEDCLk01);

        // List.add(getObject()

        XMLUtils.printXML(root);
        zknWebServiceTemplate.marshalSendAndReceive(root);
    }


    public void voegBesluitToe(Besluit besluit) {
        ObjectFactory objectFactory = new ObjectFactory();

        Di01VoegBesluitToe di01VoegBesluitToe = objectFactory.createDi01VoegBesluitToe();

        // Stuurgegevens
        Di01StuurgegevensVbt di01StuurgegevensVbt = objectFactory.createDi01StuurgegevensVbt();

        BerichtcodeDi01 berichtcodeDi01 = BerichtcodeDi01.DI_01;
        di01StuurgegevensVbt.setBerichtcode(berichtcodeDi01);

        Systeem systeem = objectFactory.createSysteem();
        systeem.setApplicatie("BPMNengine");
        di01StuurgegevensVbt.setZender(systeem);

        Systeem systeem1 = objectFactory.createSysteem();
        systeem1.setApplicatie("ZSH");
        systeem1.setOrganisatie("0392");
        di01StuurgegevensVbt.setOntvanger(systeem1);

        di01StuurgegevensVbt.setReferentienummer("");
        di01StuurgegevensVbt.setTijdstipBericht(StUFUtils.getStUFDateTime());

        FunctievoegBesluitToe functievoegBesluitToe = FunctievoegBesluitToe.VOEG_BESLUIT_TOE;
        di01StuurgegevensVbt.setFunctie(functievoegBesluitToe);

        di01VoegBesluitToe.setStuurgegevens(di01StuurgegevensVbt);

        // Object
        VoegBesluitToeObject voegBesluitToeObject = objectFactory.createVoegBesluitToeObject();

        // Besluit
        VoegBesluitToeBSLKennisgeving voegBesluitToeBSLKennisgeving = objectFactory.createVoegBesluitToeBSLKennisgeving();
        voegBesluitToeBSLKennisgeving.setEntiteittype("BSL");
        voegBesluitToeBSLKennisgeving.setVerwerkingssoort(Verwerkingssoort.T);

        // Identificatie
        DocumentIdentificatieE documentIdentificatieE = objectFactory.createDocumentIdentificatieE();
        documentIdentificatieE.setValue(besluit.getBesluitIdentificatie());
        voegBesluitToeBSLKennisgeving.setIdentificatie(documentIdentificatieE);

        // Omschrijving
        OmschrijvingE omschrijvingE = objectFactory.createOmschrijvingE();
        omschrijvingE.setValue(besluit.getOmschrijving());
        QName qName = new QName("http://www.egem.nl/StUF/sector/zkn/0310", "bst.omschrijving");
        JAXBElement<OmschrijvingE> omschrijvingEJAXBElement = new JAXBElement<>(qName, OmschrijvingE.class, omschrijvingE);
        voegBesluitToeBSLKennisgeving.setBstOmschrijving(omschrijvingEJAXBElement);

        // DatumBeslissing
        DatumE datumE = objectFactory.createDatumE();
        String stufDate = StUFUtils.getStUFDate();
        datumE.setValue(new BigDecimal(stufDate));
        voegBesluitToeBSLKennisgeving.setDatumBeslissing(datumE);

        // Toelichting
        ToelichtingE toelichting = objectFactory.createToelichtingE();
        toelichting.setValue(besluit.getToelichting());
        QName qName1 = new QName("http://www.egem.nl/StUF/sector/zkn/0310", "toelichting");
        JAXBElement<ToelichtingE> toelichtingEJAXBElement = new JAXBElement<ToelichtingE>(qName1, ToelichtingE.class, toelichting);
        voegBesluitToeBSLKennisgeving.setToelichting(toelichtingEJAXBElement);

        // IngangsdatumWerking
        DatumE datumE1 = objectFactory.createDatumE();
        datumE1.setValue(new BigDecimal(besluit.getIngangsdatum()));
        voegBesluitToeBSLKennisgeving.setIngangsdatumWerking(datumE1);

        // EinddatumWerking
        DatumE datumE2 = objectFactory.createDatumE();
        datumE2.setValue(new BigDecimal(besluit.getVervaldatum()));
        JAXBElement<DatumE> datumEJAXBElement = objectFactory.createBSLBasisEinddatumWerking(datumE2);
        voegBesluitToeBSLKennisgeving.setEinddatumWerking(datumEJAXBElement);

        // Vervalreden
        VervalredenE vervalredenE = objectFactory.createVervalredenE();
        vervalredenE.setValue(Vervalreden.BESLUIT_MET_TIJDELIJKE_WERKING);
        JAXBElement<VervalredenE> vervalredenEJAXBElement = objectFactory.createBSLBasisVervalreden(vervalredenE);
        voegBesluitToeBSLKennisgeving.setVervalreden(vervalredenEJAXBElement);

        // TijdvakGeldigheid
        TijdvakGeldigheid tijdvakGeldigheid = objectFactory.createTijdvakGeldigheid();
        TijdstipMetIndicator tijdstipMetIndicator = objectFactory.createTijdstipMetIndicator();
        tijdstipMetIndicator.setValue(besluit.getBeginGeldigheid());
        tijdvakGeldigheid.setBeginGeldigheid(tijdstipMetIndicator);

        TijdstipMetIndicator tijdstipMetIndicator1 = objectFactory.createTijdstipMetIndicator();
        tijdstipMetIndicator1.setValue(besluit.getEindGeldigheid());
        tijdvakGeldigheid.setEindGeldigheid(tijdstipMetIndicator1);

        voegBesluitToeBSLKennisgeving.setTijdvakGeldigheid(tijdvakGeldigheid);

        voegBesluitToeObject.setBesluit(voegBesluitToeBSLKennisgeving);

        // Zaak
        VoegBesluitToeZAKKerngegevensKennisgeving voegBesluitToeZAKKerngegevensKennisgeving = objectFactory.createVoegBesluitToeZAKKerngegevensKennisgeving();
        voegBesluitToeZAKKerngegevensKennisgeving.setEntiteittype("ZAK");
        voegBesluitToeZAKKerngegevensKennisgeving.setVerwerkingssoort(Verwerkingssoort.I);
        ZaakIdentificatieE zaakIdentificatieE = objectFactory.createZaakIdentificatieE();
        zaakIdentificatieE.setValue(besluit.getZaakidentificatie());
        voegBesluitToeZAKKerngegevensKennisgeving.setIdentificatie(zaakIdentificatieE);

        voegBesluitToeObject.setZaak(voegBesluitToeZAKKerngegevensKennisgeving);

        di01VoegBesluitToe.setObject(voegBesluitToeObject);

        QName qName2 = new QName("http://www.stufstandaarden.nl/koppelvlak/zds0120", "voegBesluitToe_Di01");
        JAXBElement<Di01VoegBesluitToe> root = new JAXBElement<>(qName2, Di01VoegBesluitToe.class, di01VoegBesluitToe);

        XMLUtils.printXML(root);
        zknWebServiceTemplate.marshalSendAndReceive(root);
    }


    private ActualiseerZaakstatusZAKSTTKennisgeving getHeeft(ObjectFactory factory, Zaak zaak, List<ActualiseerZaakstatusZAKKennisgeving> statusLijst) {

        ActualiseerZaakstatusZAKSTTKennisgeving heeft = factory.createActualiseerZaakstatusZAKSTTKennisgeving();
        heeft.setEntiteittype("ZAK");
        heeft.setVerwerkingssoort(Verwerkingssoort.T);

        ActualiseerZaakstatusSTTKerngegevensKennisgeving gerelateerde = factory
                .createActualiseerZaakstatusSTTKerngegevensKennisgeving();
        gerelateerde.setEntiteittype("");
        gerelateerde.setVerwerkingssoort(Verwerkingssoort.T);

        CodeE codeE = factory.createCodeE();
        codeE.setValue(zaak.getZaaktypeidentificatie().toString());
        JAXBElement<CodeE> codeEJAXBElement = factory.createSTTBasisZktCode(codeE);
        gerelateerde.setZktCode(codeEJAXBElement);

        VolgnummerE volgorde = factory.createVolgnummerE();
        volgorde.setValue(BigInteger.valueOf(zaak.getStatustypeVolgnummer()));
        gerelateerde.setVolgnummer(volgorde);

        OmschrijvingE omschrijving = factory.createOmschrijvingE();
        omschrijving.setValue(zaak.getStatustypeomschrijving());
        gerelateerde.setOmschrijving(omschrijving);

        heeft.setGerelateerde(gerelateerde);

        TijdstipE tijdstip = factory.createTijdstipE();
        tijdstip.setValue(StUFUtils.getStUFDateTime());
        heeft.setDatumStatusGezet(tijdstip);

        ActualiseerZaakstatusZAKSTTBTRKennisgeving isGezetDoor = factory.createActualiseerZaakstatusZAKSTTBTRKennisgeving();
        isGezetDoor.setEntiteittype("");
        isGezetDoor.setVerwerkingssoort(Verwerkingssoort.T);

        ActualiseerZaakstatusBTRSubtypeMDWOEHKerngegevensKennisgeving gerelateerde2 = factory.createActualiseerZaakstatusBTRSubtypeMDWOEHKerngegevensKennisgeving();
        ActualiseerZaakstatusOEHKerngegevensKennisgeving organisatorischeEenheid = factory.createActualiseerZaakstatusOEHKerngegevensKennisgeving();
        organisatorischeEenheid.setEntiteittype("");
        organisatorischeEenheid.setVerwerkingssoort(Verwerkingssoort.I);

        IdentificatieE identificatie = factory.createIdentificatieE();
        identificatie.setValue(zaak.getInitiatorOEidentificatie());
        organisatorischeEenheid.setIdentificatie(identificatie);

        QName qName = new QName("http://www.egem.nl/StUF/sector/zkn/0310", "organisatorischeEenheid");
        JAXBElement<ActualiseerZaakstatusOEHKerngegevensKennisgeving> organisatorischeEenheidJ = new JAXBElement<ActualiseerZaakstatusOEHKerngegevensKennisgeving>(qName, ActualiseerZaakstatusOEHKerngegevensKennisgeving.class, organisatorischeEenheid);
        gerelateerde2.setOrganisatorischeEenheid(organisatorischeEenheidJ);
        isGezetDoor.setGerelateerde(gerelateerde2);

        heeft.setIsGezetDoor(isGezetDoor);

        return heeft;
    }

    private CreeerZaakZAKKennisgeving getKennisgeving(ObjectFactory factory, Zaak zaak) {

        // Object
        CreeerZaakZAKKennisgeving kennisgeving = factory.createCreeerZaakZAKKennisgeving();
        kennisgeving.setEntiteittype("ZAK");
        kennisgeving.setVerwerkingssoort(Verwerkingssoort.T);

        // Identificatie
        ZaakIdentificatieE id = factory.createZaakIdentificatieE();
        id.setValue(zaak.getZaakidentificatie());
        kennisgeving.setIdentificatie(id);

        // Omschrijving
        OmschrijvingE omschrijving = factory.createOmschrijvingE();
        omschrijving.setValue(zaak.getOmschrijving());
        QName qName = new QName("http://www.egem.nl/StUF/sector/zkn/0310", "omschrijving");
        JAXBElement<OmschrijvingE> jomschrijving = new JAXBElement<OmschrijvingE>(qName, OmschrijvingE.class, omschrijving);
        kennisgeving.setOmschrijving(jomschrijving);

        // Toelichting
        ToelichtingE toelichting = factory.createToelichtingE();
        toelichting.setValue(zaak.getToelichting());
        QName qName2 = new QName("http://www.egem.nl/StUF/sector/zkn/0310", "toelichting");
        JAXBElement<ToelichtingE> jtoelichting = new JAXBElement<ToelichtingE>(qName2, ToelichtingE.class, toelichting);
        kennisgeving.setToelichting(jtoelichting);

        Boolean isNAP = false;
        // anderZaakObject (Niet Authentiek Persoon)
        if (zaak.getPersoon().getBsn() == null || zaak.getPersoon().getBsn().equals("") || zaak.getPersoon().getBsn().equals("null")) {
            // Niet authentiek persoon (ander zaakobject)

            // GUID toevoegen aan registratie -> anderZaakObject
            String guid = zaakNapAPI.postContactpersoonGetGUID(zaak);

            // Omschrijving
            ZaakobjectGrp zaakobjectGrp = factory.createZaakobjectGrp();
            OmschrijvingE omschrijvingE = factory.createOmschrijvingE();
            omschrijvingE.setValue("contactpersoon");
            zaakobjectGrp.setOmschrijving(omschrijvingE);

            // Aanduiding
            OmschrijvingE omschrijvingE1 = factory.createOmschrijvingE();
            omschrijvingE1.setValue("contactpersoon");
            JAXBElement<OmschrijvingE> omschrijvingEJAXBElement = factory.createZaakobjectGrpAanduiding(omschrijvingE1);
            zaakobjectGrp.setAanduiding(omschrijvingEJAXBElement);

            // Registratie
            ZaakobjectRegistratieE zaakobjectRegistratieE = factory.createZaakobjectRegistratieE();
            zaakobjectRegistratieE.setValue(guid);
            JAXBElement<ZaakobjectRegistratieE> zaakobjectRegistratieEJAXBElement = factory.createZaakobjectGrpRegistratie(zaakobjectRegistratieE);
            zaakobjectGrp.setRegistratie(zaakobjectRegistratieEJAXBElement);
            kennisgeving.getAnderZaakObject().add(zaakobjectGrp);

            isNAP = true;
        }

        // Startdatum
        DatumE datum = factory.createDatumE();
        String stufDate = StUFUtils.getStUFDate();
        datum.setValue(new BigDecimal(stufDate));
        kennisgeving.setStartdatum(datum);

        // zaakC update
        zaakC.setStartdatum(stufDate);

        // Registratiedatum
        kennisgeving.setRegistratiedatum(datum);

//        QName qName3 = new QName("http://www.egem.nl/StUF/sector/zkn/0310", "einddatumGepland");
//        JAXBElement<DatumE> jd = new JAXBElement<DatumE>(qName3, DatumE.class, datum);
//        kennisgeving.setEinddatumGepland(jd);

//        IndicatieE ie = factory.createIndicatieE();
//        ie.setValue(Indicatie.J);
//        QName qName4 = new QName("http://www.egem.nl/StUF/sector/zkn/0310", "archiefnominatie");
//        JAXBElement<IndicatieE> ji = new JAXBElement<IndicatieE>(qName4, IndicatieE.class, ie);
//        kennisgeving.setArchiefnominatie(ji);

        // Zaakniveau
        ZaakNiveauE niveau = factory.createZaakNiveauE();
        niveau.setValue(1);
        kennisgeving.setZaakniveau(niveau);

        // DeelzakenIndicatie
        IndicatieE die = factory.createIndicatieE();
        die.setValue(Indicatie.N);
        kennisgeving.setDeelzakenIndicatie(die);

        // isVan
        CreeerZaakZAKZKTKennisgeving isVan = factory.createCreeerZaakZAKZKTKennisgeving();
        isVan.setEntiteittype("ZAKZKT");
        isVan.setVerwerkingssoort(Verwerkingssoort.T);

        CreeerZaakZKTKerngegevensKennisgeving gerelateerde1 = factory.createCreeerZaakZKTKerngegevensKennisgeving();
        gerelateerde1.setEntiteittype("ZKT");
        gerelateerde1.setVerwerkingssoort(Verwerkingssoort.I);

        // Code
        CodeE code = factory.createCodeE();
        code.setValue(zaak.getZaaktypeidentificatie().toString());
        gerelateerde1.setCode(code);

        isVan.setGerelateerde(gerelateerde1);
        kennisgeving.setIsVan(isVan);

        // Initiator
        CreeerZaakZAKBTRINIKennisgeving heeftAlsInitiator = factory.createCreeerZaakZAKBTRINIKennisgeving();
        heeftAlsInitiator.setEntiteittype("ZAKBTRINI");
        heeftAlsInitiator.setVerwerkingssoort(Verwerkingssoort.T);

        CreeerZaakZBTRKerngegevensKennisgeving gerelateerde = factory.createCreeerZaakZBTRKerngegevensKennisgeving();

        // natuurlijkPersoon
        if (zaak.getPersoon().getBsn() != null && !zaak.getPersoon().getBsn().equals("") && !zaak.getPersoon().getBsn().equals("null")) {
            Persoon persoon = zaak.getPersoon();
            CreeerZaakNPSZknKerngegevensKennisgeving creeerZaakNPSZknKerngegevensKennisgeving = factory.createCreeerZaakNPSZknKerngegevensKennisgeving();
            creeerZaakNPSZknKerngegevensKennisgeving.setEntiteittype("NPS");
            creeerZaakNPSZknKerngegevensKennisgeving.setVerwerkingssoort(Verwerkingssoort.T);

            // BSN
            BSNE bsne = factory.createBSNE();
            bsne.setValue(persoon.getBsn());
            creeerZaakNPSZknKerngegevensKennisgeving.setInpBsn(bsne);

            // Voorletters
            VoorlettersE voorlettersE = factory.createVoorlettersE();
            voorlettersE.setValue(persoon.getVoorletters());
            JAXBElement<VoorlettersE> voorlettersEJAXBElement = factory.createNPSKerngegevensVoorletters(voorlettersE);
            creeerZaakNPSZknKerngegevensKennisgeving.setVoorletters(voorlettersEJAXBElement);

            // VoorvoegselGeslachtsnaam
            VoorvoegselGeslachtsnaamE voorvoegselGeslachtsnaamE = factory.createVoorvoegselGeslachtsnaamE();
            voorvoegselGeslachtsnaamE.setValue(persoon.getVoorvoegselGeslachtsnaam());
            JAXBElement<VoorvoegselGeslachtsnaamE> voorvoegselGeslachtsnaamEJAXBElement = factory.createNPSKerngegevensVoorvoegselGeslachtsnaam(voorvoegselGeslachtsnaamE);
            creeerZaakNPSZknKerngegevensKennisgeving.setVoorvoegselGeslachtsnaam(voorvoegselGeslachtsnaamEJAXBElement);

            // Geslachtsnaam
            GeslachtsnaamE geslachtsnaamE = factory.createGeslachtsnaamE();
            geslachtsnaamE.setValue(persoon.getGeslachtsnaam());
            JAXBElement<GeslachtsnaamE> geslachtsnaamEJAXBElement = factory.createNPSKerngegevensGeslachtsnaam(geslachtsnaamE);
            creeerZaakNPSZknKerngegevensKennisgeving.setGeslachtsnaam(geslachtsnaamEJAXBElement);

            // Geboortedatum
            DatumMetIndicator datumMetIndicator = factory.createDatumMetIndicator();
            datumMetIndicator.setValue(new BigDecimal(persoon.getGeboortedatum()));
            JAXBElement<DatumMetIndicator> datumMetIndicatorJAXBElement = factory.createNPSKerngegevensGeboortedatum(datumMetIndicator);
            creeerZaakNPSZknKerngegevensKennisgeving.setGeboortedatum(datumMetIndicatorJAXBElement);

            JAXBElement<CreeerZaakNPSZknKerngegevensKennisgeving> creeerZaakNPSZknKerngegevensKennisgevingJAXBElement = factory.createCreeerZaakZBTRKerngegevensKennisgevingNatuurlijkPersoon(creeerZaakNPSZknKerngegevensKennisgeving);
            gerelateerde.setNatuurlijkPersoon(creeerZaakNPSZknKerngegevensKennisgevingJAXBElement);
            heeftAlsInitiator.setGerelateerde(gerelateerde);

        } else if (isNAP) {
            // Niet authentiek persoon (ander zaakobject)? Initiator is OEH

            CreeerZaakOEHKerngegevensKennisgeving creeerZaakOEHKerngegevensKennisgeving = factory.createCreeerZaakOEHKerngegevensKennisgeving();
            creeerZaakOEHKerngegevensKennisgeving.setEntiteittype("OEH");
            creeerZaakOEHKerngegevensKennisgeving.setVerwerkingssoort(Verwerkingssoort.I);

            IdentificatieE i = factory.createIdentificatieE();
            i.setValue(zaak.getInitiatorOEidentificatie());
            creeerZaakOEHKerngegevensKennisgeving.setIdentificatie(i);

            JAXBElement<CreeerZaakOEHKerngegevensKennisgeving> creeerZaakOEHKerngegevensKennisgevingJAXBElement = factory.createCreeerZaakZBTRKerngegevensKennisgevingOrganisatorischeEenheid(creeerZaakOEHKerngegevensKennisgeving);
            gerelateerde.setOrganisatorischeEenheid(creeerZaakOEHKerngegevensKennisgevingJAXBElement);
            heeftAlsInitiator.setGerelateerde(gerelateerde);
        }

//        QName qName9 = new QName("http://www.egem.nl/StUF/sector/zkn/0310", "organisatorischeEenheid");
//        JAXBElement<CreeerZaakOEHKerngegevensKennisgeving> natuurlijkPersoonJ = new JAXBElement<CreeerZaakOEHKerngegevensKennisgeving>(qName9, CreeerZaakOEHKerngegevensKennisgeving.class, creeerZaakOEHKerngegevensKennisgeving);
//        gerelateerde.setNatuurlijkPersoon(natuurlijkPersoonJ);
//        gerelateerde.setOrganisatorischeEenheid(natuurlijkPersoonJ);
//        heeftAlsInitiator.setGerelateerde(gerelateerde);

        kennisgeving.setHeeftAlsInitiator(heeftAlsInitiator);

        return kennisgeving;
    }

    private ParametersLk01 getParams(ObjectFactory factory) {
        ParametersLk01 params = factory.createParametersLk01();
        params.setIndicatorOvername(IndicatorOvername.V);
        params.setMutatiesoort(Mutatiesoort.T);
        return params;
    }

    private ZAKStuurgegevensLk01 getStuurgegevens(ObjectFactory factory) {
        ZAKStuurgegevensLk01 stuurgegevens = factory.createZAKStuurgegevensLk01();

        stuurgegevens.setBerichtcode(BerichtcodeLk01.LK_01);
        Systeem ontvanger = factory.createSysteem();
        ontvanger.setOrganisatie("0392");
        ontvanger.setApplicatie("ZSH");
        stuurgegevens.setOntvanger(ontvanger);

        Systeem zender = factory.createSysteem();
        zender.setOrganisatie("0392");
        zender.setApplicatie("BPMengine");
        stuurgegevens.setZender(zender);

        stuurgegevens.setReferentienummer(java.util.UUID.randomUUID().toString());
        stuurgegevens.setTijdstipBericht(StUFUtils.getStUFDateTime());
        stuurgegevens.setEntiteittype(EntiteittypeZAK.ZAK);

        return stuurgegevens;
    }

    private EDCStuurgegevensLk01 getEDCStuurgegevensLk01(ObjectFactory factory) {

        EDCStuurgegevensLk01 stuurgegevens = factory.createEDCStuurgegevensLk01();

        stuurgegevens.setBerichtcode(BerichtcodeLk01.LK_01);
        Systeem ontvanger = factory.createSysteem();
        ontvanger.setOrganisatie("0392");
        ontvanger.setApplicatie("ZSH");
        stuurgegevens.setOntvanger(ontvanger);

        Systeem zender = factory.createSysteem();
        zender.setOrganisatie("0392");
        zender.setApplicatie("HBS");
        stuurgegevens.setZender(zender);

        stuurgegevens.setReferentienummer(java.util.UUID.randomUUID().toString());
        stuurgegevens.setTijdstipBericht(StUFUtils.getStUFDateTime());
        stuurgegevens.setEntiteittype(EntiteittypeEDC.EDC);

        return stuurgegevens;
    }

    // private ZAKStuurgegevensLk01 getStuurGegevens(ObjectFactory factory) {
    // ZAKStuurgegevensLk01 stuurgegevens = factory.createZAKStuurgegevensLk01();
    // stuurgegevens.setBerichtcode(BerichtcodeLk01.LK_01);
    //
    // Systeem zender = factory.createSysteem();
    // zender.setOrganisatie("0392");
    // zender.setApplicatie("BPMengine");
    // stuurgegevens.setZender(zender);
    //
    // Systeem ontvanger = factory.createSysteem();
    // ontvanger.setOrganisatie("0392");
    // ontvanger.setApplicatie("ZSH");
    // stuurgegevens.setOntvanger(ontvanger);
    //
    // stuurgegevens.setReferentienummer(java.util.UUID.randomUUID().toString());
    // stuurgegevens.setTijdstipBericht(getStUFDateTime());
    // stuurgegevens.setEntiteittype(EntiteittypeZAK.ZAK);
    // return stuurgegevens;
    // }
}
