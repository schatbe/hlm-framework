package org.hlm.component.dao;

import org.hlm.component.dto.zkn.Besluiten;
import org.hlm.component.dto.zkn.Filter;
import org.hlm.component.dto.zkn.Zaak;
import org.hlm.component.dto.zkn.Zaken;
import org.hlm.component.dto.zkn.types.BesluitAPIResponse;
import org.hlm.component.dto.zkn.types.ZaakAPIResponse;
import org.hlm.framework.HLM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ZaakAPI {

	private static final Logger log = LoggerFactory.getLogger(ZaakAPI.class);
	private RestTemplate restTemplateZaakAPI;

	private static String restAPIURI = HLM.Config.get("common.zkn.rest.api-uri","");

	@Autowired
	public ZaakAPI(RestTemplate restTemplateZaakAPI) {
	    this.restTemplateZaakAPI = restTemplateZaakAPI;
	}

    public <T> T get(String query, Class<T> responseType) {
        return restTemplateZaakAPI.getForObject(restAPIURI + query, responseType);
    }

    private <T> T post(String query, String inputJSON, Class<T> responseType) {
        restTemplateZaakAPI.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(inputJSON, headers);

        ResponseEntity<T> response = restTemplateZaakAPI.exchange(restAPIURI + query,
                HttpMethod.POST,
                entity, responseType);
        return response.getBody();
    }

    private <T> ResponseEntity<T> put(String query, String inputJSON, Class<T> responseType) {
        restTemplateZaakAPI.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>(inputJSON, headers);

        return restTemplateZaakAPI.exchange(restAPIURI + query, HttpMethod.PUT, entity, responseType);
    }

    public Zaken getZaken(Filter filter) {
        log.info("filter: " + filter.toJSON());
        String inputJSON = "{\"filter\":"+filter.toJSON()+'}';
        ZaakAPIResponse response = post("zaken", inputJSON, ZaakAPIResponse.class);
        return new Zaken(response.getData());
    }

    public String updateZaken(Filter filter, Zaak zaakTemplate) {
        log.info("filter      : " + filter.toJSON());
        log.info("zaakTemplate: " + zaakTemplate.toJSON());
        String inputJSON = "{\"filter\":"+filter.toJSON()+",\"template\":"+zaakTemplate.toJSON()+'}';
        log.info("updateInput : " + inputJSON);

        ResponseEntity<String> response = put("updatezaken", inputJSON, String.class);
        return response.toString();
    }

    public Zaken getZakenArchiefActieDatum() {
        ZaakAPIResponse response = get("zakenarchiefactiedatum", ZaakAPIResponse.class);
        return new Zaken(response.getData());
    }

    public String putUpdateZaakAAD(String zaakId, String inputJSON) {
        ResponseEntity<String> response = put("updatezaakaad/" + zaakId, inputJSON, String.class);
        return response.toString();
    }

    public Besluiten getBesluiten(Filter filter) {
        log.info("filter: " + filter.toJSON());
        String inputJSON = "{\"filter\":"+filter.toJSON()+'}';
        BesluitAPIResponse response = post("besluiten", inputJSON, BesluitAPIResponse.class);
        return new Besluiten(response.getData());
    }
}
