
package org.hlm.component.dto.ztc.json2pojo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ProductDienst {

    @SerializedName("link")
    private Object Link;
    @SerializedName("naam")
    private String Naam;

    public Object getLink() {
        return Link;
    }

    public void setLink(Object link) {
        Link = link;
    }

    public String getNaam() {
        return Naam;
    }

    public void setNaam(String naam) {
        Naam = naam;
    }

}
