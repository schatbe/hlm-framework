package org.hlm.component.delegates;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.hlm.component.dao.Beantwoordvraag;
import org.hlm.component.dto.zkn.Zaak;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GeefZaakdetailsDelegate implements JavaDelegate {

	private final Logger log = LoggerFactory.getLogger(GeefZaakdetailsDelegate.class);

	@Autowired
	private Beantwoordvraag beantwoordvraagClient;

	public void execute(DelegateExecution execution) {

		// Get element from list allZaken.getZaken() (see modeler)
		Zaak elementZaak = (Zaak) execution.getVariable("elementZaak");

		log.info("Start GeefZaakdetails");

		Zaak zaak1 = beantwoordvraagClient.geefZaakdetails(elementZaak);

		execution.setVariable("zaak", zaak1);

		System.out.println("******************************** GeefZaakdetailsDelegate ********************************");
		System.out.println(zaak1.toJSON());
		System.out.println("*****************************************************************************************");

		log.info("Einde GeefZaakdetails");
	}
}
