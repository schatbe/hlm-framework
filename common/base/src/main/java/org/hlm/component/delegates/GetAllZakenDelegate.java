package org.hlm.component.delegates;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.hlm.component.dao.ZaakAPI;
import org.hlm.component.dto.zkn.Filter;
import org.hlm.component.dto.zkn.Zaak;
import org.hlm.component.dto.zkn.Zaken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GetAllZakenDelegate implements JavaDelegate {

	private final Logger log = LoggerFactory.getLogger(GetAllZakenDelegate.class);

	private final ZaakAPI zaakAPI;

    @Autowired
	public GetAllZakenDelegate(ZaakAPI zaakAPI) {
        this.zaakAPI = zaakAPI;
    }

    public void execute(DelegateExecution execution) throws Exception {

		log.info("Start getAllZaken");

		Filter filter = Filter.AND(Filter.IS_NULL("archiefactiedatum"), Filter.NOT_NULL("resultaatomschrijving"),
				Filter.NOT_NULL("einddatum"));

		Zaken zakenIds = zaakAPI.getZaken(filter).getIdsOnly();

		execution.setVariable("allZaken", zakenIds);

		System.out.println("******************************** GetAllZakenDelegate ********************************");
		for (Zaak zaakje : zakenIds.getZaken()) {
			System.out.print(zaakje.getZaakidentificatie() + ", ");
		}
		System.out.println("*************************************************************************************");

		// execution.setVariable("zaken",
		// Variables.objectValue(zaken2).serializationDataFormat(Variables.SerializationDataFormats.JAVA).create());

		log.info("Einde getAllZaken");
	}
}
