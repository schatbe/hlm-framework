package org.hlm.component.dao;

import javax.xml.bind.JAXBElement;

import nl.egem.stuf.sector.bg.*;
import org.hlm.component.dto.gba.Persoon;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

@Service
public class GBAClientImpl implements GBAClient {

	private final WebServiceTemplate gbaWebServiceTemplate;

	@Autowired
	public GBAClientImpl(WebServiceTemplate gbaWebServiceTemplate) {
		this.gbaWebServiceTemplate = gbaWebServiceTemplate;
	}

	public Persoon ophalenGegevens(String bsn) {

		NpsLv01 request = composeOphalenGegevensRequest(bsn);
		NpsLa01 response = (NpsLa01) gbaWebServiceTemplate.marshalSendAndReceive(request);
		return composeOphalenGegevensResponse(response);
	}

	private NpsLv01 composeOphalenGegevensRequest(String bsn) {
		ObjectFactory factory = new ObjectFactory();
		NpsLv01 request = factory.createNpsLv01();

		request.setStuurgegevens(ophalenStuurgegevens(factory));
		request.setGelijk(getVraagGegevens(factory, bsn));

		request.setScope(getScope(factory));

		return request;
	}

	private NPSStuurgegevensLv01 ophalenStuurgegevens(ObjectFactory stufFactory) {
		// STUUR GEGEGEVENS
		//ObjectFactory stufFactory = new ObjectFactory();
		NPSStuurgegevensLv01 stuurgegevensLv01 = stufFactory.createNPSStuurgegevensLv01();

		Systeem systeemZender = stufFactory.createSysteem();
		systeemZender.setOrganisatie("0392");
		systeemZender.setApplicatie("BPMN");

		Systeem systeemOntvanger = stufFactory.createSysteem();
		systeemOntvanger.setOrganisatie("0392");
		systeemOntvanger.setApplicatie("ESB");

		stuurgegevensLv01.setZender(systeemZender);

		return stuurgegevensLv01;

	}

	private NPSVraagSelectie getVraagGegevens(ObjectFactory factory, String bsn) {
		NPSVraagSelectie npsVraagSelectie = factory.createNPSVraagSelectie();

		BSNE bsne = factory.createBSNE();
		bsne.setValue(bsn);

		npsVraagSelectie.getInpBsnAndAuthentiekOrAnpIdentificatie().add(bsne);

		return npsVraagSelectie;

	}

	private NpsLv01.Scope getScope(ObjectFactory factory) {
		NpsLv01.Scope scope = factory.createNpsLv01Scope();

		NPSVraagScope vraagScope = factory.createNPSVraagScope();

		BSNE bsn = factory.createBSNE();
		vraagScope.getInpBsnAndAuthentiekOrAnpIdentificatie().add(bsn);

		vraagScope.setVoorvoegselGeslachtsnaam(factory.createNPSVraagScopeVoorvoegselGeslachtsnaam(factory.createVoorvoegselGeslachtsnaamE()));
		vraagScope.setVoorletters(factory.createNPSVraagScopeVoorletters(factory.createVoorlettersE()));
		vraagScope.setVoornamen(factory.createNPSVraagScopeVoornamen(factory.createVoornamenE()));
		vraagScope.setGeslachtsnaam(factory.createNPSVraagScopeGeslachtsnaam(factory.createGeslachtsnaamE()));
		vraagScope.setGeboortedatum(factory.createNPSVraagScopeGeboortedatum(factory.createDatumMetIndicator()));

		// verblijfadres
		VerblijfsadresGrpVraag verblijfsadresGrpVraag = factory.createVerblijfsadresGrpVraag();

		WoonplaatsNaamE woonplaatsNaamE = factory.createWoonplaatsNaamE();
		JAXBElement<WoonplaatsNaamE> woonplaatsNaamEJAXBElement = factory.createVerblijfsadresGrpVraagWplWoonplaatsNaam(woonplaatsNaamE);
		verblijfsadresGrpVraag.setWplWoonplaatsNaam(woonplaatsNaamEJAXBElement);

		StraatnaamE straatnaamE = factory.createStraatnaamE();
		JAXBElement<StraatnaamE> straatnaamEJAXBElement = factory.createVerblijfsadresGrpVraagGorStraatnaam(straatnaamE);
		verblijfsadresGrpVraag.setGorStraatnaam(straatnaamEJAXBElement);

		PostcodeE postcodeE = factory.createPostcodeE();
		JAXBElement<PostcodeE> postcodeEJAXBElement = factory.createVerblijfsadresGrpVraagAoaPostcode(postcodeE);
		verblijfsadresGrpVraag.setAoaPostcode(postcodeEJAXBElement);

		HuisnummeringE huisnummeringE = factory.createHuisnummeringE();
		JAXBElement<HuisnummeringE> huisnummeringEJAXBElement = factory.createVerblijfsadresGrpVraagAoaHuisnummer(huisnummeringE);
		verblijfsadresGrpVraag.setAoaHuisnummer(huisnummeringEJAXBElement);

		HuisnummertoevoegingE huisnummertoevoegingE = factory.createHuisnummertoevoegingE();
		JAXBElement<HuisnummertoevoegingE> huisnummertoevoegingEJAXBElement = factory.createVerblijfsadresGrpVraagAoaHuisnummertoevoeging(huisnummertoevoegingE);
		verblijfsadresGrpVraag.setAoaHuisnummertoevoeging(huisnummertoevoegingEJAXBElement);

		vraagScope.getVerblijfsadresOrSubVerblijfBuitenland().add(verblijfsadresGrpVraag);

		scope.setObject(vraagScope);

		return scope;
	}

	private Persoon composeOphalenGegevensResponse(NpsLa01 response) {
		Persoon persoon = new Persoon();

		if (response != null && response.getAntwoord() != null && response.getAntwoord().getObject() != null
				&& response.getAntwoord().getObject().size() > 0) {

            NpsLa01.Antwoord antwoord = response.getAntwoord();
            NPSAntwoord eersteAntwoord = antwoord.getObject().get(0);

            JAXBElement<BSNE> bsnJ = eersteAntwoord.getInpBsn();
            String bsn = bsnJ.getValue().getValue();
            persoon.setBsn(bsn);

            persoon.setVoorletters(eersteAntwoord.getVoorletters().getValue().getValue());

            JAXBElement<VoornamenE> vn = eersteAntwoord.getVoornamen();
            String voornamen = vn.getValue().getValue();
            persoon.setVoornamen(voornamen);

            JAXBElement<VoorvoegselGeslachtsnaamE> e = eersteAntwoord.getVoorvoegselGeslachtsnaam();
            String voorvoegselGeslachtsnaam = e.getValue().getValue();
            persoon.setVoorvoegselGeslachtsnaam(voorvoegselGeslachtsnaam);

            JAXBElement<GeslachtsnaamE> an = eersteAntwoord.getGeslachtsnaam();
            String geslachtsnaam = an.getValue().getValue();
            persoon.setGeslachtsnaam(geslachtsnaam);

            JAXBElement<DatumMetIndicator> gebdatumJ = eersteAntwoord.getGeboortedatum();
            String geboortedatum = gebdatumJ.getValue().getValue().toString();
            persoon.setGeboortedatum(geboortedatum);

            persoon.setWoonplaats(eersteAntwoord.getVerblijfsadres().getWplWoonplaatsNaam().getValue().getValue());
            persoon.setStraat(eersteAntwoord.getVerblijfsadres().getGorStraatnaam().getValue().getValue());
            persoon.setPostcode(eersteAntwoord.getVerblijfsadres().getAoaPostcode().getValue().getValue());
            persoon.setHuisnummer(eersteAntwoord.getVerblijfsadres().getAoaHuisnummer().getValue().getValue());
            persoon.setHuisnummertoevoeging(eersteAntwoord.getVerblijfsadres().getAoaHuisnummertoevoeging().getValue().getValue());

            return persoon;
		} else {
		    // TODO: throw exception?
            return persoon;
        }
	}
}
