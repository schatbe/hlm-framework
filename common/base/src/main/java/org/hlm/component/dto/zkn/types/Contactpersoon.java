
package org.hlm.component.dto.zkn.types;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Contactpersoon {

    @SerializedName("emailadres")
    private String Emailadres;
    @SerializedName("geboortedatum")
    private String Geboortedatum;
    @SerializedName("geslachtsnaam")
    private String Geslachtsnaam;
    @SerializedName("guid")
    private String Guid;
    @SerializedName("id")
    private Long Id;
    @SerializedName("telefoonnummer")
    private String Telefoonnummer;
    @SerializedName("voorletters")
    private String Voorletters;
    @SerializedName("voorvoegsel")
    private String Voorvoegsel;
    @SerializedName("zaakidentificatie")
    private String Zaakidentificatie;

    public String getEmailadres() {
        return Emailadres;
    }

    public void setEmailadres(String emailadres) {
        Emailadres = emailadres;
    }

    public String getGeboortedatum() {
        return Geboortedatum;
    }

    public void setGeboortedatum(String geboortedatum) {
        Geboortedatum = geboortedatum;
    }

    public String getGeslachtsnaam() {
        return Geslachtsnaam;
    }

    public void setGeslachtsnaam(String geslachtsnaam) {
        Geslachtsnaam = geslachtsnaam;
    }

    public String getGuid() {
        return Guid;
    }

    public void setGuid(String guid) {
        Guid = guid;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getTelefoonnummer() {
        return Telefoonnummer;
    }

    public void setTelefoonnummer(String telefoonnummer) {
        Telefoonnummer = telefoonnummer;
    }

    public String getVoorletters() {
        return Voorletters;
    }

    public void setVoorletters(String voorletters) {
        Voorletters = voorletters;
    }

    public String getVoorvoegsel() {
        return Voorvoegsel;
    }

    public void setVoorvoegsel(String voorvoegsel) {
        Voorvoegsel = voorvoegsel;
    }

    public String getZaakidentificatie() {
        return Zaakidentificatie;
    }

    public void setZaakidentificatie(String zaakidentificatie) {
        Zaakidentificatie = zaakidentificatie;
    }

}
