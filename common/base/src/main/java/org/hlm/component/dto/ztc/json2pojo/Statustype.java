
package org.hlm.component.dto.ztc.json2pojo;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Statustype {

    @SerializedName("checklistitem")
    private List<Object> Checklistitem;
    @SerializedName("doorlooptijd")
    private Object Doorlooptijd;
    @SerializedName("einddatumObject")
    private Object EinddatumObject;
    @SerializedName("heeftVerplichteEigenschap")
    private List<Object> HeeftVerplichteEigenschap;
    @SerializedName("heeftVerplichteInformatieobjecttype")
    private List<Object> HeeftVerplichteInformatieobjecttype;
    @SerializedName("heeftVerplichteZaakObjecttype")
    private List<Object> HeeftVerplichteZaakObjecttype;
    @SerializedName("informeren")
    private String Informeren;
    @SerializedName("ingangsdatumObject")
    private String IngangsdatumObject;
    @SerializedName("isVan")
    private String IsVan;
    @SerializedName("omschrijving")
    private String Omschrijving;
    @SerializedName("omschrijvingGeneriek")
    private String OmschrijvingGeneriek;
    @SerializedName("statustekst")
    private Object Statustekst;
    @SerializedName("toelichting")
    private Object Toelichting;
    @SerializedName("url")
    private String Url;
    @SerializedName("volgnummer")
    private Long Volgnummer;

    public List<Object> getChecklistitem() {
        return Checklistitem;
    }

    public void setChecklistitem(List<Object> checklistitem) {
        Checklistitem = checklistitem;
    }

    public Object getDoorlooptijd() {
        return Doorlooptijd;
    }

    public void setDoorlooptijd(Object doorlooptijd) {
        Doorlooptijd = doorlooptijd;
    }

    public Object getEinddatumObject() {
        return EinddatumObject;
    }

    public void setEinddatumObject(Object einddatumObject) {
        EinddatumObject = einddatumObject;
    }

    public List<Object> getHeeftVerplichteEigenschap() {
        return HeeftVerplichteEigenschap;
    }

    public void setHeeftVerplichteEigenschap(List<Object> heeftVerplichteEigenschap) {
        HeeftVerplichteEigenschap = heeftVerplichteEigenschap;
    }

    public List<Object> getHeeftVerplichteInformatieobjecttype() {
        return HeeftVerplichteInformatieobjecttype;
    }

    public void setHeeftVerplichteInformatieobjecttype(List<Object> heeftVerplichteInformatieobjecttype) {
        HeeftVerplichteInformatieobjecttype = heeftVerplichteInformatieobjecttype;
    }

    public List<Object> getHeeftVerplichteZaakObjecttype() {
        return HeeftVerplichteZaakObjecttype;
    }

    public void setHeeftVerplichteZaakObjecttype(List<Object> heeftVerplichteZaakObjecttype) {
        HeeftVerplichteZaakObjecttype = heeftVerplichteZaakObjecttype;
    }

    public String getInformeren() {
        return Informeren;
    }

    public void setInformeren(String informeren) {
        Informeren = informeren;
    }

    public String getIngangsdatumObject() {
        return IngangsdatumObject;
    }

    public void setIngangsdatumObject(String ingangsdatumObject) {
        IngangsdatumObject = ingangsdatumObject;
    }

    public String getIsVan() {
        return IsVan;
    }

    public void setIsVan(String isVan) {
        IsVan = isVan;
    }

    public String getOmschrijving() {
        return Omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        Omschrijving = omschrijving;
    }

    public String getOmschrijvingGeneriek() {
        return OmschrijvingGeneriek;
    }

    public void setOmschrijvingGeneriek(String omschrijvingGeneriek) {
        OmschrijvingGeneriek = omschrijvingGeneriek;
    }

    public Object getStatustekst() {
        return Statustekst;
    }

    public void setStatustekst(Object statustekst) {
        Statustekst = statustekst;
    }

    public Object getToelichting() {
        return Toelichting;
    }

    public void setToelichting(Object toelichting) {
        Toelichting = toelichting;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public Long getVolgnummer() {
        return Volgnummer;
    }

    public void setVolgnummer(Long volgnummer) {
        Volgnummer = volgnummer;
    }

}
