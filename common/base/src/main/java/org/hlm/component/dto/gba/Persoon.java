package org.hlm.component.dto.gba;

import org.apache.commons.lang.StringUtils;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Persoon {

    private String bsn;
    private String voorletters;
    private String voornamen;
    private String voorvoegselGeslachtsnaam;
    private String geslachtsnaam;
    private String geboortedatum;
    private String geboortedatumDisplay;
    private String straat;
    private int huisnummer;
    private String huisnummertoevoeging;
    private String postcode;
    private String woonplaats;
    private String fullLastName;
    private Date date;

    private SimpleDateFormat formatterGeboortedatum = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat formatterGeboortedatumDisplay = new SimpleDateFormat("dd-MM-yyyy");

    public String getGeboortedatum() {
        return geboortedatum;
    }
    public void setGeboortedatum(String geboortedatum) {
        this.geboortedatum = geboortedatum;

        if (this.geboortedatum != null) {
            String yyyy = StringUtils.substring(this.geboortedatum, 0, 4);
            String mm = StringUtils.substring(this.geboortedatum, 4, 6);
            String dd = StringUtils.substring(this.geboortedatum, 6, 8);
            this.geboortedatumDisplay = dd + "-" + mm + "-" + yyyy;
        }
    }

    public String getGeboortedatumDisplay() {
        return geboortedatumDisplay;
    }
    public void setGeboortedatumDisplay(String geboortedatumDisplay) {
        this.geboortedatumDisplay = geboortedatumDisplay;
    }

    public String getVoorvoegselGeslachtsnaam() {
        return voorvoegselGeslachtsnaam;
    }
    public void setVoorvoegselGeslachtsnaam(String voorvoegselGeslachtsnaam) {
        this.voorvoegselGeslachtsnaam = voorvoegselGeslachtsnaam;
    }

    public String getBsn() {
        return bsn;
    }
    public void setBsn(String bsn) {
        this.bsn = bsn;
    }

    public String getVoornamen() {
        return voornamen;
    }
    public void setVoornamen(String voornamen) {
        this.voornamen = voornamen;
    }

    public String getGeslachtsnaam() {
        return geslachtsnaam;
    }
    public void setGeslachtsnaam(String geslachtsnaam) {
        this.geslachtsnaam = geslachtsnaam;
        String lastName;
        if (this.voorvoegselGeslachtsnaam == null || this.voorvoegselGeslachtsnaam.equals("") || this.voorvoegselGeslachtsnaam.equals("null")) {
            lastName = this.geslachtsnaam;
        } else {
            lastName = this.voorvoegselGeslachtsnaam + " " + this.geslachtsnaam;
        }
        this.fullLastName = lastName;
    }

    public String getVoorletters() {
        return voorletters;
    }
    public void setVoorletters(String voorletters) {
        this.voorletters = voorletters;
    }

    public String getStraat() {
        return straat;
    }
    public void setStraat(String straat) {
        this.straat = straat;
    }

    public int getHuisnummer() {
        return huisnummer;
    }
    public void setHuisnummer(int huisnummer) {
        this.huisnummer = huisnummer;
    }

    public String getHuisnummertoevoeging() {
        return huisnummertoevoeging;
    }
    public void setHuisnummertoevoeging(String huisnummertoevoeging) {
        this.huisnummertoevoeging = huisnummertoevoeging;
    }

    public String getPostcode() {
        return postcode;
    }
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getWoonplaats() {
        return woonplaats;
    }
    public void setWoonplaats(String woonplaats) {
        this.woonplaats = woonplaats;
    }

    public String getFullLastName() {
        return fullLastName;
    }
    public void setFullLastName(String fullname) {
        this.fullLastName = fullname;
    }

    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;

        if (this.date != null) {
            this.geboortedatum = formatterGeboortedatum.format(date);
            this.geboortedatumDisplay = formatterGeboortedatumDisplay.format(date);
        }
    }

    public String printPersoon() {
        return "Persoon{" +
                "bsn='" + bsn + '\'' +
                ", voorletters='" + voorletters + '\'' +
                ", voornamen='" + voornamen + '\'' +
                ", voorvoegselGeslachtsnaam='" + voorvoegselGeslachtsnaam + '\'' +
                ", geslachtsnaam='" + geslachtsnaam + '\'' +
                ", geboortedatum='" + geboortedatum + '\'' +
                ", geboortedatumDisplay='" + geboortedatumDisplay + '\'' +
                ", straat='" + straat + '\'' +
                ", huisnummer=" + huisnummer +
                ", huisnummertoevoeging='" + huisnummertoevoeging + '\'' +
                ", postcode='" + postcode + '\'' +
                ", woonplaats='" + woonplaats + '\'' +
                ", fullLastName='" + fullLastName + '\'' +
                ", date=" + date +
                '}';
    }
}
