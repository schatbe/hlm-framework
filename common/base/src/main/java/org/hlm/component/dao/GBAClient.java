package org.hlm.component.dao;

import org.hlm.component.dto.gba.Persoon;

public interface GBAClient {

	Persoon ophalenGegevens(String bsn);
}
