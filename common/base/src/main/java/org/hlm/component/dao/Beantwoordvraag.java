package org.hlm.component.dao;

import org.hlm.component.dto.zkn.Besluiten;
import org.hlm.component.dto.zkn.Zaak;

public interface Beantwoordvraag {
	Zaak geefZaakdetails(Zaak zaak);
    Besluiten geefLijstBesluiten(Zaak zaak);
}
