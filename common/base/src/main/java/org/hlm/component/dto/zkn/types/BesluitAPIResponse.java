package org.hlm.component.dto.zkn.types;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;
import java.util.List;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class BesluitAPIResponse {

    @SerializedName("data")
    private List<BesluitAPIObject> Data;
    @SerializedName("records")
    private Integer Records;
    @SerializedName("status")
    private String Status;

    public List<BesluitAPIObject> getData() {
        return Data;
    }
    public void setData(List<BesluitAPIObject> data) {
        Data = data;
    }

    public Integer getRecords() {
        return Records;
    }
    public void setRecords(Integer records) {
        Records = records;
    }

    public String getStatus() {
        return Status;
    }
    public void setStatus(String status) {
        Status = status;
    }
}
