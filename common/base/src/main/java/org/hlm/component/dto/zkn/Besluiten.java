package org.hlm.component.dto.zkn;

import org.hlm.component.dto.zkn.types.BesluitAPIObject;

import java.util.ArrayList;
import java.util.List;

public class Besluiten {

    public List<Besluit> besluiten = new ArrayList<>();

    public List<Besluit> getBesluiten() {
        return this.besluiten;
    }
    public void setBesluiten(List<Besluit> besluiten) {
        this.besluiten = besluiten;
    }

    public Besluiten() {
        super();
    }

    public Besluiten(List<BesluitAPIObject> besluitAPIObjects) {
        besluitAPIObjects.forEach(besluitAPIObject -> besluiten.add(new Besluit(besluitAPIObject)));
    }
}
