package org.hlm.component.dto.zkn;

import org.apache.commons.lang.StringUtils;
import org.hlm.common.utils.IBaseClass;
import org.hlm.component.dto.zkn.types.BesluitAPIObject;

public class Besluit extends BesluitAPIObject implements IBaseClass {

    private String besluitIdentificatie;
    private String beginGeldigheid;
    private String eindGeldigheid;

    private String besluitdatumDisplay;
    private String ingangsdatumDisplay;
    private String vervaldatumDisplay;

    public Besluit() {
        super();
    }

    public Besluit(BesluitAPIObject besluitAPIObject) {
        super();
        updateFrom(besluitAPIObject);
    }

    @Override
    public void setBesluitdatum(String besluitdatum) {
        super.setBesluitdatum(besluitdatum);
        this.besluitdatumDisplay = createDisplayDate(super.getBesluitdatum());
    }

    @Override
    public void setIngangsdatum(String ingangsdatum) {
        super.setIngangsdatum(ingangsdatum);
        this.ingangsdatumDisplay = createDisplayDate(super.getIngangsdatum());
    }

    @Override
    public void setVervaldatum(String vervaldatum) {
        super.setVervaldatum(vervaldatum);
        this.vervaldatumDisplay = createDisplayDate(super.getVervaldatum());
    }

    public String getBesluitdatumDisplay() {
        return besluitdatumDisplay;
    }
    public void setBesluitdatumDisplay(String besluitdatumDisplay) {
        this.besluitdatumDisplay = besluitdatumDisplay;
    }

    public String getIngangsdatumDisplay() {
        return ingangsdatumDisplay;
    }
    public void setIngangsdatumDisplay(String ingangsdatumDisplay) {
        this.ingangsdatumDisplay = ingangsdatumDisplay;
    }

    public String getVervaldatumDisplay() {
        return vervaldatumDisplay;
    }
    public void setVervaldatumDisplay(String vervaldatumDisplay) {
        this.vervaldatumDisplay = vervaldatumDisplay;
    }

    public String getBesluitIdentificatie() {
        return besluitIdentificatie;
    }
    public void setBesluitIdentificatie(String besluitIdentificatie) {
        this.besluitIdentificatie = besluitIdentificatie;
    }

    public String getBeginGeldigheid() {
        return beginGeldigheid;
    }
    public void setBeginGeldigheid(String beginGeldigheid) {
        this.beginGeldigheid = beginGeldigheid;
    }

    public String getEindGeldigheid() {
        return eindGeldigheid;
    }
    public void setEindGeldigheid(String eindGeldigheid) {
        this.eindGeldigheid = eindGeldigheid;
    }

    private String createDisplayDate(String inputDate) {
        String yyyy = StringUtils.substring(inputDate, 0, 4);
        String mm = StringUtils.substring(inputDate, 4, 6);
        String dd = StringUtils.substring(inputDate, 6, 8);
        return dd + "-" + mm + "-" + yyyy;
    }
}
