
package org.hlm.component.dto.zkn.types;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ZaakNapAPIResponse {

    @SerializedName("data")
    private Contactpersoon Data;
    @SerializedName("status")
    private String Status;

    public Contactpersoon getData() {
        return Data;
    }

    public void setData(Contactpersoon data) {
        Data = data;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}
