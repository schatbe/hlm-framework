package org.hlm.component.processors;

import io.datatree.Tree;
import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.hlm.component.dao.GBAClientImpl;
import org.hlm.component.dto.gba.Persoon;
import org.hlm.framework.HLM;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GBAProcessor {

    private final GBAClientImpl gbaClient;

    @Autowired
    public GBAProcessor(GBAClientImpl gbaClient) {
        this.gbaClient = gbaClient;
    }

    @Handler
    public void process(Exchange exchange) {

        try {
            Tree treeBody = new Tree(exchange.getIn().getBody(String.class));
            System.out.println("ExchangeInBody TreeJson: " + treeBody.toString("json", true));

            // get variables from process
            RestTemplate restTemplate = new RestTemplate();
            String url = "http://localhost:8080/engine-rest/engine/" + HLM.Config.get("test22.hlm.engine.name").asString() + "/process-instance/" + treeBody.get("process.processInstanceId").asString() + "/variables";
//            System.out.println("URL REQUEST: " + url);

            // Camunda API call to get variables
            if (restTemplate.getForEntity(url, String.class).getStatusCode() == HttpStatus.OK) {
                ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
                Tree result = new Tree(response.getBody());
                System.out.println("RESULT RestTemplate: " + result.toString("json", true));

                String bsn = result.get("bsn.value").asString();
                Persoon persoon = gbaClient.ophalenGegevens(bsn);
                System.out.println("Persoon GBA: " + persoon.printPersoon());

                // bsn failed
                if (persoon.getBsn() == null) {
                    exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 202);
                    exchange.getOut().setFault(true);
                    exchange.getOut().setBody("ERROR; BSN#: " + bsn + " not valid");

                    // set camunda variable BSN_ERROR
                    Tree requestTree = new Tree();
                    requestTree.put("value", true);
                    requestTree.put("type", "Boolean");

                    HttpHeaders headers = new HttpHeaders();
                    headers.setContentType(MediaType.APPLICATION_JSON);

                    HttpEntity<String> requestHttp = new HttpEntity<>(requestTree.toString(), headers);

                    // PUT /process-instance/aProcessInstanceId/variables/aVarName
                    restTemplate.exchange(url + "/BSN_ERROR", HttpMethod.PUT, requestHttp, Void.class);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
