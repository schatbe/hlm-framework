
package org.hlm.component.dto.zkn.types;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "id", "zaakidentificatie", "bronorganisatie", "omschrijving", "toelichting", "registratiedatum",
		"verantwoordelijke_organisatie", "startdatum", "einddatum", "archiefactiedatum", "archiefnominatie",
		"archiefstatus", "einddatum_gepland", "uiterlijke_einddatum_afdoening", "resultaattoelichting",
		"resultaatomschrijving", "publicatiedatum", "betalingsindicatie", "zaakgeometrie", "hoofdzaak_id",
		"zaaktypeidentificatie", "zaaktypeomschrijving", "zaaktypeomschrijving_generiek", "domein",
		"doorlooptijd_behandeling", "servicenorm_behandeling", "archiefclassificatiecode", "vertrouwelijk_aanduiding",
		"publicatie_indicatie", "trefwoord", "status_type_id", "statustypeomschrijving",
		"statustypeomschrijving_generiek", "doorlooptijd_status", "statustoelichting", "datum_status_gezet" })
public class ZaakAPIObject {

	@JsonProperty("id")
	private Integer id;
	@JsonProperty("zaakidentificatie")
	private String zaakidentificatie;
	@JsonProperty("bronorganisatie")
	private String bronorganisatie;
	@JsonProperty("omschrijving")
	private String omschrijving;
	@JsonProperty("toelichting")
	private String toelichting;
	@JsonProperty("registratiedatum")
	private String registratiedatum;
	@JsonProperty("verantwoordelijke_organisatie")
	private String verantwoordelijkeOrganisatie;
	@JsonProperty("startdatum")
	private String startdatum;
	@JsonProperty("einddatum")
	private Object einddatum;
	@JsonProperty("archiefactiedatum")
	private Object archiefactiedatum;
	@JsonProperty("archiefnominatie")
	private String archiefnominatie;
	@JsonProperty("archiefstatus")
	private String archiefstatus;
	@JsonProperty("einddatum_gepland")
	private String einddatumGepland;
	@JsonProperty("uiterlijke_einddatum_afdoening")
	private Object uiterlijkeEinddatumAfdoening;
	@JsonProperty("resultaattoelichting")
	private String resultaattoelichting;
	@JsonProperty("resultaatomschrijving")
	private String resultaatomschrijving;
	@JsonProperty("publicatiedatum")
	private Object publicatiedatum;
	@JsonProperty("betalingsindicatie")
	private Object betalingsindicatie;
	@JsonProperty("zaakgeometrie")
	private Object zaakgeometrie;
	@JsonProperty("hoofdzaak_id")
	private Object hoofdzaakId;
	@JsonProperty("zaaktypeidentificatie")
	private Integer zaaktypeidentificatie;
	@JsonProperty("zaaktypeomschrijving")
	private String zaaktypeomschrijving;
	@JsonProperty("zaaktypeomschrijving_generiek")
	private String zaaktypeomschrijvingGeneriek;
	@JsonProperty("domein")
	private String domein;
	@JsonProperty("doorlooptijd_behandeling")
	private Integer doorlooptijdBehandeling;
	@JsonProperty("servicenorm_behandeling")
	private Object servicenormBehandeling;
	@JsonProperty("archiefclassificatiecode")
	private Object archiefclassificatiecode;
	@JsonProperty("vertrouwelijk_aanduiding")
	private String vertrouwelijkAanduiding;
	@JsonProperty("publicatie_indicatie")
	private String publicatieIndicatie;
	@JsonProperty("trefwoord")
	private List<String> trefwoord = null;
	@JsonProperty("status_type_id")
	private Integer statusTypeId;
	@JsonProperty("statustypeomschrijving")
	private String statustypeomschrijving;
	@JsonProperty("statustypeomschrijving_generiek")
	private String statustypeomschrijvingGeneriek;
	@JsonProperty("statustypevolgnummer")
	private Long statustypevolgnummer;
	@JsonProperty("doorlooptijd_status")
	private Object doorlooptijdStatus;
	@JsonProperty("statustoelichting")
	private Object statustoelichting;
	@JsonProperty("datum_status_gezet")
	private String datumStatusGezet;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("id")
	public Integer getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Integer id) {
		this.id = id;
	}

	@JsonProperty("zaakidentificatie")
	public String getZaakidentificatie() {
		return zaakidentificatie;
	}

	@JsonProperty("zaakidentificatie")
	public void setZaakidentificatie(String zaakidentificatie) {
		this.zaakidentificatie = zaakidentificatie;
	}

	@JsonProperty("bronorganisatie")
	public String getBronorganisatie() {
		return bronorganisatie;
	}

	@JsonProperty("bronorganisatie")
	public void setBronorganisatie(String bronorganisatie) {
		this.bronorganisatie = bronorganisatie;
	}

	@JsonProperty("omschrijving")
	public String getOmschrijving() {
		return omschrijving;
	}

	@JsonProperty("omschrijving")
	public void setOmschrijving(String omschrijving) {
		this.omschrijving = omschrijving;
	}

	@JsonProperty("toelichting")
	public String getToelichting() {
		return toelichting;
	}

	@JsonProperty("toelichting")
	public void setToelichting(String toelichting) {
		this.toelichting = toelichting;
	}

	@JsonProperty("registratiedatum")
	public String getRegistratiedatum() {
		return registratiedatum;
	}

	@JsonProperty("registratiedatum")
	public void setRegistratiedatum(String registratiedatum) {
		this.registratiedatum = registratiedatum;
	}

	@JsonProperty("verantwoordelijke_organisatie")
	public String getVerantwoordelijkeOrganisatie() {
		return verantwoordelijkeOrganisatie;
	}

	@JsonProperty("verantwoordelijke_organisatie")
	public void setVerantwoordelijkeOrganisatie(String verantwoordelijkeOrganisatie) {
		this.verantwoordelijkeOrganisatie = verantwoordelijkeOrganisatie;
	}

	@JsonProperty("startdatum")
	public String getStartdatum() {
		return startdatum;
	}

	@JsonProperty("startdatum")
	public void setStartdatum(String startdatum) {
		this.startdatum = startdatum;
	}

	@JsonProperty("einddatum")
	public Object getEinddatum() {
		return einddatum;
	}

	@JsonProperty("einddatum")
	public void setEinddatum(Object einddatum) {
		this.einddatum = einddatum;
	}

	@JsonProperty("archiefactiedatum")
	public Object getArchiefactiedatum() {
		return archiefactiedatum;
	}

	@JsonProperty("archiefactiedatum")
	public void setArchiefactiedatum(Object archiefactiedatum) {
		this.archiefactiedatum = archiefactiedatum;
	}

	@JsonProperty("archiefnominatie")
	public String getArchiefnominatie() {
		return archiefnominatie;
	}

	@JsonProperty("archiefnominatie")
	public void setArchiefnominatie(String archiefnominatie) {
		this.archiefnominatie = archiefnominatie;
	}

	@JsonProperty("archiefstatus")
	public String getArchiefstatus() {
		return archiefstatus;
	}

	@JsonProperty("archiefstatus")
	public void setArchiefstatus(String archiefstatus) {
		this.archiefstatus = archiefstatus;
	}

	@JsonProperty("einddatum_gepland")
	public String getEinddatumGepland() {
		return einddatumGepland;
	}

	@JsonProperty("einddatum_gepland")
	public void setEinddatumGepland(String einddatumGepland) {
		this.einddatumGepland = einddatumGepland;
	}

	@JsonProperty("uiterlijke_einddatum_afdoening")
	public Object getUiterlijkeEinddatumAfdoening() {
		return uiterlijkeEinddatumAfdoening;
	}

	@JsonProperty("uiterlijke_einddatum_afdoening")
	public void setUiterlijkeEinddatumAfdoening(Object uiterlijkeEinddatumAfdoening) {
		this.uiterlijkeEinddatumAfdoening = uiterlijkeEinddatumAfdoening;
	}

	@JsonProperty("resultaattoelichting")
	public String getResultaattoelichting() {
		return resultaattoelichting;
	}

	@JsonProperty("resultaattoelichting")
	public void setResultaattoelichting(String resultaattoelichting) {
		this.resultaattoelichting = resultaattoelichting;
	}

	@JsonProperty("resultaatomschrijving")
	public String getResultaatomschrijving() {
		return resultaatomschrijving;
	}

	@JsonProperty("resultaatomschrijving")
	public void setResultaatomschrijving(String resultaatomschrijving) {
		this.resultaatomschrijving = resultaatomschrijving;
	}

	@JsonProperty("publicatiedatum")
	public Object getPublicatiedatum() {
		return publicatiedatum;
	}

	@JsonProperty("publicatiedatum")
	public void setPublicatiedatum(Object publicatiedatum) {
		this.publicatiedatum = publicatiedatum;
	}

	@JsonProperty("betalingsindicatie")
	public Object getBetalingsindicatie() {
		return betalingsindicatie;
	}

	@JsonProperty("betalingsindicatie")
	public void setBetalingsindicatie(Object betalingsindicatie) {
		this.betalingsindicatie = betalingsindicatie;
	}

	@JsonProperty("zaakgeometrie")
	public Object getZaakgeometrie() {
		return zaakgeometrie;
	}

	@JsonProperty("zaakgeometrie")
	public void setZaakgeometrie(Object zaakgeometrie) {
		this.zaakgeometrie = zaakgeometrie;
	}

	@JsonProperty("hoofdzaak_id")
	public Object getHoofdzaakId() {
		return hoofdzaakId;
	}

	@JsonProperty("hoofdzaak_id")
	public void setHoofdzaakId(Object hoofdzaakId) {
		this.hoofdzaakId = hoofdzaakId;
	}

	@JsonProperty("zaaktypeidentificatie")
	public Integer getZaaktypeidentificatie() {
		return zaaktypeidentificatie;
	}

	@JsonProperty("zaaktypeidentificatie")
	public void setZaaktypeidentificatie(Integer zaaktypeidentificatie) {
		this.zaaktypeidentificatie = zaaktypeidentificatie;
	}

	@JsonProperty("zaaktypeomschrijving")
	public String getZaaktypeomschrijving() {
		return zaaktypeomschrijving;
	}

	@JsonProperty("zaaktypeomschrijving")
	public void setZaaktypeomschrijving(String zaaktypeomschrijving) {
		this.zaaktypeomschrijving = zaaktypeomschrijving;
	}

	@JsonProperty("zaaktypeomschrijving_generiek")
	public String getZaaktypeomschrijvingGeneriek() {
		return zaaktypeomschrijvingGeneriek;
	}

	@JsonProperty("zaaktypeomschrijving_generiek")
	public void setZaaktypeomschrijvingGeneriek(String zaaktypeomschrijvingGeneriek) {
		this.zaaktypeomschrijvingGeneriek = zaaktypeomschrijvingGeneriek;
	}

	@JsonProperty("domein")
	public String getDomein() {
		return domein;
	}

	@JsonProperty("domein")
	public void setDomein(String domein) {
		this.domein = domein;
	}

	@JsonProperty("doorlooptijd_behandeling")
	public Integer getDoorlooptijdBehandeling() {
		return doorlooptijdBehandeling;
	}

	@JsonProperty("doorlooptijd_behandeling")
	public void setDoorlooptijdBehandeling(Integer doorlooptijdBehandeling) {
		this.doorlooptijdBehandeling = doorlooptijdBehandeling;
	}

	@JsonProperty("servicenorm_behandeling")
	public Object getServicenormBehandeling() {
		return servicenormBehandeling;
	}

	@JsonProperty("servicenorm_behandeling")
	public void setServicenormBehandeling(Object servicenormBehandeling) {
		this.servicenormBehandeling = servicenormBehandeling;
	}

	@JsonProperty("archiefclassificatiecode")
	public Object getArchiefclassificatiecode() {
		return archiefclassificatiecode;
	}

	@JsonProperty("archiefclassificatiecode")
	public void setArchiefclassificatiecode(Object archiefclassificatiecode) {
		this.archiefclassificatiecode = archiefclassificatiecode;
	}

	@JsonProperty("vertrouwelijk_aanduiding")
	public String getVertrouwelijkAanduiding() {
		return vertrouwelijkAanduiding;
	}

	@JsonProperty("vertrouwelijk_aanduiding")
	public void setVertrouwelijkAanduiding(String vertrouwelijkAanduiding) {
		this.vertrouwelijkAanduiding = vertrouwelijkAanduiding;
	}

	@JsonProperty("publicatie_indicatie")
	public String getPublicatieIndicatie() {
		return publicatieIndicatie;
	}

	@JsonProperty("publicatie_indicatie")
	public void setPublicatieIndicatie(String publicatieIndicatie) {
		this.publicatieIndicatie = publicatieIndicatie;
	}

	@JsonProperty("trefwoord")
	public List<String> getTrefwoord() {
		return trefwoord;
	}

	@JsonProperty("trefwoord")
	public void setTrefwoord(List<String> trefwoord) {
		this.trefwoord = trefwoord;
	}

	@JsonProperty("status_type_id")
	public Integer getStatusTypeId() {
		return statusTypeId;
	}

	@JsonProperty("status_type_id")
	public void setStatusTypeId(Integer statusTypeId) {
		this.statusTypeId = statusTypeId;
	}

	@JsonProperty("statustypeomschrijving")
	public String getStatustypeomschrijving() {
		return statustypeomschrijving;
	}

	@JsonProperty("statustypeomschrijving")
	public void setStatustypeomschrijving(String statustypeomschrijving) {
		this.statustypeomschrijving = statustypeomschrijving;
	}

	@JsonProperty("statustypeomschrijving_generiek")
	public String getStatustypeomschrijvingGeneriek() {
		return statustypeomschrijvingGeneriek;
	}

	@JsonProperty("statustypeomschrijving_generiek")
	public void setStatustypeomschrijvingGeneriek(String statustypeomschrijvingGeneriek) {
		this.statustypeomschrijvingGeneriek = statustypeomschrijvingGeneriek;
	}

	@JsonProperty("statustypevolgnummer")
	public Long getStatustypevolgnummer() {
		return statustypevolgnummer;
	}

	@JsonProperty("statustypevolgnummer")
	public void setStatustypevolgnummer(Long statustypevolgnummer) {
		this.statustypevolgnummer = statustypevolgnummer;
	}

	@JsonProperty("doorlooptijd_status")
	public Object getDoorlooptijdStatus() {
		return doorlooptijdStatus;
	}

	@JsonProperty("doorlooptijd_status")
	public void setDoorlooptijdStatus(Object doorlooptijdStatus) {
		this.doorlooptijdStatus = doorlooptijdStatus;
	}

	@JsonProperty("statustoelichting")
	public Object getStatustoelichting() {
		return statustoelichting;
	}

	@JsonProperty("statustoelichting")
	public void setStatustoelichting(Object statustoelichting) {
		this.statustoelichting = statustoelichting;
	}

	@JsonProperty("datum_status_gezet")
	public String getDatumStatusGezet() {
		return datumStatusGezet;
	}

	@JsonProperty("datum_status_gezet")
	public void setDatumStatusGezet(String datumStatusGezet) {
		this.datumStatusGezet = datumStatusGezet;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}
