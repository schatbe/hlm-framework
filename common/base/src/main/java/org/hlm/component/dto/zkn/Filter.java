package org.hlm.component.dto.zkn;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;

import org.hlm.common.utils.IBaseClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Filter implements IBaseClass {
	private final Logger log = LoggerFactory.getLogger(Filter.class);
	/* Usage:
	 *	Filter filter = Filter.AND(
	 *		Filter.LESS_THAN("archiefactiedatum", zaak.getArchiefactiedatum().toString()),
	 *		Filter.EQUAL_TO("archiefStatus", "vernietigen")
	 *	);
	 */
	
	private enum Operation {
		LESS_THAN, GREATER_THAN, LESS_THAN_OR_EQUAL, GREATER_THAN_OR_EQUAL, 
		STARTS_WITH, ENDS_WITH, CONTAINS, 
		EQUAL_TO, IS_NULL, NOT_NULL, 
		AND, OR };
	
    @JsonProperty("property")
	public String property;
    @JsonProperty("operation")
	public Operation operation;
    @JsonProperty("value")
	public String value;
    @JsonProperty("filters")
	public List<Filter> filters;
    
	public static Filter LESS_THAN(String property, String value) {
		return new Filter(Operation.LESS_THAN, property, value);
	}

	public static Filter GREATER_THAN(String property, String value) {
		return new Filter(Operation.GREATER_THAN, property, value);
	}
	
	public static Filter LESS_THAN_OR_EQUAL(String property, String value) {
		return new Filter(Operation.LESS_THAN_OR_EQUAL, property, value);
	}

	public static Filter GREATER_THAN_OR_EQUAL(String property, String value) {
		return new Filter(Operation.GREATER_THAN_OR_EQUAL, property, value);
	}
	
	public static Filter EQUAL_TO(String property, String value) {
		return new Filter(Operation.EQUAL_TO, property, value);
	}

	public static Filter STARTS_WITH(String property, String value) {
		return new Filter(Operation.STARTS_WITH, property, value);
	}
	
	public static Filter ENDS_WITH(String property, String value) {
		return new Filter(Operation.ENDS_WITH, property, value);
	}
	
	public static Filter CONTAINS(String property, String value) {
		return new Filter(Operation.CONTAINS, property, value);
	}
	
	public static Filter IS_NULL(String property) {
		return new Filter(Operation.IS_NULL,property);
	}
	
	public static Filter NOT_NULL(String property) {
		return new Filter(Operation.NOT_NULL,property);
	}

	public static Filter AND(Filter... filters) {
		return new Filter(Operation.AND, filters);
	}

	public static Filter OR(Filter... filters) {
		return new Filter(Operation.OR, filters);
	}

	private Filter(Operation operation, Filter... filters) {
		this.operation = operation;
		this.filters = Arrays.asList(filters);
	}
	
	private Filter(Operation operation, String property) {
		this.operation = operation;
		this.property = property;
	}
	
	private Filter(Operation operation, String property, String value) {
		this.operation = operation;
		this.property = property;
		this.value = value;
	}
}
