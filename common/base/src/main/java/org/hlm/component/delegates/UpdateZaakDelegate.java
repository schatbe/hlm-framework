package org.hlm.component.delegates;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.hlm.component.dao.ZaakAPI;
import org.hlm.component.dto.zkn.Filter;
import org.hlm.component.dto.zkn.Zaak;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdateZaakDelegate implements JavaDelegate {

	private final Logger log = LoggerFactory.getLogger(UpdateZaakDelegate.class);

	private final ZaakAPI zaakAPI;

	@Autowired
	public UpdateZaakDelegate(ZaakAPI zaakAPI) {
		this.zaakAPI = zaakAPI;
	}

	public void execute(DelegateExecution execution) throws Exception {

		Zaak zaak = (Zaak) execution.getVariable("zaak");

		log.info("Start Update Zaak");
		/*        
		String response = ZaakAPI.putUpdateZaakAAD(
				zaak.getZaakidentificatie(), 
				"{\"archiefactiedatum\":\"" + zaak.getArchiefactiedatum() + "\"}"
				);
		*/
		Filter filter = Filter.EQUAL_TO("zaakidentificatie", zaak.getZaakidentificatie());

		Zaak template = new Zaak();
		template.setArchiefactiedatum(zaak.getArchiefactiedatum().toString());

		String response = zaakAPI.updateZaken(filter, template);

		System.out.println("******************************** UpdateZaakDelegate ********************************");
		System.out.println("Response: " + response);
		System.out.println("************************************************************************************");

		//        asynchroonClient.updateZaak(zaak);

		log.info("Einde Update Zaak");
	}
}
