package org.hlm.component.dto.zkn;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.hlm.common.utils.IBaseClass;
import org.hlm.component.dto.gba.Persoon;
import org.hlm.component.dto.zkn.types.ZaakAPIObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spinjar.com.fasterxml.jackson.annotation.JsonIgnore;

public class Zaak extends ZaakAPIObject implements IBaseClass {
	private static DateTimeFormatter datumFormatter = DateTimeFormatter.BASIC_ISO_DATE;
	private final Logger log = LoggerFactory.getLogger(Zaak.class);

	private String initiatorOEidentificatie;
	private String uitvoerendeOEidentificatie;
	private String emailAdresInitiator;
	private Long statustypeVolgnummer;
	private Persoon persoon; // initiator
    private Besluiten besluiten;

    public Zaak() {
		super();
	}

	/*
	 * Additional Constructor from ZaakAPIObject This uses the IBaseClass default
	 * method 'updateFrom'
	 */
	public Zaak(ZaakAPIObject zaakAPIObject) {
		super();
		updateFrom(zaakAPIObject);
	}

	/*
	 * Overloading/overriding functies voor datums en dergelijke(non-Javadoc)
	 */
	@JsonIgnore()
	public LocalDate getEinddatumLocalDate() {
		if (getEinddatum() == null)
			return null;
		return LocalDate.parse((String) getEinddatum(), datumFormatter);
	}
	public void setEinddatum(LocalDate datum) {
		super.setEinddatum(datum.format(datumFormatter));
	}

	@JsonIgnore()
	public LocalDate getArchiefactiedatumLocalDate() {
		if (getArchiefactiedatum() == null)
			return null;
		return LocalDate.parse((String) getArchiefactiedatum(), datumFormatter);
	}
	public void setArchiefactiedatum(LocalDate datum) {
		super.setArchiefactiedatum(datum.format(datumFormatter));
	}
	@JsonIgnore()
	public BigDecimal getArchiefactiedatumBigDecimal() {
		if (getArchiefactiedatum() == null)
			return null;
		return new BigDecimal((String) getArchiefactiedatum());
	}

	@JsonIgnore()
	public LocalDate getUiterlijkeEinddatumAfdoeningLocalDate() {
		if (getUiterlijkeEinddatumAfdoening() == null)
			return null;
		return LocalDate.parse((String) getUiterlijkeEinddatumAfdoening(), datumFormatter);
	}
	public void setUiterlijkeEinddatumAfdoening(LocalDate datum) {
		super.setUiterlijkeEinddatumAfdoening(datum.format(datumFormatter));
	}

	@JsonIgnore()
	public LocalDate getPublicatiedatumLocalDate() {
		if (getPublicatiedatum() == null)
			return null;
		return LocalDate.parse((String) getPublicatiedatum(), datumFormatter);
	}
	public void setPublicatiedatum(LocalDate datum) {
		super.setPublicatiedatum(datum.format(datumFormatter));
	}

	public String getInitiatorOEidentificatie() {
		return initiatorOEidentificatie;
	}
	public void setInitiatorOEidentificatie(String initiatorOEidentificatie) {
		this.initiatorOEidentificatie = initiatorOEidentificatie;
	}

	public String getUitvoerendeOEidentificatie() {
		return uitvoerendeOEidentificatie;
	}
	public void setUitvoerendeOEidentificatie(String uitvoerendeOEidentificatie) {
		this.uitvoerendeOEidentificatie = uitvoerendeOEidentificatie;
	}

	public String getEmailAdresInitiator() {
		return emailAdresInitiator;
	}
	public void setEmailAdresInitiator(String email) {
		this.emailAdresInitiator = email;
	}

    public Long getStatustypeVolgnummer() {
        return statustypeVolgnummer;
    }
    public void setStatustypeVolgnummer(Long statustypeVolgnummer) {
        this.statustypeVolgnummer = statustypeVolgnummer;
    }

	public Persoon getPersoon() {
		return persoon;
	}
	public void setPersoon(Persoon persoon) {
		this.persoon = persoon;
	}

    public Besluiten getBesluiten() {
        return besluiten;
    }
    public void setBesluiten(Besluiten besluiten) {
        this.besluiten = besluiten;
    }
}
