
package org.hlm.component.dto.ztc.json2pojo;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Catalogus {

    @SerializedName("bestaatuitBesluittype")
    private List<String> BestaatuitBesluittype;
    @SerializedName("bestaatuitInformatieobjecttype")
    private List<String> BestaatuitInformatieobjecttype;
    @SerializedName("bestaatuitZaaktype")
    private List<String> BestaatuitZaaktype;
    @SerializedName("contactpersoonBeheerEmailadres")
    private String ContactpersoonBeheerEmailadres;
    @SerializedName("contactpersoonBeheerNaam")
    private String ContactpersoonBeheerNaam;
    @SerializedName("contactpersoonBeheerTelefoonnummer")
    private String ContactpersoonBeheerTelefoonnummer;
    @SerializedName("domein")
    private String Domein;
    @SerializedName("rsin")
    private String Rsin;
    @SerializedName("url")
    private String Url;

    public List<String> getBestaatuitBesluittype() {
        return BestaatuitBesluittype;
    }

    public void setBestaatuitBesluittype(List<String> bestaatuitBesluittype) {
        BestaatuitBesluittype = bestaatuitBesluittype;
    }

    public List<String> getBestaatuitInformatieobjecttype() {
        return BestaatuitInformatieobjecttype;
    }

    public void setBestaatuitInformatieobjecttype(List<String> bestaatuitInformatieobjecttype) {
        BestaatuitInformatieobjecttype = bestaatuitInformatieobjecttype;
    }

    public List<String> getBestaatuitZaaktype() {
        return BestaatuitZaaktype;
    }

    public void setBestaatuitZaaktype(List<String> bestaatuitZaaktype) {
        BestaatuitZaaktype = bestaatuitZaaktype;
    }

    public String getContactpersoonBeheerEmailadres() {
        return ContactpersoonBeheerEmailadres;
    }

    public void setContactpersoonBeheerEmailadres(String contactpersoonBeheerEmailadres) {
        ContactpersoonBeheerEmailadres = contactpersoonBeheerEmailadres;
    }

    public String getContactpersoonBeheerNaam() {
        return ContactpersoonBeheerNaam;
    }

    public void setContactpersoonBeheerNaam(String contactpersoonBeheerNaam) {
        ContactpersoonBeheerNaam = contactpersoonBeheerNaam;
    }

    public String getContactpersoonBeheerTelefoonnummer() {
        return ContactpersoonBeheerTelefoonnummer;
    }

    public void setContactpersoonBeheerTelefoonnummer(String contactpersoonBeheerTelefoonnummer) {
        ContactpersoonBeheerTelefoonnummer = contactpersoonBeheerTelefoonnummer;
    }

    public String getDomein() {
        return Domein;
    }

    public void setDomein(String domein) {
        Domein = domein;
    }

    public String getRsin() {
        return Rsin;
    }

    public void setRsin(String rsin) {
        Rsin = rsin;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

}
