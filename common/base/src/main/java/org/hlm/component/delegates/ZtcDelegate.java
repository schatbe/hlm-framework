package org.hlm.component.delegates;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.hlm.component.dao.Ztc;
import org.hlm.component.dto.zkn.Zaak;
import org.hlm.component.dto.ztc.json2pojo.Resultaattype;
import org.hlm.component.dto.ztc.json2pojo.Zaaktype;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ZtcDelegate implements JavaDelegate {

	private final Logger log = LoggerFactory.getLogger(ZtcDelegate.class);

	@Autowired
	private Ztc ztc;

	public void execute(DelegateExecution execution) {

		log.info("Start ZTC");

		Zaak zaak = (Zaak) execution.getVariable("zaak");

		// Get correct resultaattype for 'Zaak'
		Zaaktype zaaktype = ztc.getZaaktype(ztc.getCatalogus("HLM"), String.valueOf(zaak.getZaaktypeidentificatie()));
		Resultaattype resultaattype = ztc.getResultaattype(zaaktype, zaak.getResultaatomschrijving());

		String ingangsdatumObject = resultaattype.getIngangsdatumObject();
		ingangsdatumObject = ingangsdatumObject.startsWith("V") ? ingangsdatumObject.substring(1) : ingangsdatumObject;

		LocalDate dateEinddatum = zaak.getEinddatumLocalDate();
		LocalDate dateIngangsdatumObject = LocalDate.parse(ingangsdatumObject, DateTimeFormatter.BASIC_ISO_DATE);

		System.out.println("******************************** ZtcDelegate ********************************");
		System.out.println("Resultaattype: " + resultaattype.printResultaattype());
		System.out.println("Zaak einddatum: " + dateEinddatum);
		System.out.println("IngangsdatumObject resultaattype: " + dateIngangsdatumObject);
		System.out.println("*****************************************************************************");

		if (dateEinddatum != null && dateIngangsdatumObject != null && dateEinddatum.isAfter(dateIngangsdatumObject)) {
			System.out.println("******************************** ZtcDelegate ********************************");
			System.out.println("Einddatum is after IngangsdatumObject.");
			System.out.println("*****************************************************************************");
		} else {
			System.out.println("******************************** ZtcDelegate ********************************");
			System.out.println("Einddatum is NOT after IngangsdatumObject.");
			System.out.println("*****************************************************************************");

			// TEMPORARY: setBrondatumProcedure to niks; that way process goes to '(TEST) Ophalen relevante gegevens'
			resultaattype.setBrondatumProcedure("niks");
		}
		execution.setVariable("resultaattype", resultaattype);

		log.info("Einde ZTC");
	}
}
