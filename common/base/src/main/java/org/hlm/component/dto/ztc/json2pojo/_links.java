
package org.hlm.component.dto.ztc.json2pojo;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class _links {

    @SerializedName("self")
    private org.hlm.component.dto.ztc.json2pojo.Self Self;

    public org.hlm.component.dto.ztc.json2pojo.Self getSelf() {
        return Self;
    }

    public void setSelf(org.hlm.component.dto.ztc.json2pojo.Self self) {
        Self = self;
    }

}
