
package org.hlm.component.dto.ztc.json2pojo;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Zaaktype {

    @SerializedName("aanleiding")
    private String Aanleiding;
    @SerializedName("archiefclassificatiecode")
    private String Archiefclassificatiecode;
    @SerializedName("broncatalogus")
    private Object Broncatalogus;
    @SerializedName("bronzaaktype")
    private Object Bronzaaktype;
    @SerializedName("doel")
    private String Doel;
    @SerializedName("doorlooptijd")
    private Long Doorlooptijd;
    @SerializedName("einddatumObject")
    private Object EinddatumObject;
    @SerializedName("formulier")
    private List<Object> Formulier;
    @SerializedName("handelingBehandelaar")
    private String HandelingBehandelaar;
    @SerializedName("handelingInitiator")
    private String HandelingInitiator;
    @SerializedName("heeftEigenschap")
    private List<Object> HeeftEigenschap;
    @SerializedName("heeftGerelateerd")
    private List<Object> HeeftGerelateerd;
    @SerializedName("heeftRelevantBesluittype")
    private List<Object> HeeftRelevantBesluittype;
    @SerializedName("heeftRelevantInformatieobjecttype")
    private List<String> HeeftRelevantInformatieobjecttype;
    @SerializedName("heeftRelevantResultaattype")
    private List<String> HeeftRelevantResultaattype;
    @SerializedName("heeftRelevantZaakObjecttype")
    private List<String> HeeftRelevantZaakObjecttype;
    @SerializedName("heeftRoltype")
    private List<String> HeeftRoltype;
    @SerializedName("heeftStatustype")
    private List<String> HeeftStatustype;
    @SerializedName("identificatie")
    private Long Identificatie;
    @SerializedName("indicatieInternOfExtern")
    private String IndicatieInternOfExtern;
    @SerializedName("ingangsdatumObject")
    private String IngangsdatumObject;
    @SerializedName("isDeelzaaktypeVan")
    private List<Object> IsDeelzaaktypeVan;
    @SerializedName("maaktDeelUitVan")
    private String MaaktDeelUitVan;
    @SerializedName("omschrijving")
    private String Omschrijving;
    @SerializedName("omschrijvingGeneriek")
    private String OmschrijvingGeneriek;
    @SerializedName("onderwerp")
    private String Onderwerp;
    @SerializedName("opschortingAanhouding")
    private String OpschortingAanhouding;
    @SerializedName("product_dienst")
    private List<org.hlm.component.dto.ztc.json2pojo.ProductDienst> ProductDienst;
    @SerializedName("publicatieIndicatie")
    private String PublicatieIndicatie;
    @SerializedName("publicatietekst")
    private Object Publicatietekst;
    @SerializedName("referentieproces")
    private org.hlm.component.dto.ztc.json2pojo.Referentieproces Referentieproces;
    @SerializedName("servicenorm")
    private Object Servicenorm;
    @SerializedName("toelichting")
    private String Toelichting;
    @SerializedName("trefwoord")
    private List<Object> Trefwoord;
    @SerializedName("url")
    private String Url;
    @SerializedName("verantwoordelijke")
    private String Verantwoordelijke;
    @SerializedName("verantwoordingsrelatie")
    private List<Object> Verantwoordingsrelatie;
    @SerializedName("verlengingmogelijk")
    private String Verlengingmogelijk;
    @SerializedName("verlengingstermijn")
    private Long Verlengingstermijn;
    @SerializedName("versiedatum")
    private String Versiedatum;
    @SerializedName("vertrouwelijkheidAanduiding")
    private String VertrouwelijkheidAanduiding;
    @SerializedName("zaakcategorie")
    private String Zaakcategorie;

    public String getAanleiding() {
        return Aanleiding;
    }

    public void setAanleiding(String aanleiding) {
        Aanleiding = aanleiding;
    }

    public String getArchiefclassificatiecode() {
        return Archiefclassificatiecode;
    }

    public void setArchiefclassificatiecode(String archiefclassificatiecode) {
        Archiefclassificatiecode = archiefclassificatiecode;
    }

    public Object getBroncatalogus() {
        return Broncatalogus;
    }

    public void setBroncatalogus(Object broncatalogus) {
        Broncatalogus = broncatalogus;
    }

    public Object getBronzaaktype() {
        return Bronzaaktype;
    }

    public void setBronzaaktype(Object bronzaaktype) {
        Bronzaaktype = bronzaaktype;
    }

    public String getDoel() {
        return Doel;
    }

    public void setDoel(String doel) {
        Doel = doel;
    }

    public Long getDoorlooptijd() {
        return Doorlooptijd;
    }

    public void setDoorlooptijd(Long doorlooptijd) {
        Doorlooptijd = doorlooptijd;
    }

    public Object getEinddatumObject() {
        return EinddatumObject;
    }

    public void setEinddatumObject(Object einddatumObject) {
        EinddatumObject = einddatumObject;
    }

    public List<Object> getFormulier() {
        return Formulier;
    }

    public void setFormulier(List<Object> formulier) {
        Formulier = formulier;
    }

    public String getHandelingBehandelaar() {
        return HandelingBehandelaar;
    }

    public void setHandelingBehandelaar(String handelingBehandelaar) {
        HandelingBehandelaar = handelingBehandelaar;
    }

    public String getHandelingInitiator() {
        return HandelingInitiator;
    }

    public void setHandelingInitiator(String handelingInitiator) {
        HandelingInitiator = handelingInitiator;
    }

    public List<Object> getHeeftEigenschap() {
        return HeeftEigenschap;
    }

    public void setHeeftEigenschap(List<Object> heeftEigenschap) {
        HeeftEigenschap = heeftEigenschap;
    }

    public List<Object> getHeeftGerelateerd() {
        return HeeftGerelateerd;
    }

    public void setHeeftGerelateerd(List<Object> heeftGerelateerd) {
        HeeftGerelateerd = heeftGerelateerd;
    }

    public List<Object> getHeeftRelevantBesluittype() {
        return HeeftRelevantBesluittype;
    }

    public void setHeeftRelevantBesluittype(List<Object> heeftRelevantBesluittype) {
        HeeftRelevantBesluittype = heeftRelevantBesluittype;
    }

    public List<String> getHeeftRelevantInformatieobjecttype() {
        return HeeftRelevantInformatieobjecttype;
    }

    public void setHeeftRelevantInformatieobjecttype(List<String> heeftRelevantInformatieobjecttype) {
        HeeftRelevantInformatieobjecttype = heeftRelevantInformatieobjecttype;
    }

    public List<String> getHeeftRelevantResultaattype() {
        return HeeftRelevantResultaattype;
    }

    public void setHeeftRelevantResultaattype(List<String> heeftRelevantResultaattype) {
        HeeftRelevantResultaattype = heeftRelevantResultaattype;
    }

    public List<String> getHeeftRelevantZaakObjecttype() {
        return HeeftRelevantZaakObjecttype;
    }

    public void setHeeftRelevantZaakObjecttype(List<String> heeftRelevantZaakObjecttype) {
        HeeftRelevantZaakObjecttype = heeftRelevantZaakObjecttype;
    }

    public List<String> getHeeftRoltype() {
        return HeeftRoltype;
    }

    public void setHeeftRoltype(List<String> heeftRoltype) {
        HeeftRoltype = heeftRoltype;
    }

    public List<String> getHeeftStatustype() {
        return HeeftStatustype;
    }

    public void setHeeftStatustype(List<String> heeftStatustype) {
        HeeftStatustype = heeftStatustype;
    }

    public Long getIdentificatie() {
        return Identificatie;
    }

    public void setIdentificatie(Long identificatie) {
        Identificatie = identificatie;
    }

    public String getIndicatieInternOfExtern() {
        return IndicatieInternOfExtern;
    }

    public void setIndicatieInternOfExtern(String indicatieInternOfExtern) {
        IndicatieInternOfExtern = indicatieInternOfExtern;
    }

    public String getIngangsdatumObject() {
        return IngangsdatumObject;
    }

    public void setIngangsdatumObject(String ingangsdatumObject) {
        IngangsdatumObject = ingangsdatumObject;
    }

    public List<Object> getIsDeelzaaktypeVan() {
        return IsDeelzaaktypeVan;
    }

    public void setIsDeelzaaktypeVan(List<Object> isDeelzaaktypeVan) {
        IsDeelzaaktypeVan = isDeelzaaktypeVan;
    }

    public String getMaaktDeelUitVan() {
        return MaaktDeelUitVan;
    }

    public void setMaaktDeelUitVan(String maaktDeelUitVan) {
        MaaktDeelUitVan = maaktDeelUitVan;
    }

    public String getOmschrijving() {
        return Omschrijving;
    }

    public void setOmschrijving(String omschrijving) {
        Omschrijving = omschrijving;
    }

    public String getOmschrijvingGeneriek() {
        return OmschrijvingGeneriek;
    }

    public void setOmschrijvingGeneriek(String omschrijvingGeneriek) {
        OmschrijvingGeneriek = omschrijvingGeneriek;
    }

    public String getOnderwerp() {
        return Onderwerp;
    }

    public void setOnderwerp(String onderwerp) {
        Onderwerp = onderwerp;
    }

    public String getOpschortingAanhouding() {
        return OpschortingAanhouding;
    }

    public void setOpschortingAanhouding(String opschortingAanhouding) {
        OpschortingAanhouding = opschortingAanhouding;
    }

    public List<org.hlm.component.dto.ztc.json2pojo.ProductDienst> getProductDienst() {
        return ProductDienst;
    }

    public void setProductDienst(List<org.hlm.component.dto.ztc.json2pojo.ProductDienst> productDienst) {
        ProductDienst = productDienst;
    }

    public String getPublicatieIndicatie() {
        return PublicatieIndicatie;
    }

    public void setPublicatieIndicatie(String publicatieIndicatie) {
        PublicatieIndicatie = publicatieIndicatie;
    }

    public Object getPublicatietekst() {
        return Publicatietekst;
    }

    public void setPublicatietekst(Object publicatietekst) {
        Publicatietekst = publicatietekst;
    }

    public org.hlm.component.dto.ztc.json2pojo.Referentieproces getReferentieproces() {
        return Referentieproces;
    }

    public void setReferentieproces(org.hlm.component.dto.ztc.json2pojo.Referentieproces referentieproces) {
        Referentieproces = referentieproces;
    }

    public Object getServicenorm() {
        return Servicenorm;
    }

    public void setServicenorm(Object servicenorm) {
        Servicenorm = servicenorm;
    }

    public String getToelichting() {
        return Toelichting;
    }

    public void setToelichting(String toelichting) {
        Toelichting = toelichting;
    }

    public List<Object> getTrefwoord() {
        return Trefwoord;
    }

    public void setTrefwoord(List<Object> trefwoord) {
        Trefwoord = trefwoord;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getVerantwoordelijke() {
        return Verantwoordelijke;
    }

    public void setVerantwoordelijke(String verantwoordelijke) {
        Verantwoordelijke = verantwoordelijke;
    }

    public List<Object> getVerantwoordingsrelatie() {
        return Verantwoordingsrelatie;
    }

    public void setVerantwoordingsrelatie(List<Object> verantwoordingsrelatie) {
        Verantwoordingsrelatie = verantwoordingsrelatie;
    }

    public String getVerlengingmogelijk() {
        return Verlengingmogelijk;
    }

    public void setVerlengingmogelijk(String verlengingmogelijk) {
        Verlengingmogelijk = verlengingmogelijk;
    }

    public Long getVerlengingstermijn() {
        return Verlengingstermijn;
    }

    public void setVerlengingstermijn(Long verlengingstermijn) {
        Verlengingstermijn = verlengingstermijn;
    }

    public String getVersiedatum() {
        return Versiedatum;
    }

    public void setVersiedatum(String versiedatum) {
        Versiedatum = versiedatum;
    }

    public String getVertrouwelijkheidAanduiding() {
        return VertrouwelijkheidAanduiding;
    }

    public void setVertrouwelijkheidAanduiding(String vertrouwelijkheidAanduiding) {
        VertrouwelijkheidAanduiding = vertrouwelijkheidAanduiding;
    }

    public String getZaakcategorie() {
        return Zaakcategorie;
    }

    public void setZaakcategorie(String zaakcategorie) {
        Zaakcategorie = zaakcategorie;
    }

}
