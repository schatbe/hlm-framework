package org.hlm.component.dao;

import nl.egem.stuf.sector.zkn.*;
import org.hlm.common.utils.StUFUtils;
import org.hlm.common.utils.XMLUtils;
import org.hlm.component.dto.zkn.Besluit;
import org.hlm.component.dto.zkn.Zaak;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;

@Service
public class VrijeBerichtenImpl implements VrijeBerichten {

	private final WebServiceTemplate vrbWebServiceTemplate;

	@Autowired
	public VrijeBerichtenImpl(WebServiceTemplate vrbWebServiceTemplate) {
		this.vrbWebServiceTemplate = vrbWebServiceTemplate;
	}

	@Override
	public Zaak ophalenZaaknummer() {
		ObjectFactory factory = new ObjectFactory();

		GenereerZaakIdentificatieDi02 request = factory.createGenereerZaakIdentificatieDi02();
		request.setStuurgegevens(ophalenStuurgegevens(factory));

		GenereerZaakIdentificatieDu02 response = (GenereerZaakIdentificatieDu02) vrbWebServiceTemplate.marshalSendAndReceive(request);
		return composeOphalenGegevensResponse(response);
	}

	@Override
	public String ophalenDocumentnummer() {
		ObjectFactory factory = new ObjectFactory();

		GenereerDocumentIdentificatieDi02 request = factory.createGenereerDocumentIdentificatieDi02();
		request.setStuurgegevens(ophalenDocumentStuurgegevens(factory));

		GenereerDocumentIdentificatieDu02 response = (GenereerDocumentIdentificatieDu02) vrbWebServiceTemplate.marshalSendAndReceive(request);

		return composeOphalenDocumentResponse(response);
	}

	@Override
	public Besluit genereerBesluitIdentificatie() {
		ObjectFactory factory = new ObjectFactory();

		GenereerBesluitIdentificatieDi02 genereerBesluitIdentificatieDi02 = factory.createGenereerBesluitIdentificatieDi02();

		Di02StuurgegevensGbi di02StuurgegevensGbi = factory.createDi02StuurgegevensGbi();
		BerichtcodeDi02 berichtcodeDi02 = BerichtcodeDi02.DI_02;
		di02StuurgegevensGbi.setBerichtcode(berichtcodeDi02);

		Systeem systeem = factory.createSysteem();
		systeem.setOrganisatie("0392");
		systeem.setApplicatie("BPMNengine");
		di02StuurgegevensGbi.setZender(systeem);

		Systeem systeem1 = factory.createSysteem();
		systeem1.setOrganisatie("0392");
		systeem1.setApplicatie("ZSH");
		di02StuurgegevensGbi.setOntvanger(systeem1);

		FunctiegenereerBesluitidentificatie functiegenereerBesluitidentificatie = FunctiegenereerBesluitidentificatie.GENEREER_BESLUITIDENTIFICATIE;
		di02StuurgegevensGbi.setFunctie(functiegenereerBesluitidentificatie);

		genereerBesluitIdentificatieDi02.setStuurgegevens(di02StuurgegevensGbi);

		// make SOAP request and print SOAP response
//        JAXBElement JAXBEresponse = (JAXBElement) webServiceTemplate.marshalSendAndReceive(genereerBesluitIdentificatieDi02);
//        printXML(JAXBEresponse);

		GenereerBesluitIdentificatieDu02 response = (GenereerBesluitIdentificatieDu02) vrbWebServiceTemplate.marshalSendAndReceive(genereerBesluitIdentificatieDi02);
        XMLUtils.printXML(response);

		Besluit besluit = new Besluit();
		if (response != null && response.getBesluit() != null) {
			besluit.setBesluitIdentificatie(response.getBesluit().getIdentificatie().getValue().getValue());
			return besluit;
		} else {
			besluit.setBesluitIdentificatie("ERROR");
			return besluit;
		}
	}

	private Di02StuurgegevensGdi ophalenDocumentStuurgegevens(ObjectFactory documentFactory) {
		Di02StuurgegevensGdi stuurgegevensGdi = documentFactory.createDi02StuurgegevensGdi();
		BerichtcodeDi02 di02 = BerichtcodeDi02.DI_02;
		stuurgegevensGdi.setBerichtcode(di02);

		Systeem systeemZender = documentFactory.createSysteem();
		systeemZender.setOrganisatie("0392");
		systeemZender.setApplicatie("HBS");

		stuurgegevensGdi.setZender(systeemZender);

		Systeem systeemOntvanger = documentFactory.createSysteem();
		systeemOntvanger.setOrganisatie("0392");
		systeemOntvanger.setApplicatie("ZSH");

		stuurgegevensGdi.setOntvanger(systeemOntvanger);
		stuurgegevensGdi.setReferentienummer(java.util.UUID.randomUUID().toString());
		stuurgegevensGdi.setTijdstipBericht(StUFUtils.getStUFDateTime());

		FunctiegenereerDocumentidentificatie functie = FunctiegenereerDocumentidentificatie.GENEREER_DOCUMENTIDENTIFICATIE;
		stuurgegevensGdi.setFunctie(functie);

		return stuurgegevensGdi;
	}

	private String composeOphalenDocumentResponse(GenereerDocumentIdentificatieDu02 response) {
		String documentnummer = "";

		if (response != null && response.getDocument() != null) {
			EDCGdiE edc = response.getDocument();
			DocumentIdentificatieR identificatie = edc.getIdentificatie();
			documentnummer = identificatie.getValue();

			return documentnummer;
		}
		return documentnummer;
	}

	private Zaak composeOphalenGegevensResponse(GenereerZaakIdentificatieDu02 response) {
		Zaak zaak = new Zaak();

		if (response != null && response.getZaak() != null) {
			ZAKGziE zak = response.getZaak();
			ZaakIdentificatieR identificatie = zak.getIdentificatie();
			zaak.setZaakidentificatie(identificatie.getValue());

			return zaak;
		}
		return zaak;
	}

	private Di02StuurgegevensGzi ophalenStuurgegevens(ObjectFactory stufFactory) {
		// STUUR GEGEGEVENS
		Di02StuurgegevensGzi stuurgegevensGzi = stufFactory.createDi02StuurgegevensGzi();

		BerichtcodeDi02 di02 = BerichtcodeDi02.DI_02;
		stuurgegevensGzi.setBerichtcode(di02);

		Systeem systeemZender = stufFactory.createSysteem();
		systeemZender.setOrganisatie("0392");
		systeemZender.setApplicatie("HBS");

		stuurgegevensGzi.setZender(systeemZender);

		Systeem systeemOntvanger = stufFactory.createSysteem();
		systeemOntvanger.setOrganisatie("0392");
		systeemOntvanger.setApplicatie("ZSH");

		stuurgegevensGzi.setOntvanger(systeemOntvanger);
		stuurgegevensGzi.setReferentienummer(java.util.UUID.randomUUID().toString());
		stuurgegevensGzi.setTijdstipBericht(StUFUtils.getStUFDateTime());

		FunctiegenereerZaakidentificatie functie = FunctiegenereerZaakidentificatie.GENEREER_ZAAKIDENTIFICATIE;
		stuurgegevensGzi.setFunctie(functie);

		return stuurgegevensGzi;
	}
}
