package org.hlm.camunda;
/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.Map;

import org.camunda.bpm.engine.impl.el.ExpressionManager;
import org.camunda.bpm.engine.impl.javax.el.BeanELResolver;
import org.camunda.bpm.engine.impl.javax.el.CompositeELResolver;
import org.camunda.bpm.engine.impl.javax.el.ELResolver;
import org.camunda.bpm.engine.spring.ApplicationContextElResolver;
import org.springframework.context.ApplicationContext;

/**
 * {@link ExpressionManager} that exposes the full application-context or a
 * limited set of beans in expressions.
 *
 * @author Tom Baeyens
 * @author Eelco Schatborn
 */
public class SpringExpressionManagerFix extends ExpressionManager {

	protected ApplicationContext applicationContext;

	/**
	 * @param applicationContext
	 *            the applicationContext to use. Ignored when 'beans' parameter is
	 *            not null.
	 * @param beans
	 *            a map of custom beans to expose. If null, all beans in the
	 *            application-context will be exposed.
	 */
	public SpringExpressionManagerFix(ApplicationContext applicationContext, Map<Object, Object> beans) {
		super(beans);
		this.applicationContext = applicationContext;
	}

	@Override
	protected ELResolver createElResolver() {
		CompositeELResolver compositeElResolver = (CompositeELResolver) super.createElResolver();

		if (beans == null) {
			// Expose full application-context in expressions when no beans are specified
			compositeElResolver.add(new ApplicationContextElResolver(applicationContext));
		}

		compositeElResolver.add(new BeanELResolver());

		return compositeElResolver;
	}
}
