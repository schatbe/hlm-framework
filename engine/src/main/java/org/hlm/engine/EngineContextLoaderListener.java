package org.hlm.engine;

import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.logging.Logger;

import javax.servlet.ServletContextEvent;

import org.slf4j.event.Level;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

public class EngineContextLoaderListener extends ContextLoaderListener {

	private final Logger LOGGER = Logger.getLogger(EngineContextLoaderListener.class.getName());

	public EngineContextLoaderListener() {
		// TODO Auto-generated constructor stub
	}

	public EngineContextLoaderListener(WebApplicationContext context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Close the root web application context.
	 */
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		super.contextDestroyed(event);

		// This manually deregisters JDBC driver, which prevents Tomcat 7 from
		// complaining about memory leaks wrto this class
		Enumeration<Driver> drivers = DriverManager.getDrivers();
		while (drivers.hasMoreElements()) {
			Driver driver = drivers.nextElement();
			try {
				DriverManager.deregisterDriver(driver);
				LOGGER.info(String.format("deregistering jdbc driver: %s", driver));
			} catch (SQLException e) {
				LOGGER.severe(String.format("Error deregistering driver %s", driver));
			}

		}
	}
}
