package org.hlm.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.camunda.bpm.application.impl.event.ProcessApplicationEventListenerPlugin;
import org.camunda.bpm.engine.AuthorizationService;
import org.camunda.bpm.engine.FilterService;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.authorization.*;
import org.camunda.bpm.engine.filter.Filter;
import org.camunda.bpm.engine.identity.Group;
import org.camunda.bpm.engine.identity.User;
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.camunda.bpm.engine.impl.cfg.ProcessEnginePlugin;
import org.camunda.bpm.engine.impl.persistence.StrongUuidGenerator;
import org.camunda.bpm.engine.impl.persistence.entity.AuthorizationEntity;
import org.camunda.bpm.engine.impl.plugin.AdministratorAuthorizationPlugin;
import org.camunda.bpm.engine.spring.SpringProcessEngineConfiguration;
import org.camunda.bpm.engine.spring.container.ManagedProcessEngineFactoryBean;
import org.camunda.bpm.identity.impl.ldap.plugin.LdapIdentityProviderPlugin;
import org.camunda.connect.plugin.impl.ConnectProcessEnginePlugin;
import org.camunda.spin.plugin.impl.SpinProcessEnginePlugin;
import org.hlm.camunda.SpringExpressionManagerFix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.transaction.PlatformTransactionManager;

import static org.camunda.bpm.engine.authorization.Authorization.ANY;
import static org.camunda.bpm.engine.authorization.Authorization.AUTH_TYPE_GLOBAL;
import static org.camunda.bpm.engine.authorization.Authorization.AUTH_TYPE_GRANT;
import static org.camunda.bpm.engine.authorization.Permissions.ALL;

/* Configuration class for the Engine. 
 */
@Configuration
public class EngineConfiguration {

	@Autowired
	ProcessEngine processEngine;

	@Autowired
	ApplicationContext applicationContext;

	@Bean
	public SpringProcessEngineConfiguration processEngineConfiguration() {
		SpringProcessEngineConfiguration config = new SpringProcessEngineConfiguration();

		// config.setBpmnStacktraceVerbose(true);

		config.setProcessEngineName("HLMProcessEngine");

 		config.setHistory("full");
		config.setDatabaseSchemaUpdate("true");
		config.setAuthorizationEnabled(true);
		config.setJobExecutorDeploymentAware(true);
		config.setHistoryCleanupBatchWindowStartTime("00:01");

		// config.setJobExecutorActivate(true);
        config.setDefaultSerializationFormat("application/json");

		SpinProcessEnginePlugin spinProcessEnginePlugin = new SpinProcessEnginePlugin();
		ProcessApplicationEventListenerPlugin processApplicationEventListenerPlugin = new ProcessApplicationEventListenerPlugin();
		ConnectProcessEnginePlugin connectProcessEnginePlugin = new ConnectProcessEnginePlugin();

		List<ProcessEnginePlugin> pList = new ArrayList<>();
//		pList.add(ldapIdentityProviderPlugin());
//		pList.add(administratorAuthorizationPlugin());
		pList.add(spinProcessEnginePlugin);
		pList.add(processApplicationEventListenerPlugin);
		pList.add(connectProcessEnginePlugin);
		config.setProcessEnginePlugins(pList);

		config.setDataSource(dataSource());
		config.setTransactionManager(transactionManager());

		config.setIdGenerator(new StrongUuidGenerator());

		// The SpringExpressionManager contains a bug, so we need to inject a fixed
		// version until the Camunda source is fixed
		config.setExpressionManager(new SpringExpressionManagerFix(applicationContext, null));

		config.setClassLoader(EngineConfiguration.class.getClassLoader());

		return config;
	}

	@Bean
	public ManagedProcessEngineFactoryBean processEngineFactoryBean() throws Exception {
		ManagedProcessEngineFactoryBean processEngineFactoryBean = new ManagedProcessEngineFactoryBean();
		processEngineFactoryBean.setProcessEngineConfiguration(processEngineConfiguration());
		return processEngineFactoryBean;
	}

	private LdapIdentityProviderPlugin ldapIdentityProviderPlugin() {
		LdapIdentityProviderPlugin ldapIdentityProviderPlugin = new LdapIdentityProviderPlugin();
		ldapIdentityProviderPlugin.setUseSsl(false);
		ldapIdentityProviderPlugin.setAcceptUntrustedCertificates(false);
		ldapIdentityProviderPlugin.setServerUrl("ldap://ldaphlm.ssc.lan");
//		ldapIdentityProviderPlugin.setServerUrl("ldaps://hlm.ssc.lan:636/");
		ldapIdentityProviderPlugin.setManagerDn("CN=srvcLdap,OU=Non Personal,OU=Accounts,OU=org,DC=ssc,DC=lan");
		ldapIdentityProviderPlugin.setManagerPassword("Opzo3ken");

		ldapIdentityProviderPlugin.setBaseDn("OU=Accounts,OU=Org,DC=hlm,DC=ssc,DC=lan");

		ldapIdentityProviderPlugin.setUserSearchFilter("((objectCategory=user)(objectClass=user))");
		ldapIdentityProviderPlugin.setUserIdAttribute("sAMAccountName");
		ldapIdentityProviderPlugin.setUserFirstnameAttribute("givenName");
		ldapIdentityProviderPlugin.setUserLastnameAttribute("sn");
		ldapIdentityProviderPlugin.setUserEmailAttribute("mail");
		ldapIdentityProviderPlugin.setUserPasswordAttribute("userPassword");

		ldapIdentityProviderPlugin.setGroupSearchBase("OU=Groups");
		ldapIdentityProviderPlugin.setGroupSearchFilter("(objectCategory=group)");
		ldapIdentityProviderPlugin.setGroupIdAttribute("sAMAccountName");
		ldapIdentityProviderPlugin.setGroupNameAttribute("sAMAccountName");
		ldapIdentityProviderPlugin.setGroupMemberAttribute("member");

		ldapIdentityProviderPlugin.setAuthorizationCheckEnabled(true);

		return ldapIdentityProviderPlugin;
	}

	private AdministratorAuthorizationPlugin administratorAuthorizationPlugin() {
		AdministratorAuthorizationPlugin administratorAuthorizationPlugin = new AdministratorAuthorizationPlugin();
		administratorAuthorizationPlugin.setAdministratorUserName("jongfc");

		return administratorAuthorizationPlugin;
	}

	@Bean
	public DataSource dataSource() {
		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		// In-Memory H2 database
		dataSource.setDriverClass(org.h2.Driver.class);
		dataSource.setUrl("jdbc:h2:mem:camunda;DB_CLOSE_DELAY=-1");
		dataSource.setUsername("sa");
		dataSource.setPassword("");
		return dataSource;
	}

	@Bean
	public DataSource testDataSource() {
		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		dataSource.setDriverClass(org.postgresql.Driver.class);
		dataSource.setUrl("jdbc:postgresql://linuxdbt.ssc.lan:5432/camunda");
		dataSource.setUsername("camunda");
		dataSource.setPassword("hXVcmWKp9hvN");
		return dataSource;
	}

	@Bean
	public PlatformTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource());
	}
}