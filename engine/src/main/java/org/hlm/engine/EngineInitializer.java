package org.hlm.engine;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

public class EngineInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext container) {
		// Create the 'root' Spring application context
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();

		// check if an ApplicationContext already exists in the container, add it as a parent if it does.
		Object context = container.getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
		if (context instanceof ApplicationContext) {
			rootContext.setParent((ApplicationContext) context);
			container.removeAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
			System.out.println("parent found");
		}

		// set the servletContext in the ApplicationContext
		rootContext.setServletContext(container);

		// NODO: DO NOT SET the ApplicationContext in the ServletContext. That is done by the ContextLoaderListener below, which will not work if we do it
		//		container.setAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE, rootContext);

		// do a ComponentScan of the package
		rootContext.scan(this.getClass().getPackage().getName());

		// Manage the lifecycle of the root application context
		container.addListener(new EngineContextLoaderListener(rootContext));
	}
}
