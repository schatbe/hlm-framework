package org.hlm.framework.service.stuf.ws.kafka;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import javax.xml.stream.XMLStreamException;

import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.hlm.framework.HLM;
import org.hlm.framework.service.stuf.ws.kafka.common.HLMKafkaProducer;
import org.hlm.framework.service.stuf.ws.kafka.common.HLMRecordKey;
import org.hlm.framework.service.stuf.ws.request.JsonRpcSyntaxException;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

@Component
public class KafkaProducerBean {

	private static String TOPIC = HLM.Config.get("stuf-ws.kafka.topic").asString();

	private Producer<String, String> producer;

	@Handler
	public void process(Exchange exchange)
			throws IOException, XMLStreamException, SAXException, JsonRpcSyntaxException {
		long time = System.currentTimeMillis();
		produceRecord(exchange, time);
	}

	public void produceRecord(Exchange exchange, long time) {
		String is = exchange.getIn().getBody(String.class);

		try {
			long index = time;
//		URI uri = URI.create(exchange.getFromEndpoint().getEndpointUri());
//		String[] path = uri.getPath().split("/");
//		String serviceName = path[path.length - 1];
//		System.out.println(serviceName);

			producer = HLMKafkaProducer.createProducer();
			HLMRecordKey key = new HLMRecordKey(HLMRecordKey.Type.REQUEST, index, "zkn/BeantwoordVraag");
			final ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, key.toJSON(), is);
			RecordMetadata metadata = producer.send(record).get();
			long elapsedTime = System.currentTimeMillis() - time;
			System.out.printf("stuf-ws: sent record(key=%s value=%s) " + "meta(partition=%d, offset=%d) time=%d\n",
					record.key(), record.value(), metadata.partition(), metadata.offset(), elapsedTime);
			exchange.setProperty("hlmindex", index);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			producer.flush();
			producer.close();
		}
	}

//	@PreDestroy
//	public void close() {
//		if (producer != null)
//			producer.close();
//		System.out.println("DONE");
//	}

}
