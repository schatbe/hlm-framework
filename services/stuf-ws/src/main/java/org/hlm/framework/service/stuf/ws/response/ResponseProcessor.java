package org.hlm.framework.service.stuf.ws.response;

import java.io.IOException;
import java.net.URI;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.apache.camel.component.cxf.CxfPayload;
import org.apache.cxf.binding.soap.SoapHeader;
import org.hlm.framework.service.stuf.ws.common.StUFProcessor;
import org.hlm.framework.service.stuf.ws.common.StUFService;
import org.hlm.framework.service.stuf.ws.utils.DOMUtils;
import org.hlm.framework.service.stuf.ws.utils.NsMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * The main class that handles Camel exchanges to initiate and manage the
 * mapping process of responses.
 * 
 */
@Component
public class ResponseProcessor extends StUFProcessor {

	// @Override
	@Handler
	public void process(Exchange exchange) throws ParserConfigurationException, JsonParseException,
			JsonMappingException, XMLStreamException, IOException {
		
		// get the service bean representing the SOAP action for this exchange
		StUFService serviceBean = getStUFServiceBean(exchange);

		// Read the exchange into a SOAP payload.
		@SuppressWarnings("unchecked")
		CxfPayload<SoapHeader> inputPayload = exchange.getIn().getBody(CxfPayload.class);

		// Assumption: Since JSON-RPC can send only one method request at a time,
		// the soap body should contain only one response element.
		// Get the body element of payload.
		Element inputBodyElement = inputPayload.getBody().get(0);
		{
			// EPS: this fixes a bug where the namespaces in the envelope are not propagated
			// to the payload body giving rise to exceptions regarding namespace (mainly
			// "xmlns:xsi") not found and such in the payload.
			NsMap docNs = new NsMap(inputPayload.getNsMap());
			DOMUtils.addNsToElement(inputBodyElement, docNs);

			// use the predefined NsMap (from application.yaml) as a base
			NsMap nsMap = serviceBean.getEndpointConfig().getNsMap();
			// simplify the document by removing redundant namespaces and moving all ns's to the root
			DOMUtils.simplify(inputBodyElement, nsMap);
		}

		ResponseObjectCreator roc = new ResponseObjectCreator();

		byte[] response = null;

//		// Soap headers may exist. Proceed accordingly.
//		List<SoapHeader> inputHeadersList = inputPayload.getHeaders();
//		if (!inputHeadersList.isEmpty()) {
//
//			Element inputHeadersElement = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument()
//					.createElement("SOAP-HEADER");
//
//			// Headers might have multiple elements.
//			for (int i = 0; i < inputHeadersList.size(); i++) {
//				Node item = ((Node) inputHeadersList.get(i).getObject()).cloneNode(true);
//				inputHeadersElement.getOwnerDocument().adoptNode(item);
//				inputHeadersElement.appendChild(item);
//			}
//
//			// Create a JSON-RPC response WITH HEADER.
//			response = roc.createNormalResponse(inputHeadersElement, inputBodyElement,
//					exchange.getProperty("jsonrpc-id"));
//
//			// CLEAR VARIABLE.
//			inputHeadersElement = null;
//
//		} else {
			// Create a JSON-RPC response WITHOUT HEADER.
			response = roc.createNormalResponse(inputBodyElement, exchange.getProperty("jsonrpc-id"));

//		}

		// Set the output body to response.
		exchange.getOut().setBody(response);

		// Set the content type (although it is not essential).
		exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");

		// CLEAR VARIABLES.
		inputPayload = null;
		inputBodyElement = null;
//		inputHeadersList = null;
	}

}