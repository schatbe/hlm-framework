package org.hlm.framework.service.stuf.ws.kafka;

import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.apache.camel.Message;
import org.apache.camel.component.kafka.KafkaConstants;
import org.hlm.framework.service.stuf.ws.kafka.common.HLMRecordKey;
import org.springframework.stereotype.Component;

@Component
public class PrepareRequestBean {

	@Handler
	public void process(Exchange exchange) {
		Message message = exchange.getIn();
//		String value = message.getBody(String.class);
		long time = System.currentTimeMillis();
		HLMRecordKey key = new HLMRecordKey(HLMRecordKey.Type.REQUEST, time, "zkn/BeantwoordVraag");
		message.setHeader(KafkaConstants.KEY, key);
	}
}
