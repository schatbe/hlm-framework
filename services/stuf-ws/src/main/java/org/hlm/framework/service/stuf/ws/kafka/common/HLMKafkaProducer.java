package org.hlm.framework.service.stuf.ws.kafka.common;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;

public class HLMKafkaProducer {

	private final static String TOPIC = "hlm-queue";
	private final static String BOOTSTRAP_SERVERS = "127.0.0.1:9092";

	public static Producer<String, String> createProducer() {

		Properties props = new Properties();

		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
		props.put(ProducerConfig.CLIENT_ID_CONFIG, "HLMKafkaProducer");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

		return new KafkaProducer<>(props);
	}

	/*
	 * static void runProducer(final int sendMessageCount) throws Exception { final
	 * Producer<Long, String> producer = createProducer(); long time =
	 * System.currentTimeMillis(); try { for (long index = time; index < time +
	 * sendMessageCount; index++) { final ProducerRecord<Long, String> record = new
	 * ProducerRecord<>(TOPIC, index, "Hello Mom " + index); RecordMetadata metadata
	 * = producer.send(record).get(); long elapsedTime = System.currentTimeMillis()
	 * - time; System.out.printf("sent record(key=%s value=%s) " +
	 * "meta(partition=%d, offset=%d) time=%d\n", record.key(), record.value(),
	 * metadata.partition(), metadata.offset(), elapsedTime); } } finally {
	 * producer.flush(); producer.close(); } }
	 */
}
