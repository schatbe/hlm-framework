package org.hlm.framework.service.stuf.ws;

import javax.annotation.PostConstruct;

import org.hlm.framework.HLM;
import org.hlm.framework.service.stuf.ws.common.StUFServiceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component("serviceApplication")
public class ServiceApplication {

	@Autowired
	ApplicationContext applicationContext;

	@Autowired
	StUFServiceBuilder serviceBuilder;

	public ServiceApplication() {
	}

	@PostConstruct
	public void init() {
		System.out.println("initStUFServices");
		try {
			HLM.Config.get("stuf-ws.services").forEach((child) -> {
				serviceBuilder.build(child);
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}