package org.hlm.framework.service.stuf.ws.wsdl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.wsdl.Operation;
import javax.wsdl.Part;
import javax.xml.namespace.QName;

import org.apache.camel.util.CastUtils;
import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaAll;
import org.apache.ws.commons.schema.XmlSchemaChoice;
import org.apache.ws.commons.schema.XmlSchemaComplexType;
import org.apache.ws.commons.schema.XmlSchemaElement;
import org.apache.ws.commons.schema.XmlSchemaEnumerationFacet;
import org.apache.ws.commons.schema.XmlSchemaFacet;
import org.apache.ws.commons.schema.XmlSchemaParticle;
import org.apache.ws.commons.schema.XmlSchemaSequence;
import org.apache.ws.commons.schema.XmlSchemaSimpleType;
import org.apache.ws.commons.schema.XmlSchemaSimpleTypeRestriction;
import org.apache.ws.commons.schema.XmlSchemaType;
import org.hlm.framework.service.stuf.ws.utils.ytree.YNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class calls methods during initialization of the adapter to obtain the
 * WSDL interface from the URL, parse it and extract required information to
 * specify endpoint details. Additionally, it creates a HTML page to help
 * clients create JSON-RPC request messages.
 * 
 * @author Sevket Gökay <sevket.goekay@rwth-aachen.de>
 * @author Lars C. Gleim <lars.gleim@rwth-aachen.de>
 *
 */

public class WSDLConverter {

	final static Logger LOG = LoggerFactory.getLogger(WSDLConverter.class);
	private OpenAPIBuilder contract;

	public OpenAPIBuilder getContract() {
		return contract;
	}

	private Map<String, YNode> complexTypes = new HashMap<String, YNode>();

	public WSDLConverter(WSDLParser wsdlParser) {
		contract = convert(wsdlParser);
	}

//	/**
//	 * Creates a HTML page for clients using XSLT that displays information how to
//	 * access/use the Web service.
//	 */
//	private void convertWSDLtoHTML(Document wsdlDoc) {
//		LOG.info("Creating the HTML page from WSDL...");
//		try {
//			// Read the XSLT file
//			InputStream xsltStream = Thread.currentThread().getContextClassLoader()
//					.getResourceAsStream("wsdl-viewer.xsl");
//			if (xsltStream == null) {
//				LOG.info("XSLT file could not be read. Skipping the creation of the HTML page.");
//				return;
//			}
//			Source xsltSource = new StreamSource(xsltStream);
//			xsltSource.setSystemId(wsdlParser.wsdlUrl);
//
//			// Create a transformer from the XSLT
//			Transformer transformer = TransformerFactory.newInstance().newTransformer(xsltSource);
//
//			// Do the transformation
//			DOMSource domSource = new DOMSource(wsdlDoc);
//			transformer.transform(domSource, new StreamResult(new File(wsdlParser.dirPath, "service-details.html")));
//			xsltStream.close();
//
//		} catch (TransformerFactoryConfigurationError e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		} catch (TransformerConfigurationException e) {
//			e.printStackTrace();
//		} catch (TransformerException e) {
//			e.printStackTrace();
//		}
//	}

	/**
	 * Extracts the operations offered by a web service from the WSDL definition and
	 * calls the routines for code stub creation and respective usage documentation.
	 */
	private OpenAPIBuilder convert(WSDLParser wsdlParser) {

		// code = new CodeWriter(this.wsdlUrl);
		contract = new OpenAPIBuilder(wsdlParser);
//		LOG.info("**** Listing methods ****");

		// Iterate over all operations defined for a given soap port
		for (Object op : wsdlParser.soapPort.getBinding().getPortType().getOperations()) {
			Operation operation = (Operation) op;
			String opName = operation.getName();
			System.out.println(" - " + opName);

			// Get parts from WSDL that describe the operation's parameters
			Collection<Part> inParts = CastUtils.cast(operation.getInput().getMessage().getParts().values());

			// Get parts from WSDL that describe the operation's return value
			Collection<Part> outParts = CastUtils.cast(operation.getOutput().getMessage().getParts().values());

			YNode params = new YNode();
			YNode output = new YNode();
			for (Part inPart : inParts) {
				getParameterDetails(params, wsdlParser.getSchema(), inPart.getName(), inPart.getElementName(), inPart.getTypeName());
			}
			for (Part outPart : outParts) {
				getParameterDetails(output, wsdlParser.getSchema(), outPart.getName(), outPart.getElementName(), outPart.getTypeName());
			}
//						code.addOperation(operation, params, output);
			contract.addPathSpec(operation, params, output);

		}
		// code.write(dirPath);
		System.out.println(contract);
		return contract;
	}

	/**
	 * Fetches details (names and types) for a given operation parameter or return
	 * value
	 * 
	 * @param name        A String representing the name of the part within the WSDL
	 * @param elementName The qualified name of the actual part to identify it
	 *                    uniquely if possible
	 * @param typeName    The qualified name of the parts type to use it directly if
	 *                    elementName is not set
	 * @return A String encoding the parts details
	 */
	private void getParameterDetails(YNode y, XmlSchema schema, String name, QName elementName, QName typeName) {
//		XmlSchema schema = wsdlParser.getSchema();

		XmlSchemaType param = (elementName != null) ? schema.getElementByName(elementName).getSchemaType()
				: schema.getTypeByName(typeName);

		if (param instanceof XmlSchemaComplexType)
			processComplexType(y, (XmlSchemaComplexType) param, name);
		else if (param instanceof XmlSchemaSimpleType)
			processSimpleType(y, (XmlSchemaSimpleType) param, name, isOptional(schema.getElementByName(elementName)));
		else
			y.element(name, typeName.getLocalPart());
	}

	/**
	 * Processes an XML Schema Complex Type entity.
	 * 
	 * Due to limitations of the WSDL4J project there are various schema extensions
	 * that cannot be correctly processed. Therefore a check for such elements is
	 * inevitable. In case unhandled attributes are detected, a remark is added to
	 * the source code and the function returns. If no issues are detected the
	 * nested type elements of the complex type are determined and processed
	 * analogously to the processing in the getParameterDetails(String name, QName
	 * elementName, QName typeName) function.
	 * 
	 * @param elemType The complex entity to parse
	 * @param name     The name of this entity to use for generation
	 * @return A string detailing the type for usage in Objective-C code
	 */
	private void processComplexType(YNode y, XmlSchemaComplexType elemType, String name) {
		if (!complexTypes.containsKey(name)) {
			YNode ct = new YNode(name);
			ct.element("type", "object");
			YNode p = ct.node("properties");

			XmlSchemaParticle particle = elemType.getParticle();

			if (particle == null) { // WSDL4J fails parsing some advanced definitions
				if (elemType.getUnhandledAttributes() != null) {
					ct.element(name, "NIL: unprocessed complex type - Please refer to documentation");
				}
			} else {
				List<?> elementList = null;
				if (particle instanceof XmlSchemaSequence)
					elementList = ((XmlSchemaSequence) particle).getItems();
				else if (particle instanceof XmlSchemaAll)
					elementList = ((XmlSchemaAll) particle).getItems();
				else if (particle instanceof XmlSchemaChoice)
					elementList = ((XmlSchemaChoice) particle).getItems();

				for (Object member : elementList) {
					if (member instanceof XmlSchemaElement) {
						XmlSchemaElement element = ((XmlSchemaElement) member);
						XmlSchemaType elementType = element.getSchemaType();

						if (elementType instanceof XmlSchemaSimpleType) {
							processSimpleType(p, (XmlSchemaSimpleType) elementType, element.getName(),
									isOptional(element));
						} else if (elementType instanceof XmlSchemaComplexType) {
//					System.out.println(element.getName());
//							String ns = element.getQName().toString();
//							ret += "    " + element.getQName() + ":" + element.getName() + " : "
//									+ (isOptional(element) ? "    // {Optional}\n" : "\n");
							processComplexType(p, (XmlSchemaComplexType) elementType, element.getName());
						}
					}
				}
			}
			complexTypes.put(name, ct);
			contract.addDefinition(ct);
		}
		y.element("$ref", "'#/definitions/" + name + "'");
	}

	/**
	 * Determines the type name of an XML Schema Simple Type entity
	 * 
	 * @param type       The entity to check
	 * @param name       The name of that entity
	 * @param isOptional Whether this is defined as optional in the WSDL or not
	 * @return The type of the parameter type as a string
	 */
	private static void processSimpleType(YNode y, XmlSchemaSimpleType type, String name, boolean isOptional) {
		String tmp = "";
		if (isEnumeration(type)) {
			tmp += (enumeratorValues(type));
		} else {
			tmp = (type.getName());
		}
//		return CodeWriter.keyVal(name, tmp, (isOptional) ? "{Optional}" : "");
		YNode t = y.node(name);
		t.element("type", tmp);
		t.element("required", (isOptional ? "false" : "true"));
	}

	/**
	 * Return true if a simple type is a straightforward XML Schema representation
	 * of an enumeration. If we discover schemas that are 'enum-like' with more
	 * complex structures, we might make this deal with them.
	 * 
	 * @param type Simple type, possible an enumeration.
	 * @return true for an enumeration.
	 */
	private static boolean isEnumeration(XmlSchemaSimpleType type) {
		try {
			XmlSchemaSimpleTypeRestriction restriction = (XmlSchemaSimpleTypeRestriction) type.getContent();
			List<XmlSchemaFacet> facets = restriction.getFacets();
			for (XmlSchemaFacet facet : facets) {
				if (facet instanceof XmlSchemaEnumerationFacet) {
					return true;
				}
			}
		} catch (Exception e) {
		}
		return false;
	}

	/**
	 * Retrieve the string values for an enumeration.
	 * 
	 * @param type
	 * @return
	 */
	private static List<String> enumeratorValues(XmlSchemaSimpleType type) {
		XmlSchemaSimpleTypeRestriction restriction = (XmlSchemaSimpleTypeRestriction) type.getContent();
		List<XmlSchemaFacet> facets = restriction.getFacets();
		List<String> values = new ArrayList<String>();
		for (XmlSchemaFacet facet : facets) {
			XmlSchemaEnumerationFacet enumFacet = (XmlSchemaEnumerationFacet) facet;
			values.add(enumFacet.getValue().toString());
		}
		return values;
	}

	/**
	 * Determines if an element is optional
	 * 
	 * @param element The element to check
	 * @return True if it is optional, false if not
	 */
	private static boolean isOptional(XmlSchemaElement element) {
		return !(element.getMinOccurs() != 0);
	}
}
