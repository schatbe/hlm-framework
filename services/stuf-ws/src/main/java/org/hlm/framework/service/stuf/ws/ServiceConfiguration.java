package org.hlm.framework.service.stuf.ws;

import org.apache.camel.CamelContext;
import org.apache.camel.component.cxf.CxfEndpointConfigurer;
import org.apache.camel.spring.CamelContextFactoryBean;
import org.hlm.framework.HLM;
import org.hlm.framework.service.stuf.ws.common.StUFEndpointConfigurerConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfiguration {
	/*
	 * basic methods/beans for configuration of the application
	 * 
	 */

	@Autowired
	ApplicationContext applicationContext;

	@Bean("camelContext")
	public CamelContext getCamelContext() {
		CamelContextFactoryBean camelContextFactoryBean = new CamelContextFactoryBean();
		camelContextFactoryBean.setId("HLMServiceCamelContext");
		camelContextFactoryBean.setApplicationContext(applicationContext);
		
		// configure the Security keystore
		CamelContext context = camelContextFactoryBean.getContext();
		StUFEndpointConfigurerConverter converter = new StUFEndpointConfigurerConverter(HLM.Config.get("stuf-ws.security.keystore"));
		context.getTypeConverterRegistry().addTypeConverter(CxfEndpointConfigurer.class, String.class, converter);
		
		return context;
	}
}