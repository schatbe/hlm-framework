package org.hlm.framework.service.stuf.ws.common;

import java.util.HashMap;

import org.hlm.framework.service.stuf.ws.utils.NsMap;
import org.hlm.framework.service.stuf.ws.wsdl.WSDLParser;

import io.datatree.Tree;

public class StUFRouteConfig {

	private String remoteEndpoint, localEndpoint;
	private String beanName;
	private String wsNamespace, serviceName, serviceEndPoint;
	private NsMap nsMap;
	private String dataFormat = "PAYLOAD";
	private boolean loggingFeatureEnabled = true;

	// Operation name and request message payload name might be different.
	// We store a map of operation names and their request message payload names
	// for lookup (for document/literal wrapped style).
	public HashMap<String, String> OPERATIONS_MAP;

	public StUFRouteConfig(String wsdlFile, Tree prefs) {
//	public StUFRouteConfig(String wsdlFile, String remoteEndpoint,  String localPath, NsMap nsMap) {
//		this.nsMap = new NsMap(nsMap);
		this.nsMap = new NsMap(prefs.get("namespaces"));
		String remoteEndpoint = prefs.get("remote.endpoint","");
		String localPath = prefs.get("local.path", "");

		WSDLParser wp = new WSDLParser(wsdlFile);

		this.wsNamespace = wp.wsNamespace;
		this.serviceName = wp.serviceName;
		this.beanName = localPath + this.serviceName;

		this.remoteEndpoint = remoteEndpoint + "/" + this.serviceName;
		this.localEndpoint = localPath + "/" + this.serviceName;
		this.serviceEndPoint = "cxf://" + this.remoteEndpoint + "?wsdlURL=" + wp.wsdlUrl + "&serviceName={"
				+ this.wsNamespace + "}" + this.serviceName + "&portName={" + this.wsNamespace + "}" + wp.soapPortName
				+ "&dataFormat=" + this.dataFormat + "&loggingFeatureEnabled=" + this.loggingFeatureEnabled
				+ "&cxfEndpointConfigurer=StUFEndpointConfigurer";

		this.OPERATIONS_MAP = wp.OPERATIONS_MAP;
//		OPERATIONS_MAP.forEach((String key, String value) -> {
//			System.out.println("(" + key + ", " + value + ")");
//		});
	}

	public String getBeanName() {
		return beanName;
	}

	public NsMap getNsMap() {
		return nsMap;
	}

	public String getLocalEndpoint() {
		return this.localEndpoint;
	}

	public String getServiceEndpoint() {
		return serviceEndPoint;
	}

	public String getWsNamespace() {
		return wsNamespace;
	}

	public String getServiceName() {
		return serviceName;
	}
}
