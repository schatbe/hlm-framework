package org.hlm.framework.service.stuf.ws.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.LinkedList;

public class RefStack extends LinkedList<RefMap> {

	private static final long serialVersionUID = 1L;

	{
		// initial scope
		newScope();
	}

	public RefStack() {
	}

	public String get(String name) {
		Iterator<RefMap> iter = listIterator(0);
		while (iter.hasNext()) {
			RefMap current = iter.next();
			String value = current.getValue(name);
			if (value != null) {
				return value;
			}
		}
		return null;
	}

	public String searchScope(String name) {
		return peek().getValue(name);
	}

	synchronized public RefMap newScope() {
		RefMap refMap = new RefMap();
		push(refMap);
		return refMap;
	}

	public RefMap dropScope() {
		return pop();
	}

	synchronized public void push(String name, String ref) {
		String existing = searchScope(name);
		if (existing != null) {
			// name exists in current scope, so check if they are fully equal
			if (existing.equals(ref)) {
				// they refer to the same so we already have it... do nothing
			} else {
				// same name but different ref in the same scope
				System.out.println("ERROR: Ref to be added (" + name + "->" + ref + ") is already registered as ("
						+ existing + ")");
			}
		} else {
			// name does not exist in current scope, add it
			peek().add(name, ref);
//			System.out.println("added Ref: " + name + "->" + ref);
		}
	}

	public String resolve(String name) {
		String ref = get(name);
//		System.out.println("resolve: " + name + "->" + ref);
		return (ref != null) ? ref : name;
	}

	public String toString() {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		Iterator<RefMap> iter = listIterator(0);
		while (iter.hasNext()) {
			iter.next().forEach((String name, String ref) -> pw.println(name + "->" + ref));
		}
		return sw.toString();
	}

	public void print() {
		System.out.println(toString());
	}
}
