package org.hlm.framework.service.stuf.ws.common;

import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;

import org.apache.camel.Exchange;
import org.apache.camel.TypeConversionException;
import org.apache.camel.component.cxf.CxfEndpointConfigurer;
import org.apache.camel.support.TypeConverterSupport;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.frontend.AbstractWSDLBasedEndpointFactory;
import org.apache.cxf.transport.http.URLConnectionHTTPConduit;

import io.datatree.Tree;

public class StUFEndpointConfigurerConverter extends TypeConverterSupport {

	private Tree prefs;

	public StUFEndpointConfigurerConverter(Tree config) {
		this.prefs = config;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T convertTo(Class<T> type, Exchange exchange, Object value) throws TypeConversionException {
		CxfEndpointConfigurer configurer = new CxfEndpointConfigurer() {
			@Override
			public void configure(AbstractWSDLBasedEndpointFactory factoryBean) {
				// do nothing
			}

			@Override
			public void configureClient(Client client) {
				// security.keystore
				TrustManager[] trustManagers = getTrustManagers();
				if (trustManagers != null && trustManagers.length > 0) {
					URLConnectionHTTPConduit conduit = (URLConnectionHTTPConduit) client.getConduit();
					TLSClientParameters tlsParams = new TLSClientParameters();
					tlsParams.setDisableCNCheck(true);
					tlsParams.setTrustManagers(trustManagers);
					conduit.setTlsClientParameters(tlsParams);
				}
//				// config.security
//				// BasicAuth
//				String username = prefs.get("basicauth/username", null);
//				String pwd = prefs.get("basicauth/pwd", null);
//				if (username != null && pwd != null) {
//					CxfEndpoint endPoint = (CxfEndpoint) client.getEndpoint();
//					Map<String, Object> properties = new HashMap<String, Object>();
//					AuthorizationPolicy authPolicy = new AuthorizationPolicy();
//					authPolicy.setAuthorizationType(HttpAuthHeader.AUTH_TYPE_BASIC);
//					authPolicy.setUserName(username);
//					authPolicy.setPassword(pwd);
//					authPolicy.setAuthorization("true");
//
//					properties.put(AuthorizationPolicy.class.getName(), authPolicy);
////					properties.put("org.apache.cxf.configuration.security.AuthorizationPolicy", authPolicy);
//
//					endPoint.setProperties(properties);
//				}
//				
			}

			@Override
			public void configureServer(Server server) {
				// do nothing
			}
		};
		return (T) configurer;
	}

	private TrustManager[] getTrustManagers() {
		String keyStoreFile = prefs.get("keystore.resource", "");
		if (!keyStoreFile.isEmpty()) {
			ClassLoader classLoader = getClass().getClassLoader();
			InputStream in = classLoader.getResourceAsStream(keyStoreFile);

//		CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
//		Collection<? extends Certificate> certificates = certificateFactory.generateCertificates(in);
//		if (certificates.isEmpty()) {
//			throw new IllegalArgumentException("expected non-empty set of trusted certificates");
//		}

			// add the certificate to the key store.
			char[] password = prefs.get("keystore.password", "").toCharArray();
			try {
				KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
				keyStore.load(in, password);
				// Use it to build an X509 trust manager.
//			KeyManagerFactory keyManagerFactory;
//			keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
//			keyManagerFactory.init(keyStore, password);
				TrustManagerFactory trustManagerFactory = TrustManagerFactory
						.getInstance(TrustManagerFactory.getDefaultAlgorithm());
				trustManagerFactory.init(keyStore);
				return trustManagerFactory.getTrustManagers();
			} catch (KeyStoreException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
//		} catch (UnrecoverableKeyException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
			}
		}
		return null;
	}
}
