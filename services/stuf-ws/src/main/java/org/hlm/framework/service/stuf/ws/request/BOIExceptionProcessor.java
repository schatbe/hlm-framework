package org.hlm.framework.service.stuf.ws.request;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.hlm.framework.service.stuf.ws.response.ResponseObjectCreator;

import com.fasterxml.jackson.core.JsonProcessingException;

/**
 * The processor to create a JSON-RPC error message 
 * when method does not exist at WS.
 */
public class BOIExceptionProcessor implements Processor {
	@Override
	public void process(Exchange exchange) throws JsonProcessingException {
		ResponseObjectCreator roc = new ResponseObjectCreator();
		byte[] error = roc.createErrorResponse(
				-32601, 
				"Method not found", //: " + exchange.getOut().getBody(String.class).toString(), 
				null, 
				exchange.getProperty("jsonrpc-id"));	
		exchange.getOut().setBody(error);
	}
}
