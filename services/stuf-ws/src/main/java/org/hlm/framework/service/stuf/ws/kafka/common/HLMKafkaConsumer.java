package org.hlm.framework.service.stuf.ws.kafka.common;

import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

public class HLMKafkaConsumer {

	private final static String BOOTSTRAP_SERVERS = "127.0.0.1:9092";

	public static Consumer<String, String> createConsumer() {

		Properties props = new Properties();

		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, BOOTSTRAP_SERVERS);
		props.put(ConsumerConfig.CLIENT_ID_CONFIG, "HLMKafkaConsumer");
		props.put(ConsumerConfig.GROUP_ID_CONFIG, "HLMKafka");
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

		// When a new consumer reads from a partition and there's no previous committed
		// offset, the auto.offset.reset property is used to decide what the starting
		// offset should be. If you set it to "latest" (the default) you start reading
		// at the latest (last) message. If you set it to "earliest" you get the first
		// available message. Or "none".
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

		// Create the consumer using props.
		final Consumer<String, String> consumer = new KafkaConsumer<>(props);
		return consumer;
	}
}
