package org.hlm.framework.service.stuf.ws.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import org.apache.ws.commons.schema.constants.Constants;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import io.datatree.Tree;

public class NsMap extends HashMap<String, String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int index = 1;

	public NsMap() {
		super();
	}

	public NsMap(Map<String, String> nsMap) {
		super(nsMap);
	}

	public NsMap(NsMap nsMap) {
		super(nsMap);
	}

	public NsMap(Tree tree) {
		super();
		tree.forEach((child) -> {
			put(child.getName(), child.asString());
		});
	}
	
	public NsMap(Node node) {
		this(node.getAttributes());
	}

	public NsMap(NamedNodeMap attributesList) {
		super();
		if (attributesList != null) {
			for (int j = 0; j < attributesList.getLength(); j++) {
				Node a = attributesList.item(j);
				String prefix = a.getPrefix();
				String name = a.getLocalName();
				String uri = a.getNodeValue();
				if (Constants.XMLNS_ATTRIBUTE.equals(prefix)) {
					// we have an NS decl
					// System.out.println("getNsMap: " + prefix + " " + name + " " + uri);
					put(name, uri);
				}
			}
		}
	}

	public String getValue(String key) {
		return get(key);
	}

	public String getKey(String value) {
		for (NsMap.Entry<String, String> entry : entrySet()) {
			if (entry.getValue().equals(value)) {
				return entry.getKey();
			}
		}
		return null;
	}

	/**
	 * Adds a namespace to the Map if it does not yet exist. if it does exist either
	 * by URI or name it will add the namespace under a new name and return that
	 * name.
	 * 
	 * @param key   the preferred name for the namespace
	 * @param value the URI of the namespace
	 * @return the key that was used to add the namespace, if the URI already exists
	 *         the returned key will be the one for the namespace already present in
	 *         the NsMap. If the name already exists an new name will be generated
	 *         and returned.
	 */
	public String add(String key, String value) {
		String vKey = getKey(value);
		if (vKey == null) {
			// we don't have the value yet, check the key
			String kValue = getValue(key);
			if (kValue != null) {
				// key name is taken, use a substitute
				vKey = "ns" + index++;
			} else {
				// key name is free
				vKey = key;
			}
//			System.out.println("added Ns: " + vKey + ":" + value);
			super.put(vKey, value);
		}
		return vKey;
	}

}