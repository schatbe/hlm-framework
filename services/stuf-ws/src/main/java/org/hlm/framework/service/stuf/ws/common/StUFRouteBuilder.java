package org.hlm.framework.service.stuf.ws.common;

import org.apache.camel.builder.RouteBuilder;
import org.apache.cxf.binding.soap.SoapFault;
import org.hlm.framework.service.stuf.ws.request.BOIExceptionProcessor;
import org.hlm.framework.service.stuf.ws.request.JsonRpcSyntaxException;
import org.hlm.framework.service.stuf.ws.response.SoapFaultProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class describes the mapping route from client to WS and back to client.
 */
public class StUFRouteBuilder extends RouteBuilder {

	private final Logger log = LoggerFactory.getLogger(StUFRouteBuilder.class);

	private StUFRouteConfig config;

	public StUFRouteBuilder(StUFRouteConfig config) {
		this.config = config;
	}

	@Override
	public void configure() {

		restConfiguration().component("restlet");

		{
			// If handled is true, then the thrown exception will be handled
			// and Camel will not continue routing in the original route, but break out.

			// Catch JSON-RPC syntax errors
			onException(JsonRpcSyntaxException.class).handled(true);

			// Catch a Soap Fault
			onException(SoapFault.class).handled(true).process(new SoapFaultProcessor());

			// Catch the error when method does not exist at WS
			onException(IllegalArgumentException.class).handled(true)
					.onWhen(exceptionMessage().contains("BindingOperationInfo")).process(new BOIExceptionProcessor());
		}

		rest().post(config.getLocalEndpoint()).route().bean("requestProcessor").to(config.getServiceEndpoint())
				.bean("responseProcessor");

		rest("/kafka").post(config.getLocalEndpoint()).route().bean("kafkaProducerBean").bean("kafkaConsumerBean");

		// this works with SOAP body only, i.e. no SOAP tags
		// TODO: soap gba does not work yet
//		rest("/soap").post(this.config.getServletUrl()).to(this.config.getServiceEndpoint());

		// WORKS too
//		from("rest:post:" + this.config.getServletUrl()).to("bean:requestProcessor").to(this.config.getServiceEndpoint())
//				.to("bean:responseProcessor");

		// WORKS too
//		rest().post(this.config.getServletUrl()).to("direct:handle" + this.config.getServiceName());
//		from("direct:handle" + this.config.getServiceName()).to("bean:requestProcessor")
//				.to(this.config.getServiceEndpoint()).to("bean:responseProcessor");

	}
}