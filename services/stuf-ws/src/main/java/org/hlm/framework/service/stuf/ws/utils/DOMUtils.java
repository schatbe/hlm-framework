package org.hlm.framework.service.stuf.ws.utils;

import java.util.HashSet;
import java.util.Set;

import org.apache.ws.commons.schema.constants.Constants;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.TreeWalker;

public abstract class DOMUtils {

//	private Element root;
//	private NsMap nsMap;
//	private RefStack refStack;

	/**
	 * Simplify namespace declarations by removing and resolving duplicates in the
	 * tree, and moving the namespace declarations to the root Element for clarity.
	 * 
	 * @param element the root Element of the tree to be simplified
	 * @return the root Element of the simplified tree
	 */
	public static Element simplify(Element element) {
		return simplify(element, new NsMap(), new RefStack());
	}

	/**
	 * Simplify namespace declarations by removing and resolving duplicates in the
	 * tree, and moving the namespace declarations to the root Element for clarity.
	 * 
	 * @param root the root Element of the tree to be simplified
	 * @param map  a Map<nsName, nsUri> containing namespace definitions to be
	 *             preset to the root, these will then be used for simplification.
	 *             Using the map one can make the resulting namespace names more
	 *             human readable.
	 * @return the root Element of the simplified tree
	 */

	public static Element simplify(Element element, NsMap nsMap) {
		return simplify(element, nsMap, new RefStack());
	}

	public static Element simplify(Element element, NsMap nsMap, RefStack refStack) {
		Set<String> used = new HashSet<String>();

		DocumentTraversal docTraversal = (DocumentTraversal) element.getOwnerDocument();
		TreeWalker walker = docTraversal.createTreeWalker(element, NodeFilter.SHOW_ELEMENT, null, false);

		Node node = walker.getRoot();
		while (node != null) {
			// simplify the node
			simplifyNode(node, nsMap, refStack, used);

			// depth first, i.e. check for children
			Node child = walker.firstChild();
			if (child != null) {
				refStack.newScope();
				node = child;
//				System.out.print("Child:");
				continue;
			}
			// no more children, check siblings
			// if no more siblings then check parents' siblings
			Node sibling = null;
			while (node != null && (sibling = walker.nextSibling()) == null) {
				refStack.dropScope();
				node = walker.parentNode();
//				System.out.print("Parent:");
			}
			if (sibling != null) {
				refStack.dropScope();
				refStack.newScope();
				node = sibling;
//				System.out.print("Sibling:");
				continue;
			}
		}

		// all nodes are done, now add the remaining ns decls to the root element
		addNsToElement(element, nsMap, used);
		return element;
	}
	
	private static Node simplifyNode(Node node, NsMap nsMap, RefStack refStack, Set<String> used) {
//		System.out.println("Node: " + node.getNodeName() + ":" + node.getNodeValue());

		// simplify the node
		NamedNodeMap nsAttrs = node.getAttributes();

		// iterate over the ns attributes of the node and simplify namespaces
		new NsMap(nsAttrs).forEach((nsName, nsUri) -> {
			// add the ns to the cache
			String newName = nsMap.add(nsName, nsUri);
			if (!newName.equals(nsName)) {
				refStack.push(nsName, newName);
			}
			// remove the ns declaration from the node
			nsAttrs.removeNamedItem(Constants.XMLNS_ATTRIBUTE + ":" + nsName);
		});

		// now change the prefix for this node using the nsRes
		adaptNodePrefix(node, refStack, used);

		// now iterate over all attributes of the node and correct the prefixes
		if (nsAttrs != null) {
			for (int j = 0; j < nsAttrs.getLength(); j++) {
				Node a = nsAttrs.item(j);
				adaptNodePrefix(a, refStack, used);
			}
		}
		return node;
	}

//	public static Element addNamespaces(Element element, NsMap nsMap) {
//		nsMap.forEach((String key, String value) -> {
//			System.out.println("Adding namespace: " + key + ":" + value);
//			element.setAttributeNS(Constants.XMLNS_ATTRIBUTE_NS_URI, Constants.XMLNS_ATTRIBUTE + ":" + key, value);
//		});
////		for (Map.Entry<String, String> entry : nsMap.entrySet()) {
////			// System.out.println("Adding namespace: " + entry.getKey() + ":" +
////			// entry.getValue());
////			element.setAttributeNS(Constants.XMLNS_ATTRIBUTE_NS_URI, Constants.XMLNS_ATTRIBUTE + ":" + entry.getKey(),
////					(String) entry.getValue().toString());
////		}
//		return element;
//	}

	private static void adaptNodePrefix(Node n, RefStack refStack, Set<String> used) {
		String prefix = n.getPrefix();
		// resolve ref if it exists
		String _prefix = refStack.resolve(prefix);
//			System.out.println("adaptNodePrefix: " + prefix + " | " + _prefix);
		if (prefix != null && !prefix.equals(_prefix)) {
//			System.out.println("\tadapting prefix: " + prefix + "=>" + _prefix);
			n.setPrefix(_prefix);
			used.add(_prefix);
		} else
			used.add(prefix);

	}

	public static void addNsToElement(Element e, NsMap nsMap) {
		addNsToElement(e, nsMap, nsMap.keySet());
	}

	public static void addNsToElement(Element e, NsMap nsMap, Set<String> used) {
		nsMap.forEach((String key, String value) -> {
			if (used.contains(key)) {
//				System.out.println("\tadding ns to element: " + key + " = " + value);
				if ("".equals(key))
					e.setAttributeNS(Constants.XMLNS_ATTRIBUTE_NS_URI, Constants.XMLNS_ATTRIBUTE, value);
				else
					e.setAttributeNS(Constants.XMLNS_ATTRIBUTE_NS_URI, Constants.XMLNS_ATTRIBUTE + ":" + key, value);
			}
		});

	}
}
