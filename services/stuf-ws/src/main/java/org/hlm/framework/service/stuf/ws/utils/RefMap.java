package org.hlm.framework.service.stuf.ws.utils;

import java.util.HashMap;
import java.util.Map;

public class RefMap extends HashMap<String, String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int index = 1;

	public RefMap() {
		super();
	}

	public RefMap(Map<String, String> refMap) {
		super(refMap);
	}

	public RefMap(RefMap refMap) {
		super(refMap);
	}

	public String getValue(String key) {
		return get(key);
	}

	public String getKey(String value) {
		for (RefMap.Entry<String, String> entry : entrySet()) {
			if (entry.getValue().equals(value)) {
				return entry.getKey();
			}
		}
		return null;
	}

	public void add(String name, String ref) {
		String existing = getValue(name);
		if (existing != null) {
			// name exists in current scope, so check if they are fully equal
			if (existing.equals(ref)) {
				// AND they refer to the same
				// so we already have it... do nothing
			} else {
				// same name but different ref
				System.out.println("ERROR: Ref to be added (" + name + "->" + ref + ") is already registered as ("
						+ name + "->" + existing + ")");
			}
		} else {
			// name does not exist, add it
			put(name,ref);
//			System.out.println("added Ref: " + get(0));
		}
	}
}