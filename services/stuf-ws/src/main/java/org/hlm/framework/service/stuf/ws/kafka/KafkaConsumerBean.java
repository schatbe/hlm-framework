package org.hlm.framework.service.stuf.ws.kafka;

import java.io.IOException;
import java.time.Duration;
import java.util.Collections;

import javax.xml.stream.XMLStreamException;

import org.apache.camel.Exchange;
import org.apache.camel.Handler;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.hlm.framework.HLM;
import org.hlm.framework.service.stuf.ws.kafka.common.HLMKafkaConsumer;
import org.hlm.framework.service.stuf.ws.kafka.common.HLMRecordKey;
import org.hlm.framework.service.stuf.ws.request.JsonRpcSyntaxException;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

@Component
public class KafkaConsumerBean {

//	private final static String TOPIC = "hlm-queue";
	private static String TOPIC = HLM.Config.get("stuf-ws.kafka.topic").asString();
	private static long TIMEOUT = HLM.Config.get("stuf-ws.kafka.timeout").asLong();
	private Consumer<String, String> consumer;

	@Handler
	public void process(Exchange exchange)
			throws IOException, XMLStreamException, SAXException, JsonRpcSyntaxException {
		long time = System.currentTimeMillis();
		consumeResponse(exchange, time);
	}

	public void consumeResponse(Exchange exchange, long time) {
		try {
			consumer = HLMKafkaConsumer.createConsumer();
			consumer.subscribe(Collections.singletonList(TOPIC));

			final int giveUp = 10; // seconds
			int noRecordsCount = 0;
			long index = (long) exchange.getProperty("hlmindex");
			String response = "{ \"error\": \"NO RESPONSE RECEIVED\" }";
			while (noRecordsCount < giveUp) {
				final ConsumerRecords<String, String> consumerRecords = consumer.poll(Duration.ofMillis(TIMEOUT));
				if (consumerRecords.count() == 0) {
					noRecordsCount++;
					continue;
				}
				for (ConsumerRecord<String, String> record : consumerRecords) {
					HLMRecordKey key = new HLMRecordKey();
					key.updateFromJSON(record.key());
					if (key.type.equals(HLMRecordKey.Type.REPLY) && key.time == index) {
						System.out.printf("stuf-ws: Consumer Record:(%s, %s, %d, %d)  internaltime:%d\n", record.key(),
								"", // record.value(),
								record.partition(), record.offset(), index);
						response = record.value();
						noRecordsCount = giveUp;
						break;
					} else {
						System.out.printf("SKIPPED: stuf-ws: Consumer Record:(%s, %s, %d, %d)\n", record.key(), "", // record.value(),
								record.partition(), record.offset());
					}
				}
				consumer.commitSync();
			}
			consumer.commitSync();
			// Set the output body to response.
			exchange.getOut().setBody(response);

			// Set the content type (although it is not essential).
			exchange.getOut().setHeader(Exchange.CONTENT_TYPE, "application/json");
		} finally {
			if (consumer != null)
				consumer.close();
		}
	}
}
