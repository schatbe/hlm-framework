package org.hlm.framework.service.stuf.ws.kafka.common;

import org.hlm.common.utils.IBaseClass;

public class HLMRecordKey implements IBaseClass{

	public enum Type { REQUEST, REPLY };
	
	public Type type = Type.REQUEST;
	public long time = System.currentTimeMillis();
	public String id = "";
	
	public HLMRecordKey() {
	}
	
	public HLMRecordKey(Type type, long time, String id) {
		this.type = type;
		this.time = time;
		this.id = id;
	}
	public String toString() {
		return type.toString() + ":" +time +":"+id;
	}
}