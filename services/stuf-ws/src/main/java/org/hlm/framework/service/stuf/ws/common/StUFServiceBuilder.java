package org.hlm.framework.service.stuf.ws.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import io.datatree.Tree;

/* generic StUF Service Builder
 * constructs multiple routes, one for each given WSDL for a service
 */
@Component
public class StUFServiceBuilder {

	@Autowired
	ApplicationContext applicationContext;

	public void build(Tree prefs) {
		Tree wsdls = prefs.get("wsdls");
		wsdls.forEach((child) -> {
			String wsdlFile = child.asString();
			if (wsdlFile != null) {
				ConfigurableBeanFactory factory = (ConfigurableBeanFactory) applicationContext
						.getAutowireCapableBeanFactory();
				BeanDefinitionRegistry registry = (BeanDefinitionRegistry) factory;

				// create service config from WSDL
				StUFRouteConfig config = new StUFRouteConfig(wsdlFile, prefs);
				// register a new bean (definition) with the same name as the SOAP Service
				BeanDefinitionBuilder bdb = BeanDefinitionBuilder.childBeanDefinition("StUFService")
						.addConstructorArgValue(config);
				registry.registerBeanDefinition(config.getBeanName(), bdb.getBeanDefinition());
				// ensure the bean (and the service/route) get created by requesting it
				applicationContext.getBean(config.getBeanName());
			}
		});
	}
}
