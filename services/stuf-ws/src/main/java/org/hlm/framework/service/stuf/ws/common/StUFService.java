package org.hlm.framework.service.stuf.ws.common;

import javax.annotation.PostConstruct;

import org.apache.camel.CamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

/* 
 * The StUFService bean is essentially a prototype, but since we want to use 
 * it to create _named_ beans we have to copy its BeanDefinition instead of 
 * using @Scope("prototype").
 * See ServiceApplication.java for the relevant code:
 * ...
	StUFRouteConfig config = new StUFRouteConfig(wsdlFile, serviceEndpoint, name);
	BeanDefinitionBuilder bdb = BeanDefinitionBuilder.childBeanDefinition("StUFService")
			.addConstructorArgValue(config);
	registry.registerBeanDefinition(config.getServiceName(), bdb.getBeanDefinition());
	StUFService service = (StUFService) applicationContext.getBean(config.getServiceName());
 * ...
 */
@Component("StUFService")
@Lazy
public class StUFService {

	@Autowired
	CamelContext camelContext;

	private StUFRouteConfig config = null;
	private StUFRouteBuilder serviceRoute = null;

	public StUFService(StUFRouteConfig config) {
		this.config = config;
		try {
			serviceRoute = new StUFRouteBuilder(config);
			System.out.println("StUF Service for " + config.getServiceName() + " created");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@PostConstruct
	private void init() {
		if (serviceRoute != null) {
			try {
				camelContext.addRoutes(serviceRoute);
				System.out.println("Added Camel Route for StUF Service " + config.getServiceName());
				System.out.println(this.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public String toString() {
		String result = "serviceName   : " + config.getServiceName() + "\nservletURL   : " + config.getLocalEndpoint()
				+ "\nendpoint  : " + config.getServiceEndpoint() + "\nnamespaces: " + config.getNsMap() + "\noperations: ";
//		config.OPERATIONS_MAP.forEach((String key, String value) -> {
//					result = result + "(" + key + ", " + value + ")";
//				});
		return result;
	}

	public StUFRouteConfig getEndpointConfig() {
		return config;
	}

	public StUFRouteBuilder getServiceRoute() {
		return serviceRoute;
	}
}
