@HLMPackage(type = Type.SERVICE, name = "HLM StUF WebService", organisation = "Gemeente Haarlem")
@HLMConfiguration(root = "stuf-ws")
package org.hlm.framework.service.stuf.ws;

import org.hlm.annotation.HLMConfiguration;
import org.hlm.annotation.HLMPackage.Type;
import org.hlm.annotation.HLMPackage;
