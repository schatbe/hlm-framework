package org.hlm.framework.service.stuf.ws.wsdl;

import java.net.URL;

import javax.wsdl.xml.WSDLLocator;

import org.apache.commons.io.FilenameUtils;
import org.xml.sax.InputSource;

public class WDSLResourceLocator implements WSDLLocator {

	private String baseWSDL;
	private String baseURI;
	private String latestImportURI;

	public WDSLResourceLocator(String baseWSDL) {
		URL url = Thread.currentThread().getContextClassLoader().getResource(baseWSDL);
		this.baseWSDL = url.toString();
		this.baseURI = FilenameUtils.getPath(this.baseWSDL);
		//		System.out.println(this.baseWSDL);
		//		System.out.println(this.baseURI);
	}

	public InputSource getBaseInputSource() {
		InputSource inputSource = null;
		try {
			// InputStream inputStream = url.openStream();
			// InputSource inputSource = new InputSource(inputStream);
			inputSource = new InputSource(this.baseWSDL);
			inputSource.setSystemId(this.baseWSDL);
			inputSource.setEncoding("UTF-8");

			//			System.out.println("getBaseInputSource: " + this.baseWSDL);

		} catch (RuntimeException e) {
			e.printStackTrace();
			//		} catch (IOException e) {
			//			e.printStackTrace();
		}

		return inputSource;
	}

	public String getBaseURI() {
		return this.baseWSDL;
	}

	public InputSource getImportInputSource(String parentLocation, String importLocation) {
		// simple version, only check with baseURI as root
		InputSource inputSource = null;
		String importURI = this.baseURI + importLocation;
		try {
			inputSource = new InputSource(importURI);
			inputSource.setSystemId(importURI);
			inputSource.setEncoding("UTF-8");

			//			System.out.println("import: " + importURI);

		} catch (RuntimeException e) {
			e.printStackTrace();
			//		} catch (IOException e) {
			//			e.printStackTrace();
		}

		this.latestImportURI = importURI;
		return inputSource;
	}

	public String getLatestImportURI() {
		return this.latestImportURI;
	}

	@Override
	public void close() {
		// TODO Auto-generated method stub

	}
}
