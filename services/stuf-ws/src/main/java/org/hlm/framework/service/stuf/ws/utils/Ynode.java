package org.hlm.framework.service.stuf.ws.utils;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

public class Ynode {
//		ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory());
//		JsonNode root;

	public enum Action {
		ADD_NEW, OVERWRITE, READONLY
	};

	private String name, value;
	private Ynode parent;
	private List<Ynode> children;

	////////////////

	public Ynode(String name, String value, Ynode parent) {
		this.name = name;
		this.value = value;
		this.parent = parent;
	}

	public Ynode(String name, String value) {
		this(name, value, null);
	}

	public Ynode(String name) {
		this(name, null, null);
	}

	public Ynode() {
		this(null, null, null);
	}

	////////////////

	public String getName() {
		return this.name;
	}

	public String getValue() {
		return this.value;
	}

	////////////////

// private back-end
	private Ynode setName(String name) {
		this.name = name;
		return this;
	}

	private Ynode setValue(String value) {
		this.value = value;
		return this;
	}

	private Ynode setParent(Ynode parent) {
		this.parent = parent;
		return this;
	}

	private Ynode yroot() {
		Ynode n = this;
		while (n.parent != null)
			n = n.parent;
		return n;
	}

	private Ynode ynode(String path, Action action) {
		Ynode p = this;
		if (path.startsWith(".")) {
			// absolute path, set node to tree root
			path = path.substring(1);
			p = yroot();
		}
		// relative path (can be a single ynode)
		String[] nodes = path.split("[\\.]");
		for (String n : nodes) {
			p = p.ychild(n, action);
			if (p == null)
				return null;
		}
		return p;
	}

	private Ynode ychild(Ynode node, Action action) {
		String name = node.getName();
		Ynode child;
		if (name != null && !"".equals(name) && children != null) {
			// single ynode child
			Iterator<Ynode> iter = children.iterator();
			while (iter.hasNext()) {
				child = iter.next();
				if (name != null && name.equals(child.name)) {
					return child;
				}
			}
		}
		if (action == Action.ADD_NEW && assertNode(this)) {
			if (children == null)
				children = new ArrayList<Ynode>();
			children.add(node);
			node.setParent(this);
			return node;
		}
		return null;
	}

	private Ynode ychild(String name, Action action) {
		return ychild(new Ynode(name), action);
	}

//	private void yremove(String path) {
//		Ynode y = ynode(path, Action.READONLY);
//		if (y.parent() != null) {
//			y.parent().children.remove(y);
//			y.setParent(null);
//		}
//	}

// validations/assertions

	private boolean isRoot(Ynode y) {
		return (isNode(y) && y.name == null);
	}

	private boolean isElement(Ynode y) {
		return (y != null && y.children == null && y.value != null);
	}

	private boolean isNode(Ynode y) {
		return (y != null && y.value == null);
	}

	private boolean isItem(Ynode y) {
		return (isNode(y) && "".equals(y.name));
	}

	private boolean assertElement(Ynode y) {
		if (!isElement(y)) {
			System.out.println("Ynode{ name:" + y.name + ", value:" + y.value + ", #children:"
					+ (y.children != null ? y.children.size() : "null") + " ) is not a valid element.");
			return false;
		}
		return true;
	}

	private boolean assertNode(Ynode y) {
		if (!isNode(y)) {
			System.out.println("Ynode{ name:" + y.name + ", value:" + y.value + ", #children:"
					+ (y.children != null ? y.children.size() : "null") + " ) is not a valid node.");
			return false;
		}
		return true;
	}

	private boolean assertItem(Ynode y) {
		if (!isItem(y)) {
			System.out.println("Ynode{ name:" + y.name + ", value:" + y.value + ", #children:"
					+ (y.children != null ? y.children.size() : "null") + " ) is not a valid item.");
			return false;
		}
		return true;
	}

// locals

	// getter
	public String element(String path) {
		assertNode(this);
		Ynode child = ynode(path, Action.READONLY);
		if (assertElement(child))
			return child.value;
		return null;
	}

	// setter
	public Ynode element(String name, String value) {
		assertNode(this);
		Ynode child = ychild(name, Action.ADD_NEW);
		child.setValue(value);
		return this;
	}

// navigation
	public String getPath() {
		Ynode n = this;
		String path = n.name;
		while (n.parent != null) {
			n = n.parent;
			path = n.name + "." + path;
		}
		return path;
	}

// transfers
	// UP
	public Ynode parent() {
		return this.parent;
	}

	// DOWN
	public Ynode node(String path) {
		return ynode(path, Action.ADD_NEW);
	}

	// DOWN
	public Ynode node(Ynode node) {
		// TODO should be OVERWRITE!!!
		return ychild(node, Action.ADD_NEW);
	}

	// DOWN
	public Ynode item() {
		return ynode("", Action.ADD_NEW);
	}

// printing
	public String toString() {
		return toString(0);
	}

	public String toString(int indent) {
		if (isElement(this))
			return StringUtils.repeat("  ", indent) + name + ": " + value + "\n";

		boolean noindent = false;
		StringWriter sw = new StringWriter();
		if (isRoot(this)) {
			indent = -1;
		} else if (isItem(this)) {
			sw.write(StringUtils.repeat("  ", indent) + "- ");
			noindent = true;
		} else if (isNode(this)) {
			sw.write(StringUtils.repeat("  ", indent) + name + ": " + "\n");
		}
		if (children != null) {
			for (Ynode child : children) {
				if (noindent == true) {
					sw.write(child.toString(0));
					noindent = false;
				} else {
					sw.write(child.toString(indent + 1));
				}
			}
		}
		return sw.toString();
//				return StringUtils.repeat("  ", indent) + name + ":" + "\n";
	}

	private <T> List<T> iterToList(Iterator<T> iter) {
		List<T> list = new ArrayList<T>();
		while (iter.hasNext()) {
			list.add(iter.next());
		}
		return list;
	}

	public Ynode fromYaml(String yaml) {
		ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory());
		JsonNode node;
		try {
			node = yamlMapper.readTree(yaml);
			return fromJson(node);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	public Ynode fromJson(String json) {
		ObjectMapper jsonMapper = new ObjectMapper();
		JsonNode node;
		try {
			node = jsonMapper.readTree(json);
			return fromJson(node);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return this;
	}

	public Ynode fromJson(JsonNode node) {
		if (node.isObject()) {
			// assert this == item or node
			Iterator<Map.Entry<String, JsonNode>> iterator = node.fields();

			List<Map.Entry<String, JsonNode>> nodesList = iterToList(iterator);
//			System.out.println("Walk Tree - root:" + node + ", elements keys:" + nodesList);
			for (Map.Entry<String, JsonNode> nodEntry : nodesList) {
				String name = nodEntry.getKey();
				JsonNode newNode = nodEntry.getValue();

				// System.out.println(" entry - key: " + name + ", value:" + node);
				this.ychild(name, Action.ADD_NEW).fromJson(newNode);
			}
		} else if (node.isArray()) {
			// assert this == item or node
			Iterator<JsonNode> arrayItemsIterator = node.elements();
			List<JsonNode> arrayItemsList = iterToList(arrayItemsIterator);
			for (JsonNode arrayNode : arrayItemsList) {
				this.item().fromJson(arrayNode);
			}
		} else {
			if (node.isValueNode()) {
//				System.out.println("  valueNode: " + node.asText());
				this.setValue(node.asText());
			} else {
//				System.out.println("  node some other type");
			}
		}
		return this;
	}

	/*
	 * TESTING
	 * 
	 * Ynode y = new Ynode(); y.fromJson("{ \"test\": \"waarde\" }");
	 * y.fromJson("{\n" + "    \"store\": {\n" + "        \"book\": [\n" +
	 * "            {\n" + "                \"category\": \"reference\",\n" +
	 * "                \"author\": \"Nigel Rees\",\n" +
	 * "                \"title\": \"Sayings of the Century\",\n" +
	 * "                \"price\": 8.95\n" + "            },\n" + "            {\n"
	 * + "                \"category\": \"fiction\",\n" +
	 * "                \"author\": \"Evelyn Waugh\",\n" +
	 * "                \"title\": \"Sword of Honour\",\n" +
	 * "                \"price\": 12.99\n" + "            },\n" + "            {\n"
	 * + "                \"category\": \"fiction\",\n" +
	 * "                \"author\": \"Herman Melville\",\n" +
	 * "                \"title\": \"Moby Dick\",\n" +
	 * "                \"isbn\": \"0-553-21311-3\",\n" +
	 * "                \"price\": 8.99\n" + "            },\n" + "            {\n"
	 * + "                \"category\": \"fiction\",\n" +
	 * "                \"author\": \"J. R. R. Tolkien\",\n" +
	 * "                \"title\": \"The Lord of the Rings\",\n" +
	 * "                \"isbn\": \"0-395-19395-8\",\n" +
	 * "                \"price\": 22.99\n" + "            }\n" + "        ],\n" +
	 * "        \"bicycle\": {\n" + "            \"color\": \"red\",\n" +
	 * "            \"price\": 19.95\n" + "        }\n" + "    },\n" +
	 * "    \"expensive\": 10\n" + "}");
	 */

}
