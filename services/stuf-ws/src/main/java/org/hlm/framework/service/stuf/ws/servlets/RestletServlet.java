package org.hlm.framework.service.stuf.ws.servlets;

import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;

import org.restlet.ext.spring.SpringServerServlet;

@WebServlet(name = "RestletServlet", value = "/rs/*", loadOnStartup = 1, initParams = {
		@WebInitParam(name = "org.restlet.component", value = "RestletComponent") })
public class RestletServlet extends SpringServerServlet {
	private static final long serialVersionUID = -8389248712309586966L;
}
