package org.hlm.framework.service.stuf.ws.common;

import java.net.URI;

import org.apache.camel.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class StUFProcessor {

	@Autowired
	ApplicationContext applicationContext;

	public static final ObjectMapper JSON_MAPPER = new ObjectMapper();

	public StUFService getStUFServiceBean(Exchange exchange) {
		URI uri = URI.create(exchange.getFromEndpoint().getEndpointUri());
		String[] path = uri.getPath().split("/");
		String serviceName = path[path.length-2] + path[path.length - 1];
		// System.out.println(serviceName);
		return (StUFService) applicationContext.getBean(serviceName);
	}
}