package org.hlm.framework.service.stuf.ws.wsdl;

import java.net.URL;

import org.apache.commons.io.FilenameUtils;
import org.apache.ws.commons.schema.resolver.URIResolver;
import org.xml.sax.InputSource;

public class XMLSchemaResolver implements URIResolver {

	private String baseWSDL;
	private String baseURI;

	public XMLSchemaResolver(String baseWSDL) {
		URL url = Thread.currentThread().getContextClassLoader().getResource(baseWSDL);
		this.baseWSDL = url.toString();
		this.baseURI = FilenameUtils.getPath(this.baseWSDL);
		//		System.out.println(this.baseWSDL);
		//		System.out.println(this.baseURI);
	}

	public InputSource resolveEntity(String targetNamespace, String schemaLocation, String baseUri) {
		// simple version, only check with baseURI as root
		InputSource inputSource = null;
		String importURI = this.baseURI + schemaLocation;
		//		System.out.println(targetNamespace);
		//		System.out.println(schemaLocation);
		//		System.out.println(baseUri);
		//		System.out.println("import: " + importURI);
		try {
			inputSource = new InputSource(importURI);
			inputSource.setSystemId(importURI);
			inputSource.setEncoding("UTF-8");

		} catch (RuntimeException e) {
			e.printStackTrace();
			//		} catch (IOException e) {
			//			e.printStackTrace();
		}

		return inputSource;
	}
}
