package org.hlm.framework.service.stuf.ws.utils.ytree;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public enum YPermissions {
//		ObjectMapper yamlMapper = new ObjectMapper(new YAMLFactory());
//		JsonNode root;

	INERTIA(), // nothing is always allowed, only used as dummy...
	LOOKUP(), // find an existing node
	OVERWRITE(LOOKUP), // if a node exists overwrite it, implies LOOKUP
	EXTEND(LOOKUP), // if a node does not exist add it, implies LOOKUP
	MERGE(LOOKUP, OVERWRITE, EXTEND), // implies OVERWRITE and EXTEND
	IMPORT(); // treat node/item as ROOT and import its children
				// but an element will be imported as is

	private Set<YPermissions> permissions = new HashSet<YPermissions>();

	YPermissions(YPermissions... permissions) {
		this.permissions.add(this);
		for (YPermissions permission : permissions) {
			this.permissions.add(permission);
			this.permissions.addAll(permission.permissions);
		}
	}

	static YPermissions ALLOW(YPermissions... permissions) {
		YPermissions retval = INERTIA;
		for (YPermissions permission : permissions) {
			retval.permissions.add(permission);
		}
		return retval;
	}

	public boolean isAllowed(YPermissions... permissions) {
		return isAllOf(permissions);
	}

	public boolean isAllOf(YPermissions... permissions) {
		return this.permissions.containsAll(Arrays.asList(permissions));
	}

	public boolean isSomeOf(YPermissions... permissions) {
		boolean retval = false;
		for (YPermissions permission : permissions) {
			retval |= this.permissions.contains(permission);
		}
		return retval;
	}
}
