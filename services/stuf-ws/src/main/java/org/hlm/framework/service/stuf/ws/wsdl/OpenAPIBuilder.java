package org.hlm.framework.service.stuf.ws.wsdl;

import javax.wsdl.Operation;

import org.hlm.framework.service.stuf.ws.utils.ytree.YNode;

public class OpenAPIBuilder {
//	StringWriter paths = new StringWriter();
//	PrintWriter pw = new PrintWriter(sw);

	private WSDLParser wsdlParser;

	YNode y = new YNode();
	YNode paths, definitions;

	public OpenAPIBuilder(WSDLParser wsdlParser) {
		this.wsdlParser = wsdlParser;
		
		y.element("openapi","3.0.0");
		y.node("info")
			.element("title", wsdlParser.serviceName)
			.element("description","")
			.element("version","0.0.1");
		
		y.node("servers")
			.item()
				.element("url", wsdlParser.serviceUrl)
				.element("description", "");
		paths = y.node("paths");
		definitions = y.node("definitions");
		
//		System.out.println(this);
	}

	public void addPathSpec(Operation operation, YNode params, YNode output) {
		YNode p = paths.node("/" + operation.getName())
			.element("summary", "")
			.element("description", "");

		if (params != null) {
			p = p.node("post");
			p.node("requestBody")
				.element("required", "true")
				.node("content.application/json.schema")
					.node(params);
		} else {
			p = p.node("get");
		}
		p.node("reponse")
			.node("'200'")
				.node("content.application/json")
					.node(output);
	}
	
	public void addDefinition(YNode d) {
		definitions.node(d);
	}

	public String toString() {
		return y.toString();
	}
}
