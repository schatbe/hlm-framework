package org.hlm.framework.service.stuf.ws;

import org.apache.camel.component.restlet.RestletComponent;
import org.restlet.Component;
import org.restlet.ext.spring.SpringServerServlet;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/* 
    <bean id="RestletComponent" class="org.restlet.Component"/>

    <bean id="RestletComponentService" class="org.apache.camel.component.restlet.RestletComponent">
        <constructor-arg ref="RestletComponent" />
</bean>

 */

@Configuration
public class RestletConfiguration {

	@Bean(name = "RestletComponent")
	public Component getComponent() {
		Component component = new Component();
//		component.getDefaultHost().setDefaultMatchingMode(matchingMode);
		return component;

	}

	@Bean(name = "RestletComponentService")
	public RestletComponent getRestletComponent() {
		RestletComponent component = new RestletComponent(getComponent());
//		component.setControllerDaemon(controller_daemon);
//		component.setControllerSleepTimeMs(controller_sleep_time_ms);
//		component.setInboundBufferSize(inbound_buffer_size);
//		component.setMinThreads(min_threads);
//		component.setMaxThreads(max_threads);
//		component.setLowThreads(low_threads);
//		component.setMaxQueued(max_queued);
//		component.setMaxConnectionsPerHost(max_connections_per_host);
//		component.setMaxTotalConnections(max_total_connections);
//		component.setOutboundBufferSize(outbound_buffer_size);
//		component.setPersistingConnections(persisting_connections);
//		component.setPipeliningConnections(pipelining_connections);
//		component.setThreadMaxIdleTimeMs(thread_max_idle_time_ms);
//		component.setUseForwardedForHeader(use_forwarded_header);
//		component.setReuseAddress(reuse_address);
		return component;
	}
	
//	@Bean("RestletServlet")
//	public ServletRegistrationBean exampleServletBean() {
//	    ServletRegistrationBean bean = new ServletRegistrationBean(
//	      new SpringServerServlet(), "/rs/*");
//	    bean.setLoadOnStartup(1);
//	    return bean;
//	}

}
