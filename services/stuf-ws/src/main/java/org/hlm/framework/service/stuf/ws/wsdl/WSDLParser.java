package org.hlm.framework.service.stuf.ws.wsdl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.wsdl.Definition;
import javax.wsdl.Operation;
import javax.wsdl.Part;
import javax.wsdl.Port;
import javax.wsdl.Service;
import javax.wsdl.WSDLException;
import javax.wsdl.extensions.soap.SOAPAddress;
import javax.wsdl.factory.WSDLFactory;
import javax.wsdl.xml.WSDLReader;

import org.apache.camel.util.CastUtils;
import org.apache.ws.commons.schema.XmlSchema;
import org.apache.ws.commons.schema.XmlSchemaCollection;
import org.hlm.framework.service.stuf.ws.common.StUFRouteBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import com.ibm.wsdl.extensions.schema.SchemaImpl;

/**
 * This class calls methods during initialization of the adapter to obtain the
 * WSDL interface from the URL, parse it and extract required information to
 * specify endpoint details. Additionally, it creates a HTML page to help
 * clients create JSON-RPC request messages.
 * 
 * @author Sevket Gökay <sevket.goekay@rwth-aachen.de>
 * @author Lars C. Gleim <lars.gleim@rwth-aachen.de>
 *
 */

public class WSDLParser {

	final static Logger LOG = LoggerFactory.getLogger(WSDLParser.class);
	public String wsdlUrl, serviceUrl, serviceName, wsNamespace, soapPortName, dirPath;
	public Map<String, String> nsMap;
	Port soapPort;
	private XmlSchema schema;
	// Operation name and request message payload name might be different.
	// We store a map of operation names and their request message payload names
	// for lookup (for document/literal wrapped style).
	public HashMap<String, String> OPERATIONS_MAP = new HashMap<String, String>();
	
	public XmlSchema getSchema() {
		return schema;
	}

	public WSDLParser(String wsdlUrl) {
		this.wsdlUrl = wsdlUrl;
		readWSDL();
	}

	/**
	 * Main method that starts the WSDL parsing process and calls other helper
	 * methods.
	 */
	public void readWSDL() {
		try {
			// Set up the reader
			WSDLReader reader = WSDLFactory.newInstance().newWSDLReader();
			reader.setFeature("javax.wsdl.verbose", false);
			reader.setFeature("javax.wsdl.importDocuments", true);

			WDSLResourceLocator locator = new WDSLResourceLocator(this.wsdlUrl);
			Definition def = reader.readWSDL(locator);

			// Initialize the necessary variables for Cxf
			initializeVariables(def);

			// Create a HTML page from the WSDL document
			// if(dirPath != null) convertWSDLtoHTML(wsdlDoc);

			// Get the XML Schema
			List<?> extensions = def.getTypes().getExtensibilityElements();
			for (Object extension : extensions) {
				if (extension instanceof SchemaImpl) {
					Element schElement = ((SchemaImpl) extension).getElement();
					XmlSchemaCollection schemaCollection = new XmlSchemaCollection();
					schemaCollection.setSchemaResolver(new XMLSchemaResolver(this.wsdlUrl));
					schema = schemaCollection.read(schElement);
				}
			}

			initializeOperations();

		} catch (WSDLException e) {
			e.printStackTrace();
			// } catch (NamingException e) {
			// e.printStackTrace();
		}
	}

	/**
	 * Reads the details from the WSDL definition.
	 */
	@SuppressWarnings("unchecked")
	private void initializeVariables(Definition def) {

		// Read the WSDL namespace
		wsNamespace = def.getTargetNamespace();
		nsMap = (Map<String, String>) def.getNamespaces();
		// nsMap.remove("");


		// Get the details to specify the service endpoint
		Collection<Service> services = CastUtils.cast(def.getAllServices().values());
		for (Service service : services) {
			Collection<Port> ports = CastUtils.cast(service.getPorts().values());
			for (Port port : ports) {
				List<?> extensions = port.getExtensibilityElements();
				for (Object extension : extensions) {
					if (extension instanceof SOAPAddress) {
						serviceUrl = ((SOAPAddress) extension).getLocationURI();
						serviceName = service.getQName().getLocalPart();
						soapPortName = port.getName();
						soapPort = port;
//						LOG.info("\n\tService name: " + serviceName + "\n\tPort name: " + soapPortName
//								+ "\n\tService namespace: " + wsNamespace + "\n\tRemote service address: " + serviceUrl);
					}
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	public List<Operation> getOperations() {
		return (List<Operation>) soapPort.getBinding().getPortType().getOperations();
	}

	/**
	 * Extracts the operations offered by a web service from the WSDL definition and
	 * calls the routines for code stub creation and respective usage documentation.
	 */
	private void initializeOperations() {

//		LOG.info("**** Listing methods ****");

		// Iterate over all operations defined for a given soap port
		for (Object op : getOperations()) {
			Operation operation = (Operation) op;
			String opName = operation.getName();
//			System.out.println(" - " + opName);

			// Get parts from WSDL that describe the operation's parameters
			Collection<Part> inParts = CastUtils.cast(operation.getInput().getMessage().getParts().values());

			// Get parts from WSDL that describe the operation's return value
//			Collection<Part> outParts = CastUtils.cast(operation.getOutput().getMessage().getParts().values());

			// Store the operation name and its input message payload name in a map
			if (inParts.size() == 1) {
				Part inPart = inParts.iterator().next();

				if (inPart.getElementName() == null && inPart.getTypeName() != null) {
					OPERATIONS_MAP.put(opName, inPart.getName());

				} else if (inPart.getElementName() != null && inPart.getTypeName() == null) {
					OPERATIONS_MAP.put(opName, inPart.getElementName().getLocalPart());
				}
			}
		}
	}

	/**
	 * Getter for SOAP port name
	 * 
	 * @return The port name
	 */
	public String getPortName() {
		return soapPortName;
	}

}
